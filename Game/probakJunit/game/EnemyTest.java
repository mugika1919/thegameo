package game;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import entity.Enemy;
import entity.Player;

public class EnemyTest {

	Enemy e1, e2;
	Player pl;
	
	@Before
	public void setUp() throws Exception {
		pl = Player.getMyPlayer();
		e1 = new Enemy(1, 3, 2);
	}

	@After
	public void tearDown() throws Exception {
		pl = null;
		e1 = null;
		e2 = null;
	}

	@Test
	public void testEnemy() {
		assertNotNull(e1);
	}

	@Test
	public void testGetHurt1() {
		//Comprobar la defensa
		assertFalse(e1.isDead());
		e1.getHurt(1);
		assertFalse(e1.isDead());
		e1.getHurt(2);
		assertFalse(e1.isDead());
		e1.getHurt(1);
		assertTrue(e1.isDead());
		e1.getHurt(1);
		assertTrue(e1.isDead());
	}
	
	@Test
	public void testGetHurt2() {
		assertFalse(e1.isDead());
		e1.getHurt(4);
		assertTrue(e1.isDead());
	}
	
	@Test
	public void testGetHurt3() {
		assertFalse(e1.isDead());
		e1.getHurt(40);
		assertTrue(e1.isDead());
	}
	@Test
	public void testAttack() {
		Player.getMyPlayer().printInfo();
		e1.attack();
		Player.getMyPlayer().printInfo();
	}

	@Test
	public void testDefendMyself() {
		assertFalse(e1.isDead());
		e1.getHurt(3);
		try{
			e1.defendMyself(2);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		assertFalse(e1.isDead());
		e1.getHurt(2);
		assertFalse(e1.isDead());
		
		e1.updateRound();
		try{
			e1.defendMyself(2);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
		e1.getHurt(5);
		assertFalse(e1.isDead());
		
		try{
			e1.defendMyself(2);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		e1.updateRound();
		
		e1.getHurt(5);
		assertTrue(e1.isDead());
		
		try{
			e1.defendMyself(-2);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void testUpdateRound() {
		
		e1.getHurt(3);
		e1.updateRound();
		assertFalse(e1.isDead());
		e1.getHurt(3);
		assertFalse(e1.isDead());
		e1.getHurt(3);
		assertTrue(e1.isDead());
		
	}

	@Test
	public void testSetVeneno() {
		e1 = new Enemy(3, 1, 2);
		e1.setVeneno(2);
		e1.updateRound();
		assertFalse(e1.isDead());
		e1.updateRound();
		assertTrue(e1.isDead());
		
		e1 = new Enemy(10, 1, 2);
		e1.setVeneno(5);
		System.out.println(e1.getStatus());
		e1.updateRound();
		System.out.println(e1.getStatus());
		e1.setVeneno(1);
		System.out.println(e1.getStatus());
		assertFalse(e1.isDead());
		e1.updateRound();
		assertTrue(e1.isDead());
		
		e1 = new Enemy(1, 1, 2);
		e1.setVeneno(5);
		System.out.println(e1.getStatus());
		e1.updateRound();
		assertTrue(e1.isDead());
		
		e1 = new Enemy(12, 1, 2);
		e1.setVeneno(4);
		System.out.println(e1.getStatus());
		e1.updateRound();
		System.out.println(e1.getStatus());
		e1.updateRound();
		System.out.println(e1.getStatus());
		e1.updateRound();
		System.out.println(e1.getStatus());
		e1.updateRound();
		System.out.println(e1.getStatus());
		assertFalse(e1.isDead());
		assertEquals(e1.getHP(),2);
		
		
	}

	@Test
	public void testSetVulnerableInt() {
		//Hay que tener en cuenta la defensa
		e1.setVulnerable(2);
		System.out.println(e1.getStatus());
		e1.updateRound();
		System.out.println(e1.getStatus());
		
		e1.setVulnerable(1);
		System.out.println(e1.getStatus());
		e1.getHurt(2);
		assertEquals(e1.getHP(),1);
		
		e1 = new Enemy(10, 0, 1);
		e1.setVulnerable(1);
		e1.getHurt(5);
		assertEquals(e1.getHP(),2);
		e1.getHurt(1);
		assertEquals(e1.getHP(), 0);
		
		e1 = new Enemy(10, 0, 1);
		e1.setVulnerable(1);
		e1.getHurt(3);
		assertEquals(e1.getHP(),5);
		
		e1 = new Enemy(12, 0, 1);
		e1.setVulnerable(1);
		e1.getHurt(7);
		assertEquals(e1.getHP(),1);
		
	
	}

	@Test
	public void testSetVulnerableIntInt() {
		e1 = new Enemy(15, 0, 1);
		e1.setVulnerable(1,1);
		e1.setVulnerable(1);
		e1.getHurt(7);
		assertEquals(e1.getHP(),1);
	}
	
	/*
	@Test
	public void testSetSangrado() {
		e1 = new Enemy(4, 0, 1);
		e1.setSangrado();
		e1.updateRound();
		assertEquals(e1.getHP(), 3);
		e1.updateRound();
		assertEquals(e1.getHP(), 2);
		e1.updateRound();
		assertEquals(e1.getHP(), 1);
		e1.updateRound();
		assertEquals(e1.getHP(), 0);
	}
	
	
	@Test
	public void testSetSangradoInt() {
		e1 = new Enemy(8, 5, 1);
		e1.setSangrado(2);
		e1.updateRound();
		assertEquals(e1.getHP(), 6);
		e1.updateRound();
		assertEquals(e1.getHP(), 4);
		e1.updateRound();
		assertEquals(e1.getHP(), 2);
		e1.updateRound();
		assertEquals(e1.getHP(), 0);
	}
	*/
	
	@Test
	public void testGetStatus() {
		e1 = new Enemy(8, 5, 1);
		// e1.setSangrado(2);
		e1.setVeneno(2);
		e1.setVulnerable(2);
		System.out.println(e1.getStatus());
		
	}

	@Test
	public void testIsDead() {
		fail("Not yet implemented");
	}

}
