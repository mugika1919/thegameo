package cards;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import entity.Enemy;
import entity.Player;

public class DefendCardeTest {
	
	DefendCard car1, car2;
	Enemy en;
	Player pl;
	
	@Before
	public void setUp() throws Exception {
		car1 = new DefendCard("Blokeatu", 1201, 5, 1, 1);
		car1.setDescription("Lortu 5 defentsa");
		car2 = new DefendCard("Blokeatu+", 1201, 8, 3, 1);
		car2.setDescription("Lortu 8 defentsa");

		en = new Enemy("ETSAI1", 001, 50, 0, null);
		en = en.copyButRandomized();
		pl = Player.getMyPlayer();
		pl.updateRound();
	}

	@After
	public void tearDown() throws Exception {
		car1 = null;
		car2 = null;
		
		en = null;
		pl.reset();
		pl = null;
	}
	
	@Test
	public void testCloneCard() {
		this.ondoKlonatua(car1, car1.cloneCard());
		this.ondoKlonatua(car2, car2.cloneCard());
	}
	
	private void ondoKlonatua(Card pOrig, Card pClone) {
		assertNotEquals(pOrig, pClone);
		assertEquals(pOrig.getId(), pClone.getId());
		assertEquals(pOrig.getName(), pClone.getName());
		assertEquals(pOrig.getDesc(), pClone.getDesc());
		assertEquals(pOrig.getPower(), pClone.getPower());
		assertEquals(pOrig.getPrice(), pClone.getPrice());
		assertEquals(pOrig.getEnergyCost(), pClone.getEnergyCost());
		assertEquals(pOrig.getProbability(), pClone.getProbability());
		assertFalse(pOrig.cloneCard().isUsed());
		assertFalse(pClone.isUsed());
	}

	@Test
	public void testUseCard() {
		String strPrev = pl.getBlock();
		pl.updateRound();
		car1.useCard(null, null);
		assertFalse(car1.isUsed());
		car1.useCard(null, en);
		assertFalse(car1.isUsed());
		
		assertEquals(strPrev, pl.getBlock());
		car1.useCard(pl, null);
		assertTrue(car1.isUsed());
		assertNotEquals(strPrev, pl.getBlock());
		
		strPrev = pl.getBlock();
		car1.useCard(pl, en);
		assertTrue(car1.isUsed());
		assertEquals(strPrev, pl.getBlock());
		
		car2.useCard(pl, en);
		assertFalse(car2.isUsed());
		assertEquals(strPrev, pl.getBlock());
		
		
	}

	@Test
	public void testGetRealTarget() {
		assertEquals(car1.getRealTarget(en, pl), "Etsaia: " + en.getName());
		assertEquals(car2.getRealTarget(en, pl), "Etsaia: " + en.getName());
		
		assertEquals(car1.getRealTarget(pl, en), "Jokalaria: " + pl.getName());
		assertEquals(car2.getRealTarget(pl, pl), "Jokalaria: " + pl.getName());
		
		assertEquals(car1.getRealTarget(null, null), "Ez dago");
		
	}

	@Test
	public void testDefendCard() {
		assertNotNull(car1);
		assertNotNull(car2);
	}

}
