package cards;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CardTest {
	Card card10, card11, card2;
	CardCollection cardCol;
	
	@Before
	public void setUp() throws Exception {
		card10 = new AttackCard("Kolpe", 1101, 9, 1, 1, 0, 0);
		card10.setDescription("Egin 4 puntuko mina");
		
		card11 = new AttackCard("Kolpe+", 11101, 9, 1, 1, 0, 0);
		card11.setDescription("Egin 9 puntuko mina");
		
		card2 = new DefendCard("Blokeatu+", 11201, 8, 0, 3);
		card2.setDescription("Lortu 8 defentsa");
		
		cardCol = CardCollection.getCardCollection();
		
	}

	@After
	public void tearDown() throws Exception {
		card10 = null;
		card11 = null;
		card2 = null;
		cardCol.reset();
		cardCol = null;
	}

	@Test
	public void testCard() {
		assertNotNull(card11);
		assertNotNull(card2);
		card2 = new DefendCard("Blokeatu+", -1, -1, -1, -1);
		assertNotNull(card2);
		card2 = new DefendCard("Blokeatu+", 0, 0, 0, 0);
		assertNotNull(card2);
		
		card10 = new AttackCard("Kolpe", -1, -1, -1, -1, -1, -1);
		assertNotNull(card10);
		card10 = new AttackCard("Kolpe", 0, 0, 0, 0, 0, 0);
		assertNotNull(card10);
	}

	@Test
	public void testGetPrice() {
		assertEquals(card11.getPrice(), 1);
		assertEquals(card2.getPrice(), 3);
		
		card11 = new AttackCard("Kolpe+", 11101, 9, 2, 33, 0, 0);
		assertEquals(card11.getPrice(), 33);
	}

	@Test
	public void testGetName() {
		assertEquals(card11.getName(), "Kolpe+");
		assertEquals(card2.getName(), "Blokeatu+");
		
		card11 = new AttackCard("LOLOLO", 11101, 9, 2, 33, 0, 0);
		assertEquals(card11.getName(), "LOLOLO");
	}

	@Test
	public void testGetId() {
		assertEquals(card11.getId(), 11101);
		assertEquals(card2.getId(), 11201);
		
		card11 = new AttackCard("LOLOLO", 1400575, 0, 1, 2, 0, 0);
		assertEquals(card11.getId(), 1400575);
		
		card11 = new DefendCard("LOLOLO", 1400575, 0, 1, 2);
		assertEquals(card11.getId(), 1400575);
	}

	@Test
	public void testGetCost() {
		assertEquals(card11.getEnergyCost(), 1);
		assertEquals(card2.getEnergyCost(), 0);
		
		card11 = new AttackCard("LOLOLO", 0, 0, 9, 0, 0, 0);
		assertEquals(card11.getEnergyCost(), 9);
		
		card11 = new DefendCard("LOLOLO", 0, 0, 10, 0);
		assertEquals(card11.getEnergyCost(), 10);
	}

	@Test
	public void testGetPower() {
		assertEquals(card11.getPower(), 9);
		assertEquals(card2.getPower(), 8);
		
		card11 = new AttackCard("LOLOLO", 0, 9, 0, 0, 0, 0);
		assertEquals(card11.getPower(), 9);
		
		card11 = new DefendCard("LOLOLO", 0, 8, 0, 0);
		assertEquals(card11.getPower(), 8);
	}

	@Test
	public void testGetDesc() {
		assertEquals(card11.getDesc(), "Egin 9 puntuko mina");
		assertEquals(card2.getDesc(), "Lortu 8 defentsa");
		card11 = new AttackCard("LOLOLO", 0, 9, 0, 0, 0, 0);
		assertEquals(card11.getDesc(), "Ez du deskripziorik");
		card11.setDescription("KAIXOOO");
		assertEquals(card11.getDesc(), "KAIXOOO");
	}

	@Test
	public void testGetUpgrade() {
		cardCol.addCardToCollection(card10);
		cardCol.addCardToCollection(card11);		
		card10 = cardCol.searchCardById(card10.getId());
		card11 = cardCol.searchCardById(card11.getId());
		
		assertEquals(card10.getId() + 10000, card11.getId());
		
		assertEquals(card11, card10.getUpgrade());
		assertNull(card11.getUpgrade());
		
		
		card10 = new AttackCard("LOLOLO", -10, 9, 0, 0, 0, 0);
		card11 = new AttackCard("LOLOLO+", -10010, 9, 0, 0, 0, 0);
		
		cardCol.addCardToCollection(card10);
		cardCol.addCardToCollection(card11);		
		card10 = cardCol.searchCardById(card10.getId());
		card11 = cardCol.searchCardById(card11.getId());
		
		assertEquals(card10.getId() - 10000, card11.getId());
		
		assertEquals(card11, card10.getUpgrade());
		assertNull(card11.getUpgrade());
	}

	@Test
	public void testSetDescription() {
		assertEquals(card10.getDesc(), "Ez du deskripziorik");
		card10.setDescription("HEY");
		assertEquals(card10.getDesc(), "HEY");
		card10.setDescription("");
		assertEquals(card10.getDesc(), "");
	}

	@Test
	public void testHasThisId() {
		assertTrue(card10.hasThisId(1101));
		assertFalse(card10.hasThisId(0));
	}

	@Test
	public void testHasSameId() {
		
		assertTrue(card10.hasSameId(card10));
		assertFalse(card10.hasSameId(card11));
		card11 = new AttackCard("LOLOLO+", 1101, 9, 0, 0, 0, 0);
		assertTrue(card10.hasSameId(card11));
	}

	@Test
	public void testIsUsed() {
		assertFalse(card10.isUsed());
		card10.use();
		assertTrue(card10.isUsed());
	}

	@Test
	public void testUse() {
		assertFalse(card10.isUsed());
		
		card10.use();
		assertTrue(card10.isUsed());
		card10.use();
		assertTrue(card10.isUsed());
	}

	@Test
	public void testSetProbability() {
		assertEquals(card10.getProbability(), 10);
		card10.setProbability(20);
		assertEquals(card10.getProbability(), 20);
		
		card10.setProbability(-20);
		assertEquals(card10.getProbability(), 20);
		
		card10.setProbability(0);
		assertEquals(card10.getProbability(), 0);

	}

	@Test
	public void testGetProbability() {
		assertEquals(card10.getProbability(), 10);
		
		card10.setProbability(1000);
		assertEquals(card10.getProbability(), 1000);
	}

	@Test
	public void testReset() {
		assertFalse(card10.isUsed());
		card10.use();
		assertTrue(card10.isUsed());
		card10.reset();
		assertFalse(card10.isUsed());
		
		card10.reset();
		assertFalse(card10.isUsed());
	}

	@Test
	public void testGetCardAsString() {
		System.out.println("---testGetCardAsString()---");
		System.out.println(card10.getCardAsString());
		System.out.println(card2.getCardAsString());
	}

	@Test
	public void testGetCardInfoAsSimpleString() {
		System.out.println("---testGetCardInfoAsSimpleString()---");
		System.out.println(card10.getCardInfoAsSimpleString());
		System.out.println(card2.getCardInfoAsSimpleString());
		card2.setProbability(0);
		System.out.println(card2.getCardInfoAsSimpleString());
	}
}
