package cards;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import entity.Enemy;
import entity.Player;
import game.Battle;
import game.TextBox;

public class DynamicCardTest {
	
	DynamicCard car1, car2;
	Enemy en;
	Player pl;
	@Before
	public void setUp() throws Exception {
		car1 = new DynamicCard("DeckTamain", 01, 2, 1, 1, 2);
		car2 = new DynamicCard("Jok-arenBizMax", 02, 2, 1, 1, 3);
		
		en = new Enemy("ETSAI1", 001, 50, 0, null);
		en = en.copyButRandomized();
		pl = Player.getMyPlayer();
		pl.updateRound();
	}

	@After
	public void tearDown() throws Exception {
		car1 = null;
		car2 = null;
		
		en = null;
		pl.reset();
		pl = null;
	}

	@Test
	public void testCloneCard() {
		this.ondoKlonatua(car1, car1.cloneCard());
		this.ondoKlonatua(car2, car2.cloneCard());
	}
	
	private void ondoKlonatua(Card pOrig, Card pClone) {
		assertNotEquals(pOrig, pClone);
		assertEquals(pOrig.getId(), pClone.getId());
		assertEquals(pOrig.getName(), pClone.getName());
		assertEquals(pOrig.getDesc(), pClone.getDesc());
		assertEquals(pOrig.getPower(), pClone.getPower());
		assertEquals(pOrig.getPrice(), pClone.getPrice());
		assertEquals(pOrig.getEnergyCost(), pClone.getEnergyCost());
		assertEquals(pOrig.getProbability(), pClone.getProbability());
		assertFalse(pOrig.cloneCard().isUsed());
		assertFalse(pClone.isUsed());
	}

	@Test
	public void testUseCard() {
		car1.useCard(null, null);
		car1.useCard(pl, null);
		car1.useCard(null, en);
		assertFalse(car1.isUsed());
		
		car1 = new DynamicCard("Ezer", 01, 2, 2, 1, 0);
		//Karta Erabiliko da
		car1.useCard(pl, en);
		assertTrue(car1.isUsed());
		
		//Ez da karta Erabiliko jadanik erabilia izan delako
		car1.useCard(pl, en);
		assertTrue(car1.isUsed());
		
		//Ez da karta Erabiliko ez duelako energia nahikorik
		car1.reset();
		assertFalse(car1.isUsed());
		car1.useCard(pl, en);
		assertFalse(car1.isUsed());
		
		//Karta hau beti erabiliko da energia erabiltzen ez dulako bizitza baizik
		car1 = new DynamicCard("2BiziKendEnergGabe", 01, 2, 2, 1, -10);
		car1.useCard(pl, en);
		assertTrue(car1.isUsed());
		TextBox.getBoxDev().activate(true);
		TextBox.getBoxDev().printText();
		
	}
	
	@Test
	public void testUseCardType2_1() {
		Battle bt = new Battle();
		
		car1 = new DynamicCard("DeckLuzeeraT2", 01, 2, 0, 1, 2);
		car2 = new DynamicCard("DeckLuzeeraT-1", 01, 2, 0, 1, -1);
		
		int prev = en.getHp();
		car1.useCard(pl, en);
		assertEquals(prev - Deck.getMyDeck().deckSize(10), en.getHp());
		
		bt.beginBattle();
		prev = en.getHp();
		assertFalse(car2.isUsed());
		car2.useCard(pl, en);
		assertTrue(car2.isUsed());
		assertEquals(prev - Deck.getMyDeck().deckSize(10) - Battle.getCurrentBatle().getRound(), en.getHp());
		
		car2.reset();
		Battle.getCurrentBatle().battleLogic();
		assertEquals(Battle.getCurrentBatle().getRound(), 0);
		prev = en.getHp();
		assertFalse(car2.isUsed());
		car2.useCard(pl, en);
		assertTrue(car2.isUsed());
		assertEquals(prev - Deck.getMyDeck().deckSize(10) - Battle.getCurrentBatle().getRound(), en.getHp());
	}
	
	@Test
	public void testUseCardType3() {

		car1 = new DynamicCard("DeckLuzeeraT2", 01, 2, 0, 1, 3);
		
		int prev = en.getHp();
		assertFalse(car1.isUsed());
		car1.useCard(pl, en);
		assertTrue(car1.isUsed());
		assertEquals(prev - (int)(Player.getMyPlayer().getMaxHp()*0.2), en.getHp());
		
	}
	
	@Test
	public void testUseCardType4() {

		car1 = new DynamicCard("DeckLuzeeraT2", 01, 2, 0, 1, 4);
		
		int prev = en.getHp();
		assertFalse(car1.isUsed());
		car1.useCard(pl, en);
		assertTrue(car1.isUsed());
		assertEquals(prev - Player.getMyPlayer().getMoney(), en.getHp());
		
	}
	
	@Test
	public void testUseCardType6() {

		car1 = new DynamicCard("DeckLuzeeraT2", 01, 2, 0, 1, 6);
		
		int prev = en.getHp();
		assertFalse(car1.isUsed());
		car1.useCard(pl, en);
		assertTrue(car1.isUsed());
		assertEquals(prev - (pl.getMaxHp()-pl.getHp())/10, en.getHp());
		
	}
	
	@Test
	public void testUseCardTypeMin2() {

		car1 = new DynamicCard("DeckLuzeeraT2", 01, 2, 0, 1, -2);
		
		int prev = en.getHp();
		pl.takeMoney(10);
		assertFalse(car1.isUsed());
		car1.useCard(pl, en);
		assertTrue(car1.isUsed());
		
		assertEquals(prev-pl.getMoney()/2, en.getHp());
		
	}
	
	@Test
	public void testUseCardTypeMin3() {

		car1 = new DynamicCard("DeckLuzeeraT2", 01, 2, 0, 1, -3);
		
		assertEquals("Block: 0 �!", pl.getBlock());
		assertFalse(car1.isUsed());
		pl.getHurt(10);
		car1.useCard(pl, en);
		assertTrue(car1.isUsed());
		String concat = "Block: " + (pl.getMaxHp()-pl.getHp());
		assertEquals(pl.getBlock(), concat);
	}
	
	@Test
	public void testUseCardTypeMin4() {

		car1 = new DynamicCard("DeckLuzeeraT2", 01, 2, 0, 1, -4);
		
		int prev = en.getHp();
		assertFalse(car1.isUsed());
		car1.useCard(pl, en);
		assertTrue(car1.isUsed());
		assertEquals(prev - (pl.getMaxHp() - pl.getHp()) * 2, en.getHp());
		
	}
	
	@Test
	public void testUseCardTypeMin10() {

		car1 = new DynamicCard("DeckLuzeeraT2", 01, 20, 10, 1, -10);
		
		int prev1 = en.getHp();
		int prev2 = pl.getHp();
		assertFalse(car1.isUsed());
		car1.useCard(pl, en);
		assertTrue(car1.isUsed());
		assertEquals(prev1 - 20, en.getHp());
		assertEquals(prev2 - 10, pl.getHp());
		
	}
	
	
	
	@Test
	public void testGetRealTarget() {
		car1 = new DynamicCard("DeckLuzeeraT2", 01, 20, 10, 1, 0);
		assertEquals(car1.getRealTarget(pl, en), "Ez Dago");
		
		//Target 
		car1 = new DynamicCard("DeckLuzeeraT2", 01, 20, 10, 1, 2);
		assertEquals(car1.getRealTarget(pl, en), en.getInstanceSring());
		
		car1 = new DynamicCard("DeckLuzeeraT2", 01, 20, 10, 1, 3);
		assertEquals(car1.getRealTarget(pl, en), en.getInstanceSring());
		
		car1 = new DynamicCard("DeckLuzeeraT2", 01, 20, 10, 1, 4);
		assertEquals(car1.getRealTarget(pl, en), en.getInstanceSring());
		
		car1 = new DynamicCard("DeckLuzeeraT2", 01, 20, 10, 1, 6);
		assertEquals(car1.getRealTarget(pl, en), en.getInstanceSring());
		
		car1 = new DynamicCard("DeckLuzeeraT2", 01, 20, 10, 1, -1);
		assertEquals(car1.getRealTarget(pl, en), en.getInstanceSring());
		
		car1 = new DynamicCard("DeckLuzeeraT2", 01, 20, 10, 1, -2);
		assertEquals(car1.getRealTarget(pl, en), en.getInstanceSring());
		
		car1 = new DynamicCard("DeckLuzeeraT2", 01, 20, 10, 1, -4);
		assertEquals(car1.getRealTarget(pl, en), en.getInstanceSring());
		
		//Owner
		car1 = new DynamicCard("DeckLuzeeraT2", 01, 20, 10, 1, -3);
		assertEquals(car1.getRealTarget(pl, en), pl.getInstanceSring());
		
		
		//Owner eta Target
		car1 = new DynamicCard("DeckLuzeeraT2", 01, 20, 10, 1, -10);
		System.out.println(pl.getInstanceSring() + " eta " + en.getInstanceSring());
		String comb = pl.getInstanceSring() + " eta " + en.getInstanceSring();
		assertEquals(car1.getRealTarget(pl, en), comb);
		
	}

	@Test
	public void testDynamicCard() {
		assertNotNull(car1);
		assertNotNull(car2);
	}

}
