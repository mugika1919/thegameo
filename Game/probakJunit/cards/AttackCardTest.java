package cards;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import entity.Enemy;
import entity.Player;

public class AttackCardTest {
	
	Enemy en;
	Player pl;
	
	AttackCard car1N, car2Z, car3P, car4ZP;
	@Before
	public void setUp() throws Exception {
		car1N = new AttackCard("Kolpe", 1101, 6, 1, 1, 0, 0);
		car1N.setDescription("Egin 6 puntuko mina");
		
		car2Z = new AttackCard("Ukabilkada", 1104, 0, 0, 0, 2, 2);
		car2Z.setDescription("Zaurgarri 2 aplikatu");
		
		car3P = new AttackCard("Pozoitu", 2105, 0, 0, 0, 1, 2);
		car3P.setDescription("Pozoia 2 aplikatu");
		
		car4ZP = new AttackCard("Pozoitu eta zauritu", 2105, 0, 0, 0, 3, 2);
		car4ZP.setDescription("Pozoia eta zaurgarri 2 aplikatu");
		en = new Enemy("ETSAI1", 001, 50, 0, null);
		en = en.copyButRandomized();
		pl = Player.getMyPlayer();
		pl.updateRound();
	}

	@After
	public void tearDown() throws Exception {
		car1N = null;
		car2Z = null;
		car3P = null;
		en = null; 
		pl.reset();
		pl = null;
	}

	@Test
	public void testCloneCard() {
		this.ondoKlonatua(car1N, car1N.cloneCard());
		car1N.use();
		this.ondoKlonatua(car1N, car1N.cloneCard());
		this.ondoKlonatua(car2Z, car2Z.cloneCard());
		this.ondoKlonatua(car3P, car3P.cloneCard());
		
	}
	
	private void ondoKlonatua(Card pOrig, Card pClone) {
		assertNotEquals(pOrig, pClone);
		assertEquals(pOrig.getId(), pClone.getId());
		assertEquals(pOrig.getName(), pClone.getName());
		assertEquals(pOrig.getDesc(), pClone.getDesc());
		assertEquals(pOrig.getPower(), pClone.getPower());
		assertEquals(pOrig.getPrice(), pClone.getPrice());
		assertEquals(pOrig.getEnergyCost(), pClone.getEnergyCost());
		assertEquals(pOrig.getProbability(), pClone.getProbability());
		assertFalse(pOrig.cloneCard().isUsed());
		assertFalse(pClone.isUsed());
	}
	
	@Test
	public void testUseCard() {
		pl.updateRound();
		car1N.useCard(null, null);
		car1N.useCard(null, en);
		car1N.useCard(en, null);
		int prev = en.getHp();
		car1N.useCard(pl, en);

		assertEquals((prev-6), en.getHp());
		prev = en.getHp();
		assertTrue(car1N.isUsed());
		
		car1N.useCard(pl, en);
		assertEquals(prev, en.getHp());
		
		car1N.reset();
		car1N.useCard(pl, en);
		car1N.reset();
		car1N.useCard(pl, en);
		
		assertEquals(prev - 6*2, en.getHp());
		prev = en.getHp();
		
		car1N.reset();
		car1N.useCard(pl, en);
		assertEquals(prev, en.getHp());
	}
	
	@Test
	public void testUseSpecial() {
		int prev = en.getHp();
		car1N = new AttackCard("Kolpe", 1101, 6, 0, 0, 0, 0);
		car1N.setDescription("Egin 6 puntuko mina");
		
		//Zaurigarri:
		
				assertEquals(en.getStatus(),"Efektuak: EZ");
				car1N.useCard(pl, en);
				car1N.reset();
				assertEquals(en.getHp(), prev - 6);
				
				prev = en.getHp();
				car2Z.useCard(pl, en);
				assertNotEquals(en.getStatus(),"Efektuak: EZ");
				assertEquals(en.getHp(), prev);
				
				car1N.useCard(pl, en);
				car1N.reset();
				assertEquals(en.getHp(), prev - 9);
				en.resetEfects();	
		
		//Pozoia:
		
				assertEquals(en.getStatus(),"Efektuak: EZ");
				prev = en.getHp();
				car3P.useCard(pl, en);
				assertNotEquals(en.getStatus(),"Efektuak: EZ");
				assertEquals(en.getHp(), prev);
				en.updateRound();
				
				assertEquals(en.getHp(), prev - 2);
				prev = en.getHp();
				en.updateRound();
				
				assertEquals(en.getHp(), prev - 1);
				
				en.resetEfects();

		//Zaurgarri eta pozoia:
				assertEquals(en.getStatus(),"Efektuak: EZ");
				
				car4ZP.useCard(pl, en);
				assertNotEquals(en.getStatus(),"Efektuak: EZ");
				
				en.resetEfects();
				
				
	}

	@Test
	public void testTranslateSpecial() {
		car1N.useCard(pl, en);
		assertEquals(en.getStatus(),"Efektuak: EZ");
		
		car1N = new AttackCard("Kolpe", 1101, 1, 1, 1, 120123, 1201);
		car1N.useCard(pl, en);
		assertEquals(en.getStatus(),"Efektuak: EZ");
		
		car1N = new AttackCard("Kolpe", 1101, 1, 1, 1, -41522, 1201);
		car1N.useCard(pl, en);
		assertEquals(en.getStatus(),"Efektuak: EZ");		
	}
	
	@Test
	public void testGetRealTarget() {
		assertEquals(car1N.getRealTarget(pl, en), "Etsaia: " + en.getName());
		assertEquals(car2Z.getRealTarget(pl, en), "Etsaia: " + en.getName());
		assertEquals(car3P.getRealTarget(en, en), "Etsaia: " + en.getName());
		
		assertEquals(car2Z.getRealTarget(en, pl), "Jokalaria: " + pl.getName());
		assertEquals(car2Z.getRealTarget(pl, pl), "Jokalaria: " + pl.getName());
		
		assertEquals(car2Z.getRealTarget(null, null), "Ez dago");
		
	}

	@Test
	public void testAttackCard() {
		assertNotNull(car1N);
		assertNotNull(car2Z);
		assertNotNull(car3P);
		assertNotNull(car4ZP);
	}

}
