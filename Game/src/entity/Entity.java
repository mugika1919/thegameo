package entity;

import game.TextBox;

public abstract class Entity{
	
	private int hp[] = {3, 3}; //{Hp at moment, Max hp};
	
	private int defense[] = {0, 0};//{Block at moment, Block for ever}
	
	private String izena;
	// private String descr; //TODO NO SE USA valor eliminacion
	
	//Status:
	private int veneno = 0;
	private double vulnerable[] = {0, 0.5}; //{por cuantos turnos activado, en cuanto se incrementa el da�o que le hacen}
	// private int sangrado[] = {0, 1}; //{0/1 activado o desactivado, nivel de sangrado (Cuantos puntos de da�o se hacen)}

	protected Entity(String pIzena, int pHp) {
		this.izena = pIzena;
		if(pHp <= 0) {
			this.hp[1] = 0;
		}else {
			this.hp[1] = pHp;
		}
		this.hp[0] = this.hp[1];
	}
	protected Entity(String pIzena, int pHp, int pDefense) {
		this(pIzena, pHp);
		if(pDefense <= 0) {
			this.defense[1] = 0;
		}else {
			this.hp[1] = pDefense;
		}
		this.defense[0] = this.hp[1];
		
	}
	//Getters
	public int getHp() {
		return this.getHp(false);
	}
	protected int getHp(boolean pMax) {
		int pos = 0;
		if(pMax) {
			pos = 1;
		}
		return this.hp[pos];
	}
	protected void setMaxHP(int pHP){
		if(0 < pHP) {
			this.hp[0] = pHP;
			this.hp[1] = pHP;
		}else {
			TextBox.getBoxDev().addText("Entity-setMaxHP: Ezin izan da HP maximoa aldatu pHP("+ pHP +") <= 0 delako.");
		}
	}
	public int getMaxHp() {
		return this.hp[1];
	}
	
	protected void updateMaxHP(int pHP){
		this.hp[1] = pHP;
		if(this.hp[0]>pHP) {
			this.hp[0] = pHP;
		}
	}
	
	protected int getDefense() {
		return this.getDefense(false);
	}
	protected int getDefense(boolean pMax) {
		int pos = 0;
		if(pMax) {
			pos = 1;
		}
		return this.defense[pos];
	}
	public String getName() {
		return this.izena;
	}
	
	//Setters
	public void changeName(String pName) {
		this.izena = pName;
	}
	/*//TODO No se usa
	public void addDescription(String pDescrip) {
		this.descr = pDescrip;
	}
	*/
	public void heal(int pHeal) {
		/** 
		 * FUNTZIOA: Unitatearen bizitza kantitateari puntu gehiago gehituko dio metodoa, baina ez da
		 * 	bere maximotik pasatuko. Beraz jokalaria sendatu egingo da baina maximora heldu arte bakarrik.
		 * ERABILPENA: Unitatea sendatu nahi denean.
		 * Input: Bizitza kantitatea.
		 * OutPut:--
		*/
		if(this.hp[1] - this.hp[0] - pHeal < 0) {
			this.hp[0] = this.hp[1];
		}else {
			this.hp[0] += pHeal;
		}
	}
	public void increaseMaxHp(int pMaxHp) {
		/** 
		 * FUNTZIOA: Unitatearen bizitza maximoari bizitza kantitate bat gehituko edo kenduko zaio. Kenketa jokalaria hilko balu orduan
		 * 				haren maxHp-ren balioa 1 era jarriko du.
		 * ERABILPENA: Energia maximoa aldatu nahi denean.
		 * Input: Gehitu(+) edo kendu(-) nahi den kantitatea.
		 * OutPut:--
		*/
		if(this.hp[1] + pMaxHp <= 0) {
			this.hp[1] = 1;

		}else {
			this.hp[1] = this.hp[1] + pMaxHp;
		}
		this.heal(0);
		
	}
	
	public void increaseDefense(int pDefense) {
		/** 
		 * FUNTZIOA: Unitatearen defentsari kantitate bat gehituko zaio.
		 * ERABILPENA: Defentsa gehitu nahi denean.
		 * Input: Defentsa kantitatea.
		 * OutPut:--
		*/
		this.defense[0] = this.defense[0] + pDefense; 
	}
	
	public void increaseMaxDefense(int pDefense) {
		/** 
		 * FUNTZIOA: Unitatearen defentsa maximoari kantitate bat gehituko zaio.
		 * ERABILPENA: Defentsa maximoa handitu nahi denean.
		 * Input: Defentsa kantitatea.
		 * OutPut:--
		*/
		this.defense[1] = this.defense[1] + pDefense;
	}
	
	public void getHurt(int pDamage) { //e
		/** 
		 * FUNTZIOA: Unitate bateri erasotzean bere bizitza kantitatea txikituko da. Zaurgarri bada kantitatea handitu egingo da, 
		 * 	baina unitateari min egin baino lehen bere defentsari egingo zaio min. Defentsa min kantitatea baino handiagoa bada ez du 
		 * 	unitatearen bizitza aldatuko, defentsa txikiagoa bada bizitzari (pDmage-defentsa) kenduko zaio.
		 * ERABILPENA: Borroketan erasotzean.
		 * Input: Egindako erasoaren indarra. 
		 * OutPut:--
		*/
		int damage = pDamage;
		if(vulnerable[0] > 0) {
			damage = damage + Math.round((float)(damage * vulnerable[1]));
		}
		this.defense[0] = this.defense[0] - damage;
		if(this.defense[0] < 0) {
			this.hp[0] = this.hp[0] + this.defense[0];
			this.defense[0] = 0;
		}
		
		if(this.hp[0] < 0) {
			this.hp[0] = 0;
		}
	}
	
	public abstract boolean enoughEnergy(int pEnergy);
	
	public abstract void spendEnergy(int pEnergy);
	
	public void updateRound() {
		/** 
		 * FUNTZIOA: Unitatearen defentsa bere hasierako balioan jarri. 
		 * ERABILPENA: Txanda hasieran.
		 * Input: --
		 * OutPut:--
		*/
		this.defense[0] = this.defense[1];
		this.updateStatus();
	}
	
	public boolean isDead() {
		/** 
		 * FUNTZIOA: Unitatea bizik dagoen ala ez konprobatuko du.
		 * ERABILPENA: Loop bakoitzean.
		 * Input: --
		 * OutPut: Bizirik badago false, bestela true.
		*/
		return this.hp[0] <= 0;
	}
	
	
	
	//SET STATUS METHODS
	public void setVeneno(int pInt) {
			if(pInt > 0) {
				this.veneno = this.veneno + pInt;
			}
		}
		
	public void setVulnerable(int pTurnos) {
			if(pTurnos > 0) {
				this.vulnerable[0] = this.vulnerable[0] + pTurnos;
			}
		}
	public void setVulnerable(int pTurnos, int pCantidad) {
			this.setVulnerable(pTurnos);
			if(pCantidad > 0) {
				this.vulnerable[1] =  pCantidad;
			}
		}
		/*
	public void setSangrado() {
			this.sangrado[0] = 1;
		}
		public void setSangrado(int pDa�oPorSangrado) {
			if(pDa�oPorSangrado > 0) {
				this.sangrado[0] = 1;
				if(this.sangrado[1] < pDa�oPorSangrado) {
					this.sangrado[1] = pDa�oPorSangrado;
				}
			}
		}
		*/
	
	private void updateStatus() {
		/** 
		 * FUNTZIOA: Unitatearen egoerak egunetzen ditu, txanda bakoitzean -1 egingo dira. Gainera pozoituta badago
		 * 	bizitza murriztuko da.
		 * ERABILPENA: Txanda bakoitzean.
		 * Input: --
		 * OutPut:--
		*/
		if(this.veneno > 0) {
			this.hp[0] = this.hp[0] - veneno; //Da�o penetrande
			this.veneno = this.veneno - 1;
		}
		/*
		if(this.sangrado[0] > 0) {
			this.hp = this.hp - sangrado[1];
		}
		*/
		if(this.vulnerable[0] > 0) {
			this.vulnerable[0] = this.vulnerable[0] - 1.0;
		}			
	}
	
	public void resetEfects() {
		/** 
		 * FUNTZIOA: Egoeren balioa hasierakora bueltatuko da.
		 * ERABILPENA: Borroken amaieran.
		 * Input: --
		 * OutPut:--
		*/
		this.veneno = 0;
		this.vulnerable[0] = 0;
	}
	
	
	//Info Methods
	public abstract String getInfo(boolean pBattle, int pResol);

	public String getHpBar(int pResolut) {
		/** 
		 * FUNTZIOA: Unitatearen bizitza kantitate maximoa normalizatzen da barra bat lortzeko. Gero 
		 * 	Dena #-z betetzen da. Barra lortuta momentua daukan energiaren arabera - char-aren gaitik 
		 * 	ordezkatuko da.
		 * ERABILPENA: Borroketan infomazioa idazteko.
		 * Input: Normalizatzeko erabiltzen den zenbakia.
		 * OutPut: Normalizatutako barra.
		*/
		int batezBesteko = Math.round((this.hp[0]*pResolut)/this.hp[1]);
		String str = "HP:[";
		String block = new String(new char[batezBesteko]).replace("\0", "#");
		str = str + block;
		
		block = new String(new char[(pResolut-batezBesteko)]).replace("\0", "-");
		str = str + block + "]"+this.hp[0]+"/"+this.hp[1];
		return str;
	}
	
	public String getBlock() {
		/** 
		 * FUNTZIOA: Uniateren defentsa kantitatea string moduan erakuzteko prest dagoen str-n gordeko du
		 * ERABILPENA: Defentsa kantitatea erakusteko borroketan.
		 * Input: --
		 * OutPut: Defentsa kantitatea string bezala.
		*/
		String str = "Block: " + this.defense[0];
		if((this.defense[0] == 0 && this instanceof Player) || (this.defense[0] != 0 && this instanceof Enemy)) {
			str = str + " �!";
		}
		
		return str;
	}
	
	public String getStatus() {
		/** 
		 * FUNTZIOA: Unitateak edonolako egoerarik daukan ikusi eta string batean gordeko du lortutako
		 * 	infomazioa. 
		 * ERABILPENA: Borroketako loop bakoitzean.
		 * Input: --
		 * OutPut: String moduan untitatearen egoeraren infomazioa.
		*/
		boolean efekturik = false;
		String str = "Efektuak: ";
		if(this.veneno > 0) {
			str = str + String.format("Pozoia: %d; ", this.veneno); //TODO Esto no se si es asi
			efekturik = true;
		}
		if(this.vulnerable[0] > 0) {
			str = str + String.format("Ahul: %d Txandaz, +%dehun. Jasotako mina; ", (int)(this.vulnerable[0]), Math.round(this.vulnerable[1]*100));
			efekturik = true;
		}
		if(!efekturik) {
			str = "Efektuak: EZ";
		}
		/*
		if(this.sangrado[0] != 0) {
			str = str + String.format("Odoletan: %d Jasoko den mina", this.sangrado[1]);
		}
		*/
		
		return str;
	}
	
	public abstract String getInstanceSring();
	
}
