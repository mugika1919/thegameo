package entity;

import java.util.Random;

import cards.AttackCard;
import cards.Card;
import cards.CardList;
import cards.DefendCard;
import game.Battle;
import game.Mapa;
import game.TextBox;

public class Enemy extends Entity {
	
	//private Entity targets[] = {this, null};
	private int id = 0;
	
	private boolean unique = true;
	
	private int hpVariation[] = {0, 0}; // {HP Minimo, Variacion de HP}
	
	private static int avaliableId = 0;
	
	private CardList actionCards = null;
	
	private int identNumber = 0;
	
	private Card nextAtack = null;
	
	
	//Descripcion de Enemigo
	public Enemy(String pIzena, int id, int pHpMin, int pHpVariation, CardList pCardList) {
		super(pIzena, -1);
		
		this.id = id;
		
		if(pHpVariation < 0) {
			this.hpVariation[1] = 0;
		}else {
			this.hpVariation[1] = pHpVariation;
		}
		this.hpVariation[0] = pHpMin;
		
		if(pCardList != null && pCardList.size() != 0) {
			this.actionCards = pCardList;
		}else {
			CardList cardList2 = new CardList();
			Card ecard101= new AttackCard("Atzaparkada I", 101, 4, 1, 1, 0, 0);
	        ecard101.setDescription("Kolpeatu 4");
	        ecard101.setProbability(100);
	        cardList2.addCard(ecard101);
			this.actionCards = cardList2;
		}
	}
	
	
	public Enemy copyButRandomized() {
		/** 
		 * FUNTZIOA: 
		 * ERABILPENA: 
		 * Input: 
		 * OutPut: 
		*/
		
		Random r = new Random(); //System.currentTimeMillis()
		int randHp = this.hpVariation[0];
		if(this.hpVariation[1] > 0) {
			randHp = r.nextInt(this.hpVariation[1]) + this.hpVariation[0];
		}
		Enemy enemy = new Enemy(this.getName(),this.id, this.hpVariation[0], this.hpVariation[1], this.actionCards);
		enemy.setMaxHP(randHp);
		enemy.unique = false;
		enemy.nextAtack = enemy.getRandomAttack();
		Enemy.avaliableId = Enemy.avaliableId + 1;
		enemy.identNumber = Enemy.avaliableId;
		
		return enemy;
	}
	
	public Enemy copy() {
		/** 
		 * FUNTZIOA: 
		 * ERABILPENA: 
		 * Input: 
		 * OutPut: 
		*/
		Enemy enemy = null;
		if(!this.unique) {
			enemy = new Enemy(this.getName(),this.id, this.hpVariation[0], this.hpVariation[1], this.actionCards);
			enemy.setMaxHP(this.getHp());
			enemy.unique = false;
			enemy.nextAtack = enemy.getRandomAttack();
			Enemy.avaliableId = Enemy.avaliableId + 1;
			enemy.identNumber = Enemy.avaliableId;
		}else {
			enemy = new Enemy(this.getName(),this.id, this.hpVariation[0], this.hpVariation[1], this.actionCards);
			enemy.setMaxHP(this.hpVariation[0]);
			enemy.unique = false;
			enemy.nextAtack = enemy.getRandomAttack();
			Enemy.avaliableId = Enemy.avaliableId + 1;
			enemy.identNumber = Enemy.avaliableId;
		}
		return enemy;
	}
	
	private Card getRandomAttack() {
		/** 
		 * FUNTZIOA: 
		 * ERABILPENA: 
		 * Input: 
		 * OutPut: 
		*/
		Card ema = null;
		Random r = new Random();//System.currentTimeMillis()
		ema = this.actionCards.getCardByProbability(r.nextInt(this.actionCards.getTotalProbability()));
		return ema;
	}
	
	
	//public void 
	//TODO 	borrar o no borrar esa es la cuestion.
	public boolean isUnique() {
		/** 
		 * FUNTZIOA: Etsaiaren unique balioa lortzen du.
		 * ERABILPENA: Etsaia unique bada jakiteko. 
		 * Input: --
		 * OutPut: Unique boolear balioa.
		*/
		return this.unique;
	}
	
	public boolean hasSameId(int pId) {
		/**  
		 * FUNTZIOA: Bi etsaren Id-ak konparatuko ditu berdinak diren jakiteko.
		 * ERABILPENA: Bi etsaien id konparatzeko.
		 * Input: Beste etsaia baten Id-a.
		 * OutPut: Id berdina badauka true, bestela false.
		*/
		return this.id == pId;
	}
	
	public boolean hasSameId(Enemy pEnemy) {
		/** 
		 * FUNTZIOA: Bi etsaren Id-ak konparatuko ditu berdinak diren jakiteko.
		 * ERABILPENA: Bi etsaien id konparatzeko.
		 * Input: Beste etsaia bat.
		 * OutPut: Id berdina badauka true, bestela false.
		*/
		return pEnemy.hasSameId(this.id);
	}
	
	public int getMota() {
		/** 
		 * FUNTZIOA: Etsaiek daukaten id-ak kodifikatuta daude eta id-aren milakoa
		 * 	bere mota zein den identifikatzen du. Milako zenbaki hori  lortzeko %1000/100 algoritmoa erabiltzen dugu.
		 * ERABILPENA: Etsaiaren mota lortzeko.
		 * Input: --
		 * OutPut: Etsaiaren mota.
		*/
		return (this.id%1000)/100;
	}
	
	public void restartEnemyQuantity() {
		/** 
		 * FUNTZIOA: avaliableId-k borrokan dauden etsaien ID-a dauka. Id hau ez da 
		 * 	etsai bakoitsak daukan id-a, 1-etik 3-ra doan zenbakia da baizik. Etsaia hiltzerakoan Id kantitatea txikitu egingo da.
		 * ERABILPENA: Etsai bat borrokan hiltzen denean. 
		 * Input: --
		 * OutPut: --
		*/
		Enemy.avaliableId = 0;
	}
	/*
	public void setTargets(Entity[] pEnt) {
		if(pEnt.length == this.targets.length) {
			this.targets = pEnt;
		}else {
			TextBox.getBoxDev().addText("Error Enemy/97: Ezin dira target-ak finkatu sartutako info. ez dator bat");
		}
	}
	*/
	
	//Battle Methods
	
	
	
	public void attack() {

		/** 
		 * FUNTZIOA: 
		 * ERABILPENA: 
		 * Input: 
		 * OutPut: 
		*/
		
		if(this.nextAtack != null) {
			this.nextAtack.useCard(this, Player.getMyPlayer());
			TextBox.getBox().addText(this.getInstanceSring() +" " + this.nextAtack.getName() + " erabili du "+ this.nextAtack.getRealTarget(this, Player.getMyPlayer()) + "n");
			this.nextAtack.reset();
		}else {
			TextBox.getBox().addText(this.getInstanceSring() +" ez du ezer ez egin.");
		}

		
	}
	
	public void spendEnergy(int pEnergy) { 
		//TODO 
		
	}
	
	public boolean enoughEnergy(int pEnergy) {
		return true; //TODO Balance de energia entre enemigos
		
	}
	
	@Override
	public boolean isDead() {
		/** 
		 * FUNTZIOA: Etsaia bizirik badagoen ikusiko da. Bizitza 0 edo gutxiago bada ez dago bizirik.
		 * ERABILPENA: Borroketan etsai bizirk dagoen jakiteko.
		 * Input: --
		 * OutPut: Bizirik badago false, bestela true.
		*/
		boolean isdead = super.isDead();
		
		if(isdead && this.identNumber != 0) {
			Enemy.avaliableId = Enemy.avaliableId - 1;
			this.identNumber = 0;
		}
		return isdead;
	}
	
	public void updateNextAtack() {
		/** 
		 * FUNTZIOA: Etsaiaren hurrengo erasoa aleatorioki aldatuko da.
		 * ERABILPENA: Txanda bakoitzean.
		 * Input: --
		 * OutPut: --
		*/
		this.nextAtack = this.getRandomAttack();
	}
 
	/*
	public void defendMyself(int pDefend) throws Exception{
		if(pDefend <= 0) {
			Exception ex = new Exception("Defentsa argumentua negatiboa ezin da izan");
			throw ex;
		}
		this.defense[0] = this.defense[0] + pDefend;
	}
	*/

	
	//INFO METHODS
	
	
	public String getInstanceSring() {
		/** 
		 * FUNTZIOA: 
		 * ERABILPENA: 
		 * Input: 
		 * OutPut: 
		*/
		String str = "";
		if(Battle.getCurrentBatle() != null) {
			str = this.identNumber + ". " ;
		}
		str = str + "Etsaia: " + this.getName();
		
		return str;
	}
	
	public String getNextAtackInfo() {
		/** 
		 * FUNTZIOA: Etsai baten hurrengo mugimenduren informazioa lortuko du. Informazio hori 
		 * 	mugimenduren informazioa eta bere efektua nork jasango duen osatuko dute.
		 * ERABILPENA: Borroketan Etsaiaren mugimenduak jakiteko.
		 * Input: --
		 * OutPut: Etsaiaren hurrengo mugimendua.
		*/
		String ema = "Hur. Eras.: ";
		if(this.nextAtack != null) {
			ema = ema + this.nextAtack.getDesc() + "->" + this.nextAtack.getRealTarget(this, Player.getMyPlayer());
			
		}else {
			ema = ema + "Ezer";
		}
		return ema;
	}
	
	

	
	public String getInfo(boolean pBattle, int pResol) {
		/** 
		 * FUNTZIOA: 
		 * ERABILPENA: 
		 * Input: 
		 * OutPut: 
		*/
		String str = "";
		int len = 20;
		
		if(pBattle) {
			str = str + this.getHpBar(pResol);
			len = str.length();
			str = this.getInstanceSring() + "\n" + str + "\n";
			str = str + this.getNextAtackInfo() + "\n";
			str = str + this.getBlock() + "\n";
			str = str + Mapa.reduceString(this.getStatus(), len, true, false);
		}else {
			str = str + this.getInstanceSring() + "\n";
			if(this.unique) {
				str = str + "HP: " +this.hpVariation[0] + "~" + (this.hpVariation[0] + this.hpVariation[1]) + "; " + this.getBlock() + "\n";
			}else {
				str = str + "HP: " +this.getHp() + "/" + this.getHp(true) + "; " + this.getBlock() + "\n";
			}
				str = str + "Kartak:" + "\n";
				String[] cardsStr = this.actionCards.getCardsInfoAsSinpleString();
				int kont = 0;
				while(kont < cardsStr.length) {
					str = str + "+ " + cardsStr[kont] + "\n";
					kont = kont + 1;
				}
				str = str + Mapa.reduceString(("Probab. Guztira: "+ this.actionCards.getTotalProbability()), 20, false, false);
			
		}
		
		
		
		
		return str; //TODO
	}

}
