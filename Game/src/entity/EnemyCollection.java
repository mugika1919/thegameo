package entity;
import java.util.Random;

import cards.Card;
import cards.CardCollection;
import cards.CardList;
import game.TextBox;


public class EnemyCollection {
	
	private int[] enemyCount = new int[3];
	private static EnemyCollection myEnemyCollection = null;
	private EnemyList lista;
	
	
	//Eraikitzaile
	private EnemyCollection() {
		this.lista = new EnemyList();
		this.createAllEnemies();
	}
	//Geters
	public static EnemyCollection getEnemyCollection() {
		if(EnemyCollection.myEnemyCollection == null) {
			EnemyCollection.myEnemyCollection = new EnemyCollection();
		}
		return EnemyCollection.myEnemyCollection;
	}
	public int motaKop() {
		return this.enemyCount.length;
	}
	public int enemyNumber() {
		return this.lista.size();
	}
	//Metodoak
	public Enemy searchEnemyById(int pId) {
		/** 
		 * FUNTZIOA: Id bat kontuan izanda, listan id berdina duen etsaia aurkituko du.
		 * ERABILPENA: Etsai bat id-z aurkitu nahi denean.
		 * Input: Etsaiaren Id-a.
		 * OutPut: Aurkitutako etsaia.
		*/
		return this.lista.searchEnemyById(pId);
  	}
	
	public void addEnemyToCollection(Enemy pEnemy) {
		/** 
		 * FUNTZIOA: Etsai bat sartu baino lehen listan badagoen ala ez ikusten da. Listan badago exception bat sortuko da, bestela 
		 *  	etsaia lista sartuko da. Etsaia listan sartzen bada, gero motaren arabera mota horretako kontagailua inkrementatu egingo da.
		 *  	Mota ez bada existitzen exception bat sortuko da.
		 * ERABILPENA: Etsaien berri bat jokoan sartu nahi denean.
		 * Input: Sartu nahi den etsaia.
		 * OutPut:-- 
		*/
		try {
			if(this.lista.thereIsEnemyWithSameId(pEnemy)) {
				throw new Exception("Ezin izan da etsaia gehitu id berdina duen beste etsai bat baitago");
			}else {	
					int mota = pEnemy.getMota();
					if(mota >= 0 && mota < this.enemyCount.length) {
						this.enemyCount[mota]++;
					}else {
						throw new Exception("Ezin izan da mota finkatu");
					}
				
				this.lista.addEnemy(pEnemy);
			}
		}catch(Exception e) {
			TextBox.getBoxDev().addText("EnemyCollection: " + e.getMessage());
		}
	}
	
	public Enemy getCopRandomEnemy(int pMota) throws Exception{
		/** 
		 * FUNTZIOA: Mota batetako etsaia aleatorio bat autatuko du listatik. Mota horretako etsairik ez badago exception bat botako du.
		 * 	Autatutako etsaiari balio aleatorio batzuk jarriko zaizkio, rango batzuen bitartean.
		 * ERABILPENA: Motaren arabera etsaiak sortu nahi direnenan.
		 * Input: Nahi den mota.
		 * OutPut:-- 
		*/
		//TODO EXCEPTION
		Enemy myEnemy = null;
		Random r = new Random();
		int m = pMota;
		int saiakerak = 100;
		if(this.enemyCount[m] != 0) {
			while(myEnemy == null && saiakerak > 0)
			{
				if(m < 0 || m >= this.enemyCount.length) {
					m = 0;
				}
				int tarte = r.nextInt(this.enemyCount[m]) + 1;
				
				myEnemy = this.searchEnemyById(m*100 + tarte);
				saiakerak = saiakerak - 1;
			}
		}else {
			throw new Exception("EnemyCollection: Ez da mota horretako etsairik existitzen");
		}
		if(myEnemy != null) {
			myEnemy = myEnemy.copyButRandomized();
		}
		return myEnemy;
		
	}
	public Enemy getRandomizedEnemyById(int pId) {
		/** 
		 * FUNTZIOA: Etsaien listaik Id-z bilatuko da etsaia eta gero rango batzuen bitartean 
		 * 	balio aleatorio berri batzuk izango dituzte.
		 * ERABILPENA: Borroketan id berdineko etsaiek beti berdinak ez izateko.
		 * Input: Etsaiaren Id-a
		 * OutPut: Sortu den etsaia.
		*/
		Enemy en = this.searchEnemyById(pId);
		if(en != null) {
			en = en.copyButRandomized();
		}
		
		return en;
	}
	
	private void createAllEnemies() {
		/** 
		 * FUNTZIOA: Jokoan zehar agertuko diren etsai guztiak sortuko dira listan sartzeko.
		 * ERABILPENA: Haiseran sortuko diren etsai guztiak.
		 * Input: --
		 * OutPut:-- 
		*/

        CardCollection collection = CardCollection.getCardCollection();
        Card card1 = collection.searchCardById(101).cloneCard();
        Card card2 = collection.searchCardById(301).cloneCard();


        Card card3 = collection.searchCardById(102).cloneCard();
        card3.setProbability(5);
        Card card4 = collection.searchCardById(303).cloneCard();
        card4.setProbability(4);
        Card card5 = collection.searchCardById(203).cloneCard();
        card5.setProbability(1);
        
        Card nagia1 = collection.searchCardById(912);
        nagia1.setProbability(9);   
        Card nagia2 = collection.searchCardById(911);
        nagia2.setProbability(1);
        
        Card lizuna1 = collection.searchCardById(921);
        lizuna1.setProbability(3);
        Card lizuna2 = collection.searchCardById(103);
        lizuna2.setProbability(4);
        Card lizuna3 = collection.searchCardById(303);
        lizuna3.setProbability(3);
        
        Card gula1 = collection.searchCardById(931);
        gula1.setProbability(3);
        Card gula2 = collection.searchCardById(304);
        gula2.setProbability(7);
        
        Card zeken1 = collection.searchCardById(941);
        zeken1.setProbability(1);
        Card zeken2 = collection.searchCardById(305);
        zeken2.setProbability(3);
        
        Card bekaiz1 = collection.searchCardById(1205);
        bekaiz1.setProbability(1);
        Card bekaiz2 = collection.searchCardById(11103);
        bekaiz2.setProbability(1);
        Card bekaiz3 = collection.searchCardById(1105);
        bekaiz3.setProbability(1);
        Card bekaiz4 = collection.searchCardById(-1);
        bekaiz4.setProbability(1);
        
        Card haserre1 = collection.searchCardById(961);
        haserre1.setProbability(1);
        
        



        CardList enemyCards1 = new CardList();
        enemyCards1.addCard(card1);
        enemyCards1.addCard(card2);
        Enemy enemy1 = new Enemy("Zizarria", 1, 19, 11, enemyCards1);
        //enemy1.addDescription("Zizarriak harriz osatutako zizareak dira. Normalean oso defentsiboak izaten dira, eta ez dute gauza handirik egiten.");
        this.addEnemyToCollection(enemy1);
        
        CardList enemyCards2 = new CardList();
        enemyCards2.addCard(card3);
        enemyCards2.addCard(card4);
        enemyCards2.addCard(card5);
        Enemy enemy2 = new Enemy("Armikaitzak", 2, 25, 15, enemyCards2);
        //enemy2.addDescription("Elektrizitatez kargatutako armiarma hauek nahiko osasuntsuak dira normalean, nahiz eta arrisku handirik ez suposatu, kontuz ibili behar zara hauekin.");
        this.addEnemyToCollection(enemy2);
        
        CardList Nagikeri = new CardList();
        Nagikeri.addCard(nagia1);
        Nagikeri.addCard(nagia2);
        Enemy Nagikeria = new Enemy("Nagikeri",-1,200,150,Nagikeri);
        this.addEnemyToCollection(Nagikeria);
        
        CardList Lizunkeri = new CardList();
        Lizunkeri.addCard(lizuna1);
        Lizunkeri.addCard(lizuna2);
        Lizunkeri.addCard(lizuna3);
        Enemy Lizunkeria = new Enemy("Lizunkeri",-2,125,75,Lizunkeri);
        this.addEnemyToCollection(Lizunkeria);
        
        CardList Gula = new CardList();
        Gula.addCard(gula1);
        Gula.addCard(gula2);
        Enemy gula = new Enemy("Gula",-3,200,100,Gula);
        this.addEnemyToCollection(gula);
        
        CardList Zekenkeri = new CardList();
        Zekenkeri.addCard(zeken1);
        Zekenkeri.addCard(zeken2);
        Enemy Zekenkeria = new Enemy("Zekenkeri",-4,250,100,Zekenkeri);
        this.addEnemyToCollection(Zekenkeria);
        
        CardList Bekaizkeri = new CardList();
        Bekaizkeri.addCard(bekaiz1);
        Bekaizkeri.addCard(bekaiz2);
        Bekaizkeri.addCard(bekaiz3);
        Bekaizkeri.addCard(bekaiz4);
        Enemy Bekaizkeria = new Enemy("Bekaizkeri",-5,250,150,Bekaizkeri);
        this.addEnemyToCollection(Bekaizkeria);
        
        CardList Haserre = new CardList();
        Haserre.addCard(haserre1);
        Enemy Haserrea = new Enemy("Haserre",-6,400,200,Haserre);
        this.addEnemyToCollection(Haserrea);
        
        CardList Harrokeri = new CardList();
        Harrokeri.addCard(nagia2);
        Harrokeri.addCard(lizuna1);
        Harrokeri.addCard(gula1);
        Harrokeri.addCard(zeken1);
        Harrokeri.addCard(haserre1);
        Enemy Harrokeria = new Enemy("Harrokeri",-7,500,250,Harrokeri);
        this.addEnemyToCollection(Harrokeria);
        
        

    }

}
