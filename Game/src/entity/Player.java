package entity;

import game.Mapa;
import game.TextBox;

public class Player extends Entity{
	
	private int positionX;
	private int positionY;

	private int energy[] = {3, 3}; //{Energy at moment, Max energy}
	
	private int money;
	private int job;
	
	private static Player myPlayer;
			
	
	//Eraikitzailea
	private Player() {
		
		super("Default", 100);
		this.positionX = 0;
		this.positionY = 0;
		this.money = 0; 
		this.job = 0 ; // 0 warrior / 1 rogue
	}
	
	//geters
	public static Player getMyPlayer() {
		
		if (myPlayer == null)
			myPlayer = new Player();
	
		return myPlayer;
	}
	
	public void reset() {
		Player.myPlayer = null;
	}
	
	public boolean InPos(int pX,int pY) {
		
		boolean eran = false;
		
		if (this.positionX == pX && this.positionY == pY)
			eran=true;
		
		return eran;
		
	}
	
	//geters
	public int getX() {
		return this.positionX;
	}
	
	public int getY() {
		return this.positionY;
	}
	public int getJob() {	
		return this.job;
	}
	public int getMoney() {
		return this.money;
	}
	//seters
	public void setJob(int pJob) {
		this.job = pJob;
		if(pJob == 0) {
			this.setMaxHP(100);
		}else if(pJob == 1) {
			this.setMaxHP(70);
		}
	}
	
	public void ChangePos(int pX,int pY) {
		
		this.positionX=pX;
		this.positionY=pY;
	}
	
	
	
	public void spendMoney(int pPrice) {
		/** 
		 * FUNTZIOA: Diru kantitate bat jokalriak daukan diruari kentzen zaio eta gero mezu bat 
		 * 	sartzen da textBoxe-an.
		 * ERABILPENA: Dendan dirua erabiltzen denean.
		 * Input: Erabiliko den dirua.
		 * OutPut: --
		*/
		TextBox mybox = TextBox.getBox();
		mybox.addText(pPrice+" diru erabili da.");
		this.money-=pPrice;
	}
	
	public void takeMoney(int pCost) {
		/** 
		 * FUNTZIOA: Emandako diru kantitatea jokalariari gehitzen zaio.
		 * ERABILPENA: Dirua lortzen denean.
		 * Input: Jokalariren dirua inkrementatu.
		 * OutPut:--
		*/
		this.money = this.money + pCost;
	}
	
	/*public void SetPosition(int pPosx, int pPosy) {
		this.positionX=pPosx;
		this.positionY=pPosy;
	}*/
	
	public void MoveLeft() {
		/** 
		 * FUNTZIOA: Jokalariren X posizioari -1 egiten zaio. Mugimendua egin baino lehen, posizio berrian gela bat badagoen
		 * 	jakin behar da, ez badago ezin izano da mugitu.
		 * ERABILPENA: Jokalaria ezkerrera mugitzean.
		 * Input: --
		 * OutPut:--
		*/
		
		TextBox mybox = TextBox.getBox();
		
		Mapa myMap=Mapa.getMyMapa();
		
		if (this.positionX-1>=0) {
			if (myMap.getArray()[this.positionY][this.positionX-1]=="Room" || myMap.getArray()[this.positionY][this.positionX-1]=="Visited" ) {
				mybox.addText("Jokalaria ezkerrera mugitu da.");
				this.positionX--;
			}
		}
	}
	
	public void MoveRight() {
		/** 
		 * FUNTZIOA: Jokalariren X posizioari +1 egiten zaio. Mugimendua egin baino lehen, posizio berrian gela bat badagoen
		 * 	jakin behar da, ez badago ezin izano da mugitu.
		 * ERABILPENA: Jokalaria eskuinera mugitzean.
		 * Input: --
		 * OutPut:--
		*/
		
		TextBox mybox = TextBox.getBox();
		
		Mapa myMap=Mapa.getMyMapa();
		
		if (this.positionX+1<=3) {
			if (myMap.getArray()[this.positionY][this.positionX+1]=="Room" || myMap.getArray()[this.positionY][this.positionX+1]=="Visited") {
				mybox.addText("Jokalaria eskuinera mugitu da.");
				this.positionX++;
			}
				
		}
	}
	
	public void MoveUp() {
		/** 
		 * FUNTZIOA: Jokalariren Y posizioari +1 egiten zaio. Mugimendua egin baino lehen, posizio berrian gela bat badagoen
		 * 	jakin behar da, ez badago ezin izano da mugitu.
		 * ERABILPENA: Jokalaria gora mugitzean.
		 * Input: --
		 * OutPut:--
		*/
	
		TextBox mybox = TextBox.getBox();
		
		Mapa myMap=Mapa.getMyMapa();
		
		if (this.positionY-1>=0) {
			if (myMap.getArray()[this.positionY-1][this.positionX]=="Room" || myMap.getArray()[this.positionY-1][this.positionX]=="Visited") {
				this.positionY--;
				mybox.addText("Jokalaria gora mugitu da.");
			}	
		}
    }

	public void MoveDown() {
		/** 
		 * FUNTZIOA: Jokalariren Y posizioari -1 egiten zaio. Mugimendua egin baino lehen, posizio berrian gela bat badagoen
		 * 	jakin behar da, ez badago ezin izano da mugitu.
		 * ERABILPENA: Jokalaria behera mugitzean.
		 * Input: --
		 * OutPut:--
		*/
		
		TextBox mybox = TextBox.getBox();
		
		Mapa myMap=Mapa.getMyMapa();
		
		if (this.positionY+1<=2) {
			if (myMap.getArray()[this.positionY+1][this.positionX]=="Room" || myMap.getArray()[this.positionY+1][this.positionX]=="Visited") {
				this.positionY++;
				mybox.addText("Jokalaria behera mugitu da.");
			}
		}
	}	
	
	//Aributos de Batalla
	
	
	
	
	@Override
	public boolean enoughEnergy(int pEnergy) {
		/** 
		 * FUNTZIOA: Karta bat erabiltzeko energia kantitate bat behar da. Metodo honek energia kantitate hori jokalariak erabili 
		 * 	dezakeen ala konprobatuko du.
		 * ERABILPENA: Karta bat erabiltzean.
		 * Input: Erabiltzen den energia.
		 * OutPut: Energia nahikoa badago true izango da, bestela false.
		*/
		boolean ema = true;
		if(this.energy[0] - pEnergy < 0){
			ema = false;
		}
		return ema;
	}
	
	@Override
	public void spendEnergy(int pEnergy) {
		/** 
		 * FUNTZIOA: Karta bat erabiltzean behar den energia kontuan izanda, kantitate hori jokalariaren energiari
		 * 	kenduko zaio. Ez badago energia nahikorik nahi den energia erabiltzeko, ez da ezer gertatuko.
		 * ERABILPENA: Karta bat erabiltzean.
		 * Input: Erabiltzen den energia.
		 * OutPut:--
		*/
		this.energy[0] = this.energy[0] - pEnergy;
	}
	
	@Override
	public void updateRound() {
		/** 
		 * FUNTZIOA: Jokalariaren energia kantitatea hasierako baliora jartzen du. Jokalariaren energia maximoa energy array-aren 
		 * 	1-garren posizioan dago eta momentu honetan daukan energia 0-garren posizioan. 
		 * ERABILPENA: Borroka batean jokalariaren txanda denean.
		 * Input: --
		 * OutPut:--
		*/
		super.updateRound();
		this.energy[0] = this.energy[1];
	}
	
	
	
	
	//TODO 
	//
	//el pResol no se usa crack, fiera, tifon, mastodonte
	//
	public String getInfo(boolean pBattle, int pResol) {
		/** 
		 * FUNTZIOA: Parametro bitartez sartutako boolearraren arabera jokalariaren informazio ezberdina bueltatuko du String batean. 7
		 * 			True bada orduan: bizitza barra, izena, energia barra, blokeoa, eta status-a
		 * 			False bada orduan: bizitza eta bizitza max, pozizioa mapan, energia maximoa
		 * ERABILPENA: 
		 * Input: boolean pBattle, int pResol
		 * OutPut: String
		*/
		//TODO
		String info = "";
		if(pBattle) {
			int len = 0;
			info = info + this.getHpBar(pResol) + "\n";
			len = info.length();
			info = Mapa.reduceString(this.getName()+": ", len, true, true) + info ;
			info = info + Mapa.reduceString(this.getEnergyBar() + " / " + this.getBlock(),len,true,true) + "\n";
			info = info + Mapa.reduceString(this.getStatus(), len, true, true) + "\n";
			
		}
		else {
			info = "/Stats/ \n";
			info = info + "---------------------------------------------" + "\n";
			info = info + "HP: "+this.getHp()+"/"+this.getHp(true)+";     X: "+(this.positionX+1)+" Y: "+(this.positionY+1)  + "\n";
			info = info + "\n";
			info = info + "Energia: " + this.energy[0] +"; Blokeo Konstantea: "+ this.getDefense(true) + "\n";
			info = info + "\n";	
		}
		return info;
	}
	
	public String getEnergyBar() {
		/** 
		 * FUNTZIOA: Jokalariren energia kantitatea, momentu honetan daukana, array batean bihurtzen du gero idazteko pantailan.
		 * 	Hasieran string bat sortzen da eta @ char-arekin ordezkatzen da. @ bakoitzak energia puntu bat adierazten du beraz, 
		 * 	idatzi behar diren @ kantitatea lortzeko (energia maximoa - momentuko enegia) egin behar da.
		 * ERABILPENA: Energia kantitatea barra bezala idazteko.
		 * Input: --
		 * OutPut: Energia barra string moduan.
		*/
		String str = "Energy:[";
		String block = new String(new char[this.energy[0]]).replace("\0", "@");
		str = str + block;
		
		block = new String(new char[(this.energy[1]-this.energy[0])]).replace("\0", "-");
		str = str + block + "]"+this.energy[0]+"/"+this.energy[1];
		return str;
	}
	
	//TODO
	public String getInstanceSring(){
		/** 
		 * FUNTZIOA: Klasearen identifikazioa. Klasea player-ekoa dela bueltatzen duen metodoa. 
		 * ERABILPENA: Kartaren erabiltzailea zein den jakin.
		 * Input: --
		 * OutPut: String
		*/
		
		return "Jokalaria: "+this.getName();
	}
	
}

