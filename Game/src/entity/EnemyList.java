package entity;
import java.util.ArrayList;
import java.util.Iterator;

import game.Mapa;

public class EnemyList {
	private ArrayList<Enemy> list;
	
	//Eraikitzaile
	public EnemyList() {
		list = new ArrayList<Enemy>();
	}
	//geters
	private Iterator<Enemy> getIterator(){
		return this.list.iterator();
	}
	public int size() {
		return this.list.size();
	}
	 //Metodoak
	public void addEnemy(Enemy pEnemy) {
		/** 
		 * FUNTZIOA: Etsai bat sartuko da listara. Sartu baino lehen emandako etsaia null den ala ez 
		 * 	konprobatzen da eta gero lista jada sartuta dagoen ala ez.
		 * ERABILPENA: Etsai berri bat sartu nahi denenan listara.
		 * Input: Sartu nahi den etsaia.
		 * OutPut:--
		*/
		if(pEnemy != null) {
			if(!this.list.contains(pEnemy)) {
				this.list.add(pEnemy);
			}
		}
		
	}
	public Enemy getEnemyInPos(int pPos) {
		/** 
		 * FUNTZIOA: Listako etsai bat posizio baten arabera, listako luzeera eta 0 artekoa, aurkituko eta bueltatuko du.
		 * ERABILPENA: Listako etsai bat bere posizioaren arabera aurkitzeko.
		 * Input: Listako posizio bat.
		 * OutPut: Posizio horretako etsaia.
		*/
		Enemy ema = null;
		if(pPos < this.list.size() && pPos >= 0) {
			ema = this.list.get(pPos);
		}
		return ema;
	}
	public boolean hasEnemy(Enemy pEnemy) {
		/** 
		 * FUNTZIOA: Etsai bat listan badagoen konprobatuko du.
		 * ERABILPENA: Etsai berri bat sartzerakoan.
		 * Input: Aurkitu nahi den etsaia. 
		 * OutPut: Listan badago true, bestela false.
		*/
		return this.list.contains(pEnemy);
	}
	
	public Enemy searchEnemyById(int pId) {
		/** 
		 * FUNTZIOA: Listako iteradore batekin listatik mugituko da eta etsaien id-ak konparatuko ditu
		 * 	bilatzen gauden aurkitu arte. Ez bada aurkitzen null bueltatuko da.
		 * ERABILPENA: Etsai bat bere idz aurkitzeko.
		 * Input: Etsaiaren id-a.
		 * OutPut: Haurkitutako etsaia.
		*/
		Iterator<Enemy> itr = this.getIterator();
		boolean top = false;
		Enemy ema = null;
		while(itr.hasNext()&& !top) {
			ema = itr.next();
			if(ema.hasSameId(pId)) {
				top = true;
			}
		}
		if(!top) {
			ema = null;
		}
		return ema;
	}
	
	public boolean thereIsEnemyWithSameId(Enemy pEnemy) {
		/** 
		 * FUNTZIOA: Etsai bate id-a lortu eta listan aurkituko du iteradore batekin. Aurkitzen bada loop-a amaitu egingo da.
		 * ERABILPENA: Etsai bat listan sartu baino lehen, ez errepikatzeko.
		 * Input: Aurkitu nahi den etsaia.
		 * OutPut: Id berdineko etsaia badago true, bestela fasle.
		*/
		Iterator<Enemy> itr = this.getIterator();
		boolean top = false;
		while(itr.hasNext()&& !top) {
			if(itr.next().hasSameId(pEnemy)) {
				top = true;
			}
		}
		
		return top;
	}
	

	public void updateRoundAll() {
		/** 
		 * FUNTZIOA: Lista osatzen duten etsai guztien update metodoa deituko da. Listan zehar joateko iteradore bat erabiliko da. 
		 * ERABILPENA: Borroketan txanda bakoitzean.
		 * Input: --
		 * OutPut: --
		*/
		Iterator<Enemy> itr = this.getIterator();
		while(itr.hasNext()) {
			itr.next().updateRound();
		}
		this.updateDeaths();
		
	}
	
	public void updateDeaths() {
		/** 
		 * FUNTZIOA: Etsaien listetik hildakoak artu eta elim izeneko lista sartzen ditu. Gero lista berri horretako etsaiak 
		 * 	ezabatzen dira.
		 * ERABILPENA: Borroketako txandetan.
		 * Input:  --
		 * OutPut: --
		*/
		ArrayList<Enemy> elimin = new ArrayList<Enemy>();
		Iterator<Enemy> itr = this.getIterator();
		Enemy en;
		while(itr.hasNext()) {
			en = itr.next();
			if(en.isDead()) {
				//TODO AUMENTAR CONTADOR TOTAL DE ENEMIGOS MUERTOS PARA RESUMEN FINAL
				elimin.add(en);
			}
		}
		this.deleteFromArrayList(elimin);
	}
	
	public void getAllNextAttack() {
		/** 
		 * FUNTZIOA: Listako etsai guztietik iteratu eta beraien hurrengo erasoa lortuko da.
		 * ERABILPENA: Borroketako txandetan.
		 * Input: --
		 * OutPut: --
		*/
		Iterator<Enemy> itr = this.getIterator();
		while(itr.hasNext()) {
			itr.next().updateNextAtack();
		}
	}
	
	private void deleteFromArrayList(ArrayList<Enemy> pEnemyList) {
		/** 
		 * FUNTZIOA: Lista batetik iteratuko da eta etsaiak banan bana kenduko dira.
		 * ERABILPENA: Etsaiak borrokatik kendu nahi direnean.
		 * Input: Ezabatu nahi den lista.
		 * OutPut:-- 
		*/
		Iterator<Enemy> itr = pEnemyList.iterator();
		while(itr.hasNext()) {
			this.list.remove(itr.next());
		}
	}
	
	public boolean isEveryoneDead() {
		/** 
		 * FUNTZIOA: Lista bateko etsaiak hilda badaude konprobatuko du iteradore batekin.
		 * ERABILPENA: Borroketako txandetan.
		 * Input: --
		 * OutPut: Etsai guztiak hilda badaude true, bestela false.
		*/
		//TODO SI ESTAN TODOS MUERTOS LA LISTA ESTA VACIA OSEA QUE CON TAL DE COMPROBAR ESO YA ESTARIA.
		Iterator<Enemy> itr = this.getIterator();
		boolean ema = true;
		while(itr.hasNext() && ema) {
			ema = itr.next().isDead();
		}
		return ema;
	}
	public void attackAll() {
		/** 
		 * FUNTZIOA: Etsai denek daukate erasoa aktibatuko da. Listako etsai guztietan egingo daiteradore bat erabiliz.
		 * ERABILPENA: Borroketan etsaien txandan.
		 * Input: --
		 * OutPut:-- 
		*/
		Iterator<Enemy> itr = this.getIterator();
		while(itr.hasNext()) {
			itr.next().attack();
		}
	}
	
	public String[] getEnemiesInfoAsString(boolean pBattle){
		/** 
		 * FUNTZIOA: Etsaien listatik iteratu eta etsaien infomazioa lortuko da. Gero bi modu desberdinetan idazeko:
		 * 		1-Etsaien informazio generala lotuko da, borroka hasi baino lehen jokalariari erakusteko zer nolako etsaiak diren.
		 * 		2-Etsaien momentu honetako informazioa lortzeko erabiliko da, borroka bitartean erabiltzen da.
		 * ERABILPENA: Borroka hasi baino lehen eta hasi eta gero informazioa lortzeko.
		 * Input: Etsaien informazioa ze modutan erakutsi nahi den.
		 * OutPut: Listako etsaien infomazioa String batean.
		*/
		String[] strList = new String[this.list.size()];
		Iterator<Enemy> itr = this.getIterator();
		int kont = 0;
		while(itr.hasNext()) {
			strList[kont] = itr.next().getInfo(pBattle, 20);
			kont = kont + 1;
		}
		return strList;
	}
	
	public String getAliveEnemiesAsString(int pSelect) {
		/** 
		 * FUNTZIOA: Borroketan dauden etsaien informazioa eta aukeratuta dagoen etsaia string moduan bueltatuko du.
		 * 	Ez badago etsairik "Ez dago Etsairik" bueltatuko da.
		 * ERABILPENA: Borroketan informazioa inprimatu behar denean.
		 * Input: Aukeratuta dagoen etsaia, bere posizioa.
		 * OutPut: Borroketan erabiliko den string-a.
		*/
		String str = "Ez dago Etsairik";
		if(this.list.size() != 0) {
			str = "Etsaiak: \n";
			str = str + Mapa.asembleAsVerticalTable(this.getEnemiesInfoAsString(true), pSelect);
		}
		
		return str;
	}
	
}
