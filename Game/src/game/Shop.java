package game;
import java.util.Random;

import cards.Card;
import cards.CardCollection;
import cards.Deck;
import entity.Player;

public class Shop {
	
	private int pos;
	private int x;
	private int y;
	private Card[] shopItem;
	private int roll;
	//private boolean remove;	 //TODO no se usa : valorar eliminacion
	private static Shop nireShop;

	//Eraikitzailea
	private Shop() { 
		this.pos = 0;
		this.shopItem = new Card[3];
		this.roll=3;
		// this.remove=false; //TODO no se usa
	}

	//geters
	public static Shop getShop() {
		
		if (nireShop == null) 
			nireShop = new Shop();
			
		return nireShop;
	}
	
	public int getRoll() {
		return this.roll;
	}
	
	
	//Metodoak
	public boolean shopInPos(int pX,int pY) {
		/** 
		 * FUNTZIOA: Bi posizio jakinda denda posizio horretako koordenatuekin bat datorren jakiteko. X eta Y koordenatuak dendaren
		 * 	posizioarekin bat badator true izango da, bestela false.
		 * ERABILPENA: Denda posizio batean badagoen jakiteko.
		 * Input: Posizio X eta Posizio Y bat.
		 * OutPut: Denda posizio horretan badago true, bestela false.
		*/
		boolean eran = false;
		if (this.x==pX && this.y==pY)
			eran = true;
		return eran;
	}
	
	public void setInMap() {
		/** 
		 * FUNTZIOA: Maparen gelen arabera eta maparen dimentsiooaren arabera, posizio aleatorio bat lortuko da.
		 * 	Lortutako posizioan ez badago gelarik berriro saiatuko da. Gainera ez du jokalariaren posizioa hautatuko.
		 * ERABILPENA: Mapa sortzea eta solairua aldatzean erabiltzen da.
		 * Input: -- 
		 * OutPut: --
		*/
		int lowX = 0;
		int lowY = 0;
		int highX = 3;
		int highY = 2;
		
		Random rand = new Random();
		
		do {
			this.x=rand.nextInt(highX-lowX) + lowX;
			this.y=rand.nextInt(highY-lowY) + lowY;
		}while(Mapa.getMyMapa().getArray()[this.y][this.x]==null || Player.getMyPlayer().InPos(this.x,this.y) );
	}
	
	public boolean inShop() {
		/** 
		 * FUNTZIOA: Jokalariaren X eta Y posizioa jakinda, gure dendako X eta Y posizioak bedinak diren konprobatzen da.
		 * ERABILPENA: Mugimendu bat egiten denean jakiteko jokalaria badagoen dendan.
		 * Input: --
		 * OutPut: posizio berean badaude true, bestela false.
		*/
		boolean eran = false;
		if (Player.getMyPlayer().InPos(this.x,this.y)) 
			eran=true;
		return eran;
		
	}
	
	public void  fillShop(){
		/** 
		 * FUNTZIOA: Dendako carten Array-a beteko da for loop bat erabiliz eta cardCollection-eko getRandomCard funtzioari esker.
		 * ERABILPENA: Mapa sortzen denean edo solairua aldatzen denean denda berriro beteko da.
		 * Input: -- 
		 * OutPut: --
		*/
		for (int i=0;i<3;i++) {
			
			this.shopItem[i]=CardCollection.getCardCollection().getRandomCard();
			
		}
		
		
	}
	
	public void resetPos() {
		/** 
		 * FUNTZIOA: Dendako kartak erozteko erabiltzen den "kurtzorea" berriro bere hasierako posiziora bueltatzeko.
		 * ERABILPENA: Dendatik irtetzean erabiltzen da.
		 * Input: -- 
		 * OutPut: --
		*/
		this.pos=0;
	}
	
	public void increasePos() {
		/** 
		 * FUNTZIOA: Dendako kurtzorearen posizioari +1 egingo dio, baina 2-tik pasatzen bada berriro hasierako posiziora bueltatuko da.
		 * ERABILPENA: Dendan kartak autatzeko.
		 * Input: -- 
		 * OutPut: --
		*/
		
		if (this.pos<2)
			this.pos++;
		else
			this.resetPos();
	}
	
	public void decreasePos() {
		/** 
		 * FUNTZIOA: Dendako kurtzorearen posizioari -1 egingo dio, baina 0 bada azkenengo posiziora bueltatuko da. 
		 * ERABILPENA: Dendan kartak autatzeko.
		 * Input: -- 
		 * OutPut: --
		*/
		if (this.pos>0)
			this.pos--;
		else 
			this.pos=2;
	}
	
	public void buy(){
		/** 
		 * FUNTZIOA: Kurtzoreak daukan posizioaren arabera dendako karta bat hautatuko da. Karta hori erosteko jokalariak diru nahikoa
		 * 	daukala eta karta hori erosi ahal dela konprobatzen da. Karta erosten bada , arrayeko poszioan null jartzen da.  
		 * ERABILPENA: Kartan erosteko.
		 * Input: -- 
		 * OutPut: --
		*/
		Player myPlayer = Player.getMyPlayer();
		if(this.shopItem[this.pos] != null) {
			if (this.shopItem[this.pos].getPrice() <= myPlayer.getMoney()) {
				myPlayer.spendMoney(this.shopItem[this.pos].getPrice());
				Deck.getMyDeck().addCard(this.shopItem[this.pos].cloneCard());
				this.shopItem[this.pos]=null;
			}else {
				TextBox.getBox().addText("Ez duzu diru nahikorik karta hori erosteko");
			}
		}else {
			TextBox.getBox().addText("Jadanik salduta dago");
		}
	}
	
	
	public void printInfo() {
		/** 
		 * FUNTZIOA: Jokalariak dendan dauden karten iformazioa eta beraien balioa ikusi dezaten printeatu egin dira.
		 * 	Karten informazioarekin lauki bat sortzen da testua formateatuz. 
		 * ERABILPENA: Informazioa erakuzteko deda dagoenean jokalaria.
		 * Input: -- 
		 * OutPut: --
		*/
		
		System.out.println("/Shop/");
		
		String str = "                      ____	   _.-._	\r\n" + 
				"                     / ___`\\	.��     ��.	\r\n" + 
				"         /|         ( (   \\ \\  /   Kaixo,  \\	\r\n" + 
				"    |^v^v  V|        \\ \\/) ) )<    lagun!   |	\r\n" + 
				"    \\  ____ /         \\_/ / /  \\____________/	\r\n" + 
				"    ,Y`    `,            / /	 		\r\n" + 
				"    ||  -  -)           { }	 _______	\r\n" + 
				"    \\\\   _\\ |           | |     | * . *	|	\r\n" + 
				"     \\\\ / _`\\_         / /	|___.___|__	\r\n" + 
				"     / |  ~ | ``\\     _|_|	| + . +	|* |	\r\n" + 
				"  ,-`  \\    |  \\ \\  ,//(_}	| - . -	|__|	\r\n" + 
				" /      |   |   | \\/  \\| |	|___.___|+ |	\r\n" + 
				"|       |   |   | '   ,\\ \\	|_______|- |	\r\n" + 
				"|     | \\   /  /\\  _/`  | |	   |___.___|	\r\n" + 
				"\\     |  | |   | ``     | |	   |_______|	\r\n" + 
				" |    \\  \\ |   |        | |			\r\n" + 
				" |    |   |/   |        / /			\r\n" + 
				" |    |        |        | |			\n"+
				"_______________________________________________________";
		
		

		System.out.println(str);
		
		if (this.shopItem[this.pos]!=null)
			System.out.println("Hautatutako kartaren balioa: "+ this.shopItem[this.pos].getPrice()+" |"+" Jokalariaren dirua: "+ Player.getMyPlayer().getMoney());
		else
			System.out.println("Ezin da erosi. \n");
		System.out.println("Kartak aldatzeko "+this.roll+" ordaindu. \n");
		System.out.println("--------------------------------------------");
		
		
						
		System.out.println(Mapa.assembleAsHorizontalTable(this.getShopCardsAsStrings(), this.pos, " - "));
		System.out.print(Deck.getMyDeck().getCardTable(0, -1,4));
	}
	private String[] getShopCardsAsStrings() {
		/** 
		 * FUNTZIOA: Dendako karta guztien informazioa String bezala gordeko da gero printeatzeko.
		 * ERABILPENA: Infomazioa formatu politago batean idazteko.
		 * Input: -- 
		 * OutPut: --
		*/
		String[] strList = new String[3];
		
		for(int i=0;i<3;i++)
		{
			if (this.shopItem[i]!=null)
				strList[i]=this.shopItem[i].getCardAsString();
			else 
				strList[i]="\n"+"Salduta";
		}
		return strList;
	}

}
