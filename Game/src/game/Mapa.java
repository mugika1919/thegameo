package game;

import java.util.Random;
import java.util.Scanner;



import entity.Player;

	public class Mapa{
		
		//Atributuak
		private int rooms;
		private int enemies;
		private int events;
		private int solairua = 1;
		private String [][]multi;
		private static Mapa nireMapa;
		
		//Eraikitzailea
		private Mapa() { 
			
			generateRooms();
			generateEnemies();
			generateEvents();
			fillMap();
		}
		
		//Getter
		public static Mapa getMyMapa() {
			
			if (nireMapa==null)
				nireMapa = new Mapa();
		
			return nireMapa;
		}
		
		public String[][] getArray() {
			
			
			return this.multi;
		}

		public int getSolairua() {
			return this.solairua;
		}
		
		//Metodoak
		
		private void rerollMap() {
			/** 
			 * FUNTZIOA: Mapako posizio guztiak null-ean jarri eta berriro gela,enemigo,ebentu eta jokalaria mapan jarri.
			 * ERABILPENA: Mapako atributuei balio berriak lortu eta tzertatu solairua aldatzean.
			 * Input: --
			 * OutPut: -- 
			*/
			generateRooms();
			generateEnemies();
			generateEvents();
			for(int i=0;i<3;i++) {
				for(int j=0;j<4;j++) {
					this.multi[i][j]=null;
				}
			}
			fillMap();
			putPlayer();
			Shop.getShop().setInMap();
		}
		
		public void putPlayer() {
			
			/** 
			 * FUNTZIOA: Jokalariari, maparen gelen posizio kontuan artuta, X eta Y hasierako posizioa lortzeko.
			 * for loopean lehenengo zutabeko zein room-ean jarriko den aukeratzen da.
			 * ERABILPENA: Mapa sortzean edo solairua aldatzean erabiliko da.
			 * Input: --
			 * OutPut: -- 
			*/
			Player myPlayer = Player.getMyPlayer();
			
			
			for (int i=0;i<2;i++) {
				if (this.multi[i][0]=="Room") {
					myPlayer.ChangePos(0, i);
					break;
				}
					
			}	
			this.multi[myPlayer.getY()][myPlayer.getX()]="Visited";
		}
		
		public boolean endGame() {
			/** 
			 * FUNTZIOA: Jokoaren amaiera zazpigarren solairuko "Boss"-ari irabaztean edo jokalariaren 
			 * 	bizitza zero denean izango da. Mapan Boos bat non dagoen jakiteko Maparen gelak non dauden jakin behar dira eta 
			 * 	konbinazioaren arabera sartuDa boolearra true bihurtuko da. sartuDa true bada boss bate borroka hasiko da eta irabaztean 
			 * 	solairuari +1 egingo zaio. +1 hori egitean solariau 8 bada eta borroka amaitta badago end true bihurtuko da jokoari amaiera
			 * 	emanez. 
			 * ERABILPENA: Jokoaren loop bakoitzean erabiltzen da jakiteko jokoa amaitu den ala ez.
			 * Input: --
			 * OutPut: Jokoa amaitu bada true, bestela false. 
			*/
			
			boolean end = false;
			Player myPlayer = Player.getMyPlayer();
			/*//TODO no es necesario ??? valorar eliminacion
			Mapa map = Mapa.getMyMapa();
			GameState states = GameState.getGameState();
			*/
			boolean sartuDa = false;
			
			
			//Player Muerto?
			if(myPlayer.isDead()) {
				end = true;
				
			}else if (myPlayer.InPos( 3,0 ) && (this.multi[0][3]=="Room" || this.multi[0][3]=="Visited")) {
				sartuDa=true;
			}
			else if (myPlayer.InPos( 3,2 ) && (this.multi[2][3]=="Room" || this.multi[2][3]=="Visited") && this.multi[0][3]==null) {
				sartuDa=true;
			}	
			else if (myPlayer.InPos( 3,1 ) && this.multi[2][3]==null && this.multi[0][3]==null ) {
				sartuDa=true;
			}	
			
			
			if (sartuDa==true) {
				if (this.solairua<=7) {
					this.rerollMap();
					Battle pelia = new Battle(this.solairua*(-1));
					pelia.beginBattle();
					this.solairua++;
				}
			}
			
			if (this.solairua>7 && Battle.getCurrentBatle()==null)
				end=true;
			
			return end;
		}
		
		public void fillMap() {
			
			/** 
			 * FUNTZIOA: Sortu dugun algoritmoak maparen gela guztiak behar den posizioetan jarriko ditu ausazko boolear bat erabiliz.
			 * 	Hasieran erdiko filan 4 gela jarriko dira horrela edonon jarritako gelak konektatuta daudela ziurtatzen da.
			 * 	Gero ausazko boolearra erabiliz for loopean gela guztietatik iteratzen da room gabe geratu arte.
			 * ERABILPENA: Mapa sortzean eta solairua aldatzean.
			 * Input: --
			 * OutPut: -- 
			*/
			
			int i=0;
			int j=0;
			int placed=4;
			
			
			this.multi[1][0]="Room";
			this.multi[1][1]="Room";
			this.multi[1][2]="Room";
			this.multi[1][3]="Room";
			this.multi[1][4]="Room";
			
			
			Random rand = new Random();
			
			for (i=0; i<3; i++)
			{
				for(j=0;j<4;j++) {
					if (placed==this.rooms)
						break;
					if (this.multi[i][j]==null) {
						if (rand.nextBoolean() ) {
							this.multi[i][j]="Room";
							placed++;
						}
					}
					
				}
				if (placed==this.rooms)
					break;
				
				if (i==2  && placed<this.rooms)
					i=0;
	
				j=0;
			}
		}
		
		public void generateRooms() {
			/** 
			 * FUNTZIOA: Bi balioen artean mapa batean sortuko diren gelak erabakiko dira.
			 * ERABILPENA: Mapa sorztean eta solarirua aldatzean.
			 * Input: --
			 * OutPut: -- 
			*/
			int low = 7;
			int high = 10;
			
			Random rand = new Random();
			
			this.rooms= rand.nextInt(high-low) + low;
			
			this.multi = new String[5][5];
			
		}
		public void generateEnemies() {
			/** 
			 * FUNTZIOA:  Bi balioen artean mapa batean sortuko diren etzaiak erabakiko dira.
			 * ERABILPENA: Mapa sorztean eta solarirua aldatzean.
			 * Input: --
			 * OutPut: -- 
			*/
			
			int low = 1;
			int high = 5;
			
			Random rand = new Random();
			
			this.enemies=rand.nextInt(high-low) + low;
		}
		
		public void generateEvents() {
			/** 
			 * FUNTZIOA:  Bi balioen artean mapa batean sortuko diren ebentuak erabakiko dira.
			 * ERABILPENA: Mapa sorztean eta solarirua aldatzean.
			 * Input: --
			 * OutPut: -- 
			*/
			
			int low = 1;
			int high = 3;
			
			Random rand = new Random();
			
			this.events=rand.nextInt(high-low) + low;
			
		}
		public void printMap(Mapa pMapa) {
			
			/** 
			 * FUNTZIOA: Mapan dauden gelak behar diren posizioan marraztuko dira.
			 * 	Erabilitako algoritmoa bi zatitan banatuta dago: Lehengoan lehen bi lerroak marraztuko dira eta
			 * 	bigarrenean hirugarren lerroa.
			 * 	Lerro bakitza hiru zatitan marraztuko da: Lehenengoan gela bat badago gelak adierazten duten karratuaren gohiko 
			 * 	zatia marraztuko da, hutsik badago ezer ez.
			 * 	Bigarrenean gela bakitzan dagoena marraztuko da : @ dendarako, X jokalariarentzat eta � bat bisitatutako gelentzat.
			 * 	Hirugarren zatian karratuaren beheko zatia marrazten da.
			 * 
			 * ERABILPENA: Loop bakoitzean erabiltzen da mapa marrazteko.
			 * Input: Momentu honetako mapa.
			 * OutPut: -- 
			*/
			
			Player myPlayer = Player.getMyPlayer();
			Shop myshop=Shop.getShop();
			int ite=0;
			System.out.println("/Mapa/");
			System.out.println("Solairua --> "+this.solairua);
			System.out.println("--------------------------------------------");
			for (int i=0;i<3;i++) {
				if (ite==3)
					ite=0;
				if (ite<3 && ite!=0) {
					i--;
				}
				for (int j=0;j<4;j++) {
						
						if(pMapa.multi[i][j]=="Room" || pMapa.multi[i][j]=="Visited") {
							if (ite==0)
								System.out.printf("--------- ");
							if (ite==1) {
								if (myPlayer.InPos(j, i))
									System.out.printf("|   X   | ");
								if (myshop.shopInPos(j, i)  && !myPlayer.InPos(j, i))
									System.out.printf("|   @   | ");
								if (!myPlayer.InPos(j, i) && !myshop.shopInPos(j, i) && this.multi[i][j]=="Room")
									System.out.printf("|       | ");
								if (!myPlayer.InPos(j, i) && !myshop.shopInPos(j, i) && this.multi[i][j]=="Visited")
									System.out.printf("|   .   | ");
							}
								
							if (ite==2)
								System.out.printf("--------- ");
							
						}
						else
						{
							System.out.printf("          ");
							
						}
						
					}
				
				System.out.print("\n");
				ite++; 
			}
			
			
			for (ite=1;ite<3;ite++) {
				for (int j=0;j<4;j++) {
					
					if(pMapa.multi[2][j]=="Room" || pMapa.multi[2][j]=="Visited") {
						if (ite==1) {
							if (myPlayer.InPos(j, 2))
								System.out.printf("|   X   | ");
							if (myshop.shopInPos(j, 2) && !myPlayer.InPos(j, 2))
								System.out.printf("|   @   | ");
							if (!myPlayer.InPos(j, 2) && !myshop.shopInPos(j, 2)&& this.multi[2][j]=="Room")
								System.out.printf("|       | ");
							if (!myPlayer.InPos(j, 2) && !myshop.shopInPos(j, 2) && this.multi[2][j]=="Visited")
								System.out.printf("|   .   | ");
						}
						if (ite==2)
							System.out.printf("--------- ");
						
					}
					else
					{
						System.out.printf("          ");
						
					}
					
				}
				System.out.print("\n");
			}
			
			
		}
	
		private boolean checkIfComb() {
			/** 
			 * FUNTZIOA:  Jokalaria mugitzean, maparen posizio horretan borroka bat izango den ala ez kalkulatuko du.
			 * 	Horretarako borroka kopurua 0 baino handiago dela zihurtatu behar da. Gainera sartutako gelan bisitatua bada ez da
			 * 	borrokarik izango. Ebentu kopurua 0 bada eta gela ez da bisitatua, beti egongo da borroka bat.
			 * ERABILPENA: Jokalaria mugitzean erabiltzen da.
			 * Input: --
			 * OutPut: Borroka hasi behar bada true bueltatuko du, bestela false. 
			*/
			
			boolean eran=false;
			Random r = new Random();
			Mapa map = Mapa.getMyMapa();
			Player myPlayer = Player.getMyPlayer();
			
			if ((map.enemies>0 && map.events<=0 && map.multi[myPlayer.getY()][myPlayer.getX()]!="Visited" ) || r.nextBoolean() && map.enemies>0 && map.multi[myPlayer.getY()][myPlayer.getX()]!="Visited")
				eran=true;
				
			return eran;
			
		}
		
		private boolean checkIfEvent() {
			/** 
			 * FUNTZIOA:  Jokalaria mugitzean, maparen posizio horretan ebentu bat izango den ala ez kalkulatuko du.
			 * 	Horretarako ebentu kopurua 0 baino handiago dela zihurtatu behar da. Gainera sartutako gelan bisitatua bada ez da
			 * 	ebenturik izango.
			 * ERABILPENA: Jokalaria mugitzean erabiltzen da.
			 * Input: --
			 * OutPut: Ebentua hasi behar bada true bueltatuko du, bestela false. 
			*/
			
			boolean eran=false;
			Mapa map = Mapa.getMyMapa();
			Player myPlayer = Player.getMyPlayer();
			
			if ( map.events>0 && map.multi[myPlayer.getY()][myPlayer.getX()]!="Visited")
				eran=true;
				
			return eran;
			
		}
		
		
		public static void main(String[] args) {
			
			
			Mapa map = Mapa.getMyMapa();
			GameState states = GameState.getGameState();
			Shop shop = Shop.getShop();
			TextBox mybox = TextBox.getBox();
			Player myPlayer = Player.getMyPlayer();
			
			map.putPlayer();
			// shop.fillShop(); //Pantaia klasean egiten da.
			Shop.getShop().setInMap();
			map.multi[myPlayer.getY()][myPlayer.getX()]="Visited";
			
			while(!map.endGame()) {
				if (states.getCurrentState()=="Map") {
					
					map.printMap(map);
					System.out.println(myPlayer.getInfo(false, 20));
					mybox.printText();
					System.out.println("");
					TextBox.getBoxDev().printText();
					System.out.println("---------------------------------------------");
					
					takeInput();
					map.endGame();
					
					if (shop.inShop())
						states.changeStateShop();
					else if (map.checkIfComb()) {
						map.enemies--;
						Battle btl = new Battle();
						btl.beginBattle();
					}
					else if ( map.checkIfEvent()) {
						map.events--;
						states.changeStateEvent();
					}
					map.multi[myPlayer.getY()][myPlayer.getX()]="Visited";
				}
				else if (states.getCurrentState()=="Shop") {
					
					shop.printInfo();
					mybox.printText();
					
					System.out.println("---------------------------------------------");
					System.out.println("");
					TextBox.getBoxDev().printText();
					
					takeInput();
				}
				else if (states.getCurrentState()=="Fight") {
					
					Battle.getCurrentBatle().battleLogic();
					
					System.out.println("\n");
					
					Battle.getCurrentBatle().printInfo();
					System.out.println("---------------------------------------------");
					System.out.println("");
					TextBox.getBox().printText();
					System.out.println("");
					TextBox.getBoxDev().printText();
					if(!map.endGame()) {
						takeInput();
					}
					Battle.getCurrentBatle().updateFase();
					
					
				}
				else if (states.getCurrentState()=="Event") {
					//map.PrintMap(map);
					Event.runEvent();
					takeInput();
					states.changeStateMap();
					
				}else if(states.getCurrentState()=="Pantaila") {
					System.out.println(Pantaila.getMyPantaila().getPantaia());
					
					if(!Pantaila.getMyPantaila().saltoEginInput()) {
						takeInput();
					}else {
						//waitMilisec(Pantaila.getMyPantaila().getMillisToWait());
					}
					Pantaila.getMyPantaila().update();
				} 
			}
			Pantaila.getMyPantaila().setPantNumb(10);
			states.changeStatePantaila(); // Ez da behar
			System.out.println(Pantaila.getMyPantaila().getPantaia());
			takeInput();
	}
		
		
	public static void cls(){
		/** 
		 * FUNTZIOA: Pantailan dagoena garbituko du hurrengo loopean berriro marazteko. 
		 * ERABILPENA: Loop bakoitzean.
		 * Input: --
		 * OutPut: -- 
		*/
		try
		{	
			new ProcessBuilder("cmd","/c","cls").inheritIO().start().waitFor();
		}catch(Exception E)
			{
			System.out.println(E);
			}
	}	
	
	
	@SuppressWarnings("resource")
	public static void takeInput(){	
		
		
		/** 
		 * FUNTZIOA:  Loop bakoitzean jokalariari scanner klasea bidez cahr bat eskatuko zaio. Jokalariak String bat jartzen badu
		 * 	sartutako stringaren lehen char-a erabiliko da. 
		 * 	Char horren araberakoa izango da egingo de mugimendu edo akzioa.
		 * ERABILPENA: Loop guztietan.
		 * Input: --
		 * OutPut: -- 
		*/
		
		
		GameState states=GameState.getGameState();
		Player myPlayer = Player.getMyPlayer();
		Scanner reader = new Scanner(System.in);
		String str = reader.next();
		char c = str.charAt(0);
		
		if (states.getCurrentState()=="Map") {
			if (c=='w')
				myPlayer.MoveUp();
			if (c=='a')
				myPlayer.MoveLeft();
			if (c=='s')
				myPlayer.MoveDown();
			if (c=='d')
				myPlayer.MoveRight();
		}
		else if (states.getCurrentState()=="Shop") {
			 
			Shop shop = Shop.getShop();
			
			if (c=='r') {
				if (Player.getMyPlayer().getMoney()>=shop.getRoll()) {
					shop.fillShop();
					Player.getMyPlayer().spendMoney(shop.getRoll());
				}
			}
			if (c=='a')
				shop.decreasePos();
			if (c=='d')
				shop.increasePos();
			if (c=='q') {
				states.changeStateMap();
				shop.resetPos();
			}
			if (c=='e')
				shop.buy();
			System.out.println("Shop State");
		}
		else if (states.getCurrentState()=="Fight") {
			Battle.setLastCharRead(c);
			
		}else if (states.getCurrentState()=="Pantaila") {
			Pantaila.setLastCharRead(c);
			Pantaila.setLastStringRead(str);
		}
		if(str.equals("!activDev")) {
			TextBox tbd = TextBox.getBoxDev();
			tbd.activate(!tbd.isActivated()); 
		}
		
		cls();
	}
	
	//TODO // ESTA MIERDA SE QUITA O QUE COJONES?
	// https://www.youtube.com/watch?v=ubrHGWoePo0
	//https://www.youtube.com/watch?v=LlqKP5vtxHk
	/*public static void waitMilisec(int pMillis) {
		try
		{
		    Thread.sleep(pMillis);
		}
		catch(InterruptedException ex)
		{
		    Thread.currentThread().interrupt();
		}
	}*/
	
	public static String reduceString(String str, int pMax, boolean pGuionOn, boolean pAlignLeft) {
		/** 
		 * FUNTZIOA: String(a), zenbaki(b) eta 2 boolear(c,d) zehaztuz, String horren lerro luzeera sartutako zenbakia gainditzen ez duela arduratuko da. Hau da, String honen luzeera sartutako
		 * 			zenbakia gaindituko balu, orduan, String(a) hori zenbaki(b) horren arabera zatituko du "\n" testu zatia dagokion lekuan txertatuz, hau da, lerro berri bat sortuz. 
		 * 			String-a(a) zatitu ondoren, zatitzeko geratzen den zatia berriz analizatuko du eta zenbakiak honen luzeera gaindituko balu berriz zatituko du. Prozezu hau behin eta berriro 
		 * 			errepikatuko da string osoa luzeera berdineko zatietan banatu arte.
		 * 			d parametro boolearra testuaren lineazioaz arduratuko da: true bada ezkerran eta false eskuman.
		 * 
		 * 			Gainera algoritmoak hitzak erditik zatitzea saihestuko du, baina zatitu behar den posizioan hitz luzeegia egongo balitz eta boolearra (c) == true izango balitz orduan
		 * 			hitza zatitu eta "-" gehituko du amaieran hitz bat erditik zatitu dela adierazteko.
		 * ERABILPENA: Testua egokitzeko.
		 * Input: String str (a), int pMax (b), boolean pGuionOn (c), boolean pAlignLeft(d)
		 * OutPut: String
		*/
		
		String addBetween = "\n";
		String ema = "";
		String split = "";
		
		int startPoint = 0;
		int endPoint = pMax;
		
		boolean atera = false;
		boolean ateraHurrengoan = false;
		
		if(str.length() - endPoint <= 0 )  {
			endPoint = str.length();
			ateraHurrengoan = true;
		}
		
		while(!atera) {
			atera = ateraHurrengoan;
			//System.out.println(split.matches(" es"));
			/*
			split =  str.substring(startPoint, endPoint);
			int pos = split.indexOf("\n");
			if(pos != -1) {
				endPoint = startPoint + pos;
				split =  split.substring(startPoint, endPoint);
				String full = new String(new char[pMax - split.length()]).replace("\0", " ");
				ema = ema + split + full + addBetween + "";
				startPoint = endPoint + 2;
				endPoint = endPoint + pMax;
				atera = false;
				
			}else 
				*/
			if(atera || str.charAt(endPoint) == ' ') {
				split =  str.substring(startPoint, endPoint);
				String full = new String(new char[pMax - split.length()]).replace("\0", " ");
				String full2 = "";
				if(!pAlignLeft) {
					full2 = full;
					full = "";
				}
				ema = ema + full2 + split + full + addBetween + "";
				startPoint = endPoint + 1; //Gehi bat egin hutsunea kentzeko
				endPoint = endPoint + pMax;
		
				
			}else {
				
				endPoint = endPoint - 1;
				if(endPoint - startPoint < pMax/2) {    // CUANDO APLICAR EL GUION
					endPoint = startPoint + pMax;
					if(pGuionOn) {
						endPoint = endPoint - 1;					
						
						ema = ema + str.substring(startPoint, endPoint) + "-" + addBetween + "";
					}else {					
						ema = ema + str.substring(startPoint, endPoint) + addBetween + "";
					}
					endPoint = startPoint + pMax - 1;					
					
					startPoint = endPoint;
					endPoint = endPoint + pMax;
				}
				
			}
			
			if(str.length() - endPoint <= 0 )  {
				endPoint = str.length();
				ateraHurrengoan = true;
			}
			
		}
		
		ema.replace("\n ", "\n");
		
		return ema;
	}
	
	public static String assembleAsHorizontalTable(String[] pString, int pSelection, String pBetween) {
		/** 
		 * FUNTZIOA: String array bat emanda, string hauen taula horizontala osatzen du. pSelection parametroaren bitartez array horretan pozizio
		 * 		zehatz batean dagoen karta erresaltatu daiteke. Zenbaki hau negatiboa izango balitz taula normala bueltatuko du. Zenbakia emadako arrayaren
		 * 		luzeera eta 0 zenbakiaren artean balego zenbaki hori array-ean pozizio bezala interpretatuko du eta posizio horretan dagoen testua erresaltatuko du
		 * 		Zenbakia Array-aren luzeera baino handiago izango balitz orduan ez du testurik erresaltatuko.
		 * 		pBetween parametroaren bitartez testua banatzen duten zutabeen karaktereak aldatu daitezke.
		 * 		Taula uniformea izateko bertan dauden string guztiak luzeera berdina dutela zihurtatuko da hutsuneak eta lerro jauziak gehituz.
		 * ERABILPENA: Taula horizontalak egiteko
		 * Input: String[] pString, int pSelection, String pBetween
		 * OutPut: String (pString-eko string-ak taula horizontal batean)
		*/
		boolean selectionOn = false;
		if(pSelection >= 0) {
			selectionOn = true;
		}
		
		String[] strList = pString;
		int kont = 0;
		int kont2 = 0;
		String ema = "";
		int maxBlocks = 0;
		// int tableLenTot = 0;
		int celdaLen = 0;
		
		String[][] strKartakGuzt = new String[strList.length][];
		
		//Kartak banandu
		while(kont < strList.length) {
			
			strKartakGuzt[kont] = strList[kont].split("\n");
			
			if(maxBlocks < strKartakGuzt[kont].length) {
				maxBlocks = strKartakGuzt[kont].length;
			}
			kont2 = 0;
			while(kont2 < strKartakGuzt[kont].length) {
				if(celdaLen < strKartakGuzt[kont][kont2].length()) {
					celdaLen = strKartakGuzt[kont][kont2].length();
				}
				kont2 = kont2 + 1;
			}
			
			kont = kont + 1;
		}
		
		//Karten Informazioa lotu
		String mark = pBetween;
		kont = 0;
		int lerroa = 0;
		while(lerroa < maxBlocks) {
			kont = 0;
			while(kont < strKartakGuzt.length) {
				
				if(selectionOn) {
					if(pSelection == kont) {
						mark = pBetween;
					}else {
						mark = " ";
					}
				}
				
				if(lerroa < strKartakGuzt[kont].length ) {
					int lenOfString = strKartakGuzt[kont][lerroa].length();
					String fix = "";
					if(lenOfString < celdaLen) {
						fix = new String(new char[celdaLen - lenOfString]).replace("\0", " ");
					}
					
					ema = ema + mark + " " + strKartakGuzt[kont][lerroa]  + fix + " " + mark;
	
				}else {
				
					String full = new String(new char[celdaLen]).replace("\0", " ");
					ema = ema +  mark + " " + full + " " +  mark;
				}
				kont = kont + 1;
				
			}
			ema = ema + "\n";
			lerroa = lerroa + 1;
		}
		/*
		String full = new String(new char[tableLenTot]).replace("\0", "-");
		ema = full + "\n" + ema + full; //TODO
		*/
		return ema;
		
		/*
		kont = strList.length;
		ArrayList<String[]> kartaMultzoa = new ArrayList<String[]>();
		while(kont > 0) {
			kont = kont - 1;
			
			
			System.out.println(strList[kont]);
			System.out.println("-----------");
			
		}
		*/
	}
	
	public static String asembleAsVerticalTable(String[] pString, int pSelection) {
		/** 
		 * FUNTZIOA: String array bat emanda, string hauen taula bertikala osatzen du. pSelection parametroaren bitartez array horretan pozizio
		 * 		zehatz batean dagoen karta erresaltatu daiteke. Zenbaki hau negatiboa izango balitz taula normala bueltatuko du. Zenbakia emadako arrayaren
		 * 		luzeera eta 0 zenbakiaren artean balego zenbaki hori array-ean pozizio bezala interpretatuko du eta posizio horretan dagoen testua erresaltatuko du
		 * 		Zenbakia Array-aren luzeera baino handiago izango balitz orduan ez du testurik erresaltatuko.
		 * 		Taula uniformea izateko bertan dauden string guztiak luzeera berdina dutela zihurtatuko da hutsuneak eta lerro jauziak gehituz.
		 * 		
		 * ERABILPENA: Taula bertikalak egiteko
		 * Input: String[] pString, int pSelection
		 * OutPut: String (pString-eko string-ak taula bertikal batean)
		*/
		String str = "";
		String[] strList = pString;
		
		int kont = 0;
		int kont2 = 0;
		int tableLenTot = 0;
		String mark;
		String mark2;

		String[][] strKartakGuzt = new String[strList.length][];
		
		
		//Kartak banandu
		while(kont < strList.length) {
			strKartakGuzt[kont] = strList[kont].split("\n");
			kont2 = 0;
			while(kont2 < strKartakGuzt[kont].length) {
				if(tableLenTot < strKartakGuzt[kont][kont2].length()) {
					tableLenTot = strKartakGuzt[kont][kont2].length();
				}
				kont2 = kont2 + 1;
			}
			
			kont = kont + 1;
		}
		
		kont = 0;
		
		if(pSelection >= 0) {
			if(pSelection == kont) {
				mark2 = new String(new char[tableLenTot+2]).replace("\0", "-");
			}else {
				mark2 = " ";
			}
		}else {
			mark2 = new String(new char[tableLenTot+2]).replace("\0", "-");
		}
		str = str + mark2 + "\n";
		while (kont < strKartakGuzt.length) {
			kont2 = 0;
			if(pSelection >= 0) {
				if(pSelection == kont) {
					mark2 = new String(new char[tableLenTot+2]).replace("\0", "-");
					mark = "|";
				}else if(pSelection-1 == kont){
					mark2 = new String(new char[tableLenTot+2]).replace("\0", "-");
					mark = " ";
				}else {
					mark2 = " ";
					mark = " ";
				}
			}else {
				mark2 = new String(new char[tableLenTot+2]).replace("\0", "-");
				mark = "|";
			}
			while(kont2 < strKartakGuzt[kont].length) {
				String fix = "";
				int lenOfSlice = strKartakGuzt[kont][kont2].length();
				if(lenOfSlice < tableLenTot) {
					fix = new String(new char[tableLenTot - lenOfSlice]).replace("\0", " ");
				}
				
				str = str + mark + strKartakGuzt[kont][kont2] + fix + mark +"\n";
				kont2 = kont2 + 1;
			}
			str = str + mark2 + "\n";
			
			kont = kont + 1;
		}
		return str;
	}
	
	public static String centerString(String[] pString, int pIndex) {
		/** 
		 * FUNTZIOA: sartutako string-en array bat eta luzeera maximoko zenbaki bat emanda, array-ko string guztiak sartutako luzeera horrekiko
		 * 			zentratuko ditu. String array-ko string-en lerroak pIndex baino tamaina handiagoa izango balute orduan pIndex-are balioa 
		 * 			array-eko string luzeenaren tamainaren balioa hartuko luke.
		 * 		
		 * ERABILPENA: Testua zentratzeko
		 * Input: String[] pString, int pSelection
		 * OutPut: String (string zentratua)
		*/
		int kont = 0;
		int maxIndex = pIndex;
		
		while(kont < pString.length) {
			if(pString[kont].length() > maxIndex) {
				maxIndex = pString[kont].length();
			}
			kont = kont + 1;
		}
		
		kont = 0;
		String ema = "";
		
		while(kont < pString.length) {
				if(pString[kont].length() < maxIndex) {
					String filler = new String(new char[((maxIndex - pString[kont].length())/2)]).replace("\0", " ");
					ema = ema + filler;
				}
				ema = ema + pString[kont] + "\n";

				kont = kont + 1;
		}
		return ema;
	}
	
	public static String centerString(String pString, int pIndex) {
		/** 
		 * FUNTZIOA: sartutako string bat eta luzeera maximoko zenbaki bat emanda, string hori lerroetan banatzen du eta zati hauek array batean gordetzen ditu.
		 * 			Ondoren array-ko string guztiak sartutako luzeera horrekiko zentratuko ditu this.centerString(String[] pString, int pIndex) metodoari deituz. 
		 * 			String array-ko string-en lerroak pIndex baino tamaina handiagoa izango balute orduan pIndex-are balioa 
		 * 			array-eko string luzeenaren tamainaren balioa hartuko luke.
		 * 		
		 * ERABILPENA: Taula bertikalak egiteko
		 * Input: String pString, int pIndex
		 * OutPut: String (pString zentratua)
		*/
		String[] zatiketa = pString.split("\n");
		return Mapa.centerString(zatiketa,pIndex);
	}
	
	public static String filler(String[] pString, int pIndex) {
		/** 
		 * FUNTZIOA: sartutako string-en array bat eta luzeera maximoko zenbaki bat emanda, string horiek parametro horren tamaina izateko hutsuneak gehitzen ditu.
		 * 			String array-ko string-en lerroak pIndex baino tamaina handiagoa izango balute orduan pIndex-are balioa 
		 * 			array-eko string luzeenaren tamainaren balioa hartuko luke.
		 * ERABILPENA: Lerro luzeera berdina duten testu homogeneoa lortzeko.
		 * Input: String[] pString, int pIndex
		 * OutPut: String (pString array-aren string-ak luzeera berdina izan eta string baten unifikatuta string bakoitzeko new line eginda)
		*/
		int kont = 0;
		int maxIndex = pIndex;
		
		while(kont < pString.length) {
			if(pString[kont].length() > maxIndex) {
				maxIndex = pString[kont].length();
			}
			kont = kont + 1;
		}
		
		kont = 0;
		String ema = "";
		
		while(kont < pString.length) {
			String filler = "";
				if(pString[kont].length() < maxIndex) {
					filler = new String(new char[(maxIndex - pString[kont].length())]).replace("\0", " ");
					
				}
				ema = ema + pString[kont] + filler + "\n";

				kont = kont + 1;
		}
		return ema;
	}
	
	public static String filler(String pString, int pIndex) {
		/** 
		 * FUNTZIOA: this.filler(String[] pString, int pIndex) metodoaren funtzionamendu berdina, bina kasu honetan String[] bat jaso beharrean
		 * 			String bat jasotzen du. String hau zatietan banatu eta zati hauek String-en array batean sartzen ditu gero this.filler(String[] pString, int pIndex)
		 * 			funtzioa detitzeko, paramtro bezala sortutako array-a emanez alegia. 
		 * ERABILPENA: Lerro luzeera berdina duten testu homogeneoa lortzeko.
		 * Input: String pString, int pIndex
		 * OutPut: String (pString lerroak luzeera berdina izan eta string baten unifikatuta string bakoitzeko new line eginda)
		*/
		String[] zatiketa = pString.split("\n");
		return Mapa.filler(zatiketa,pIndex);
	}
}
	