package game;

import java.util.Random;

import cards.Deck;
import entity.Enemy;
import entity.EnemyCollection;
import entity.EnemyList;
import entity.Player;
public class Battle {
	//KONTROLAK bataian
	private static final char right = 'd';
	private static final char left = 'a';
	private static final char quit = 'q';
	private static final char use  = 'e';
	private static final char top  = 'w';
	private static final char bot  = 's';
	
	private static Battle currentBattle = null;
	
	private boolean fighting = false;
	private boolean finished = false;
	
	private int prizeGold = 0;
	//private CardList prizeCard = new CardList(); //TODO Valorar eliminacion : Antes se querian dar cartas al vencer a boss
	
	private boolean turnoPlayer;
	private int fasea = -1;
	
	private int round = 0;
	
	private int cardSelection = 0;
	private int enemySelection = 0;
	
	private static char lastCharRead = ' ';
	
	private EnemyList enemyList;
	
	public static Battle getCurrentBatle() {
		/** 
		 * FUNTZIOA: Habian dagoen bataia eman, hau da, atributu estatikoan dagoen bataia bueltatu
		 * ERABILPENA: Klase guztiek jokoan ari den bataia lortu ahal izateko.
		 * Input: --
		 * OutPut: Battle 
		*/
		return Battle.currentBattle;
	}
	
	
	public Battle() {
		/** 
		 * FUNTZIOA: Bataia bat sortzen du. Bataia honi 1-3 arteko auzazko etsaiak jartzen dizkio. 
		 * 			Etsai hauek parametro batzuek baldintzatutako bizitza random-a izango dute
		 * ERABILPENA: Bataiak sortzeko 
		*/
		this.enemyList = new EnemyList();
		Random r = new Random();
		int enemKop = r.nextInt(3) + 1;
		while(enemKop > 0) {
			int saiakerak = 50;
			do {
				try {
					int mot = r.nextInt(EnemyCollection.getEnemyCollection().motaKop()-1);
					this.enemyList.addEnemy(EnemyCollection.getEnemyCollection().getCopRandomEnemy(mot));
					enemKop = enemKop - 1;
					saiakerak = 0;
				}catch (Exception e){
					saiakerak = saiakerak - 1;
					if(saiakerak<=0) {
						enemKop = enemKop - 1;
					}
				}
			}while(saiakerak > 0);
			
		}
	}
	
	public Battle(int[] pEnemies) {
		/** 
		 * FUNTZIOA: Bataia bat sortzen du. Parametro bitartez zehaztutako etsai id-en arraya erabiliz EnemyCollection-en
		 * 			id horiekin bat datozen etsai randomifikatuak sortuko den bataiara gehituko ditu
		 * ERABILPENA: Bataiak sortzeko etsaiak id-en bitartez zehaztuz
		*/
		this.enemyList = new EnemyList();
		int kont = pEnemies.length - 1;
		while(kont >= 0) {
			this.enemyList.addEnemy(EnemyCollection.getEnemyCollection().getRandomizedEnemyById(pEnemies[kont]));
			kont = kont - 1;
		}
		if(this.enemyList.size() == 0) {
			this.enemyList.addEnemy(EnemyCollection.getEnemyCollection().getRandomizedEnemyById(1));
			//ETSAIRIK Ez >> "1" etsaia gehitu.
		}
	}
	
	public Battle(int pEnemie) {
		/** 
		 * FUNTZIOA: Bataia bat sortzen du etsai bakarrarekin.
		 * ERABILPENA: Boss bataiak sortzeko modu errez batean
		*/
		this(intToArray(pEnemie));
	}
	
	private static int[] intToArray(int pZenb) {
		/** 
		 * FUNTZIOA: Parametro bitartez sartutako int-a pozizio bakarreko int array batean bihurtu.
		 * ERABILPENA: Eraikitzaile bat berrerabiltzeko
		 * Input: pId: int
		 * OutPut: int[] 
		*/
		int[] array = new int[1];
		array[0] = pZenb;
		return array;
	}
	
	
	public Battle(int pZenEn, int pMota) { //TODO NO SE SUSA
		/** 
		 * FUNTZIOA: Bataia bat sortzen du. Bataia honetan zein motatako etsaiak eta zenbat izango diren zehaztu daiteke. Mota edo kopurua ez bada egokia auzazki hautatuko da
		 * 			Etsai hauek parametro batzuek baldintzatutako bizitza random-a izango dute
		 * ERABILPENA: Bataiak sortzeko etsai mota eta kantitatea emanda
		*/
		this.enemyList = new EnemyList();
		Random r = new Random();
		int enemKop = 1;
		if(pZenEn > 0) {
			enemKop = pZenEn;
		}
		int maxMotKop = EnemyCollection.getEnemyCollection().motaKop();
		boolean randomizeMot = false;
		if(pMota < 0 || pMota >= maxMotKop) {
			randomizeMot = true;
		}
		
		while(enemKop > 0) {
			int mot = pMota;
			int saiakerak = 20;
			do {
				try {	
					if(randomizeMot) {
						mot = r.nextInt(2);
					}

					this.enemyList.addEnemy(EnemyCollection.getEnemyCollection().getCopRandomEnemy(mot));
					enemKop = enemKop - 1;
					saiakerak = 0;
				}catch (Exception e){
					saiakerak = saiakerak - 1;
					if(saiakerak<=0) {
						enemKop = enemKop - 1;
					}
				}
			}while(saiakerak > 0 && randomizeMot);
		}
	}
	
	public void beginBattle(){
		/** 
		 * FUNTZIOA: Bataia hasi egiten du bataia bat jadanik hasita ez badago. 
		 * 			Bataia hasten badu:
		 * 				� deck-a erreseteatu
		 * 				� Player-aren efetuak kendu eta energia berrezarri
		 * 				� State battle-era aldatu
		 * ERABILPENA: Bataiak hasteko eta guztia hasieratzeko
		*/
		
		if(Battle.currentBattle == null && !this.finished) {
			Battle.currentBattle = this;
			this.fighting = true;
			this.turnoPlayer = true;
			
			Deck.getMyDeck().restart();
			
			Player.getMyPlayer().resetEfects();
			Player.getMyPlayer().updateRound();
			
			this.setPrize(10);
			GameState.getGameState().changeStateFight();
			TextBox.getBoxDev().addText("Bataia hasi egin da");

		}else{
			if(Battle.currentBattle == this){
				TextBox.getBoxDev().addText("Bataia jadanik hasi egin da");
			}else if(this.finished){
				TextBox.getBoxDev().addText("Bataia jokatu da jadanik");
			}else {
				TextBox.getBoxDev().addText("Ezin da bataia bat hasi, jadanik beste bat hasi delako");
			}
		}
	}
	
	public void battleLogic() {
		/** 
		 * FUNTZIOA: Bataiaren logika. Metodo honetan fase bakoitzean exekutatu behar diren aginduak ordenatuta daude. 
		 * ERABILPENA: 
		*/
		Deck myDeck = Deck.getMyDeck();
		Player myPlayer = Player.getMyPlayer();
		
		// this.proveStillFighting();
		if(this.fighting && !Mapa.getMyMapa().endGame() && Battle.currentBattle == this) {
						
			if(this.turnoPlayer) {
				
				//PLAYER FASE 0
				if(this.fasea == 0) {
					//Descrip: Eskuan dauden kartak deskartatu, 5 karta lapurtu, ronda zenbakia inkrementatu eta hurrengo fasera joan.
					myDeck.discardCurrentHand();
					myDeck.getRandomCardsToHand(5);
					TextBox.getBox().addText("Jokalariak 5 karta lapurtu ditu");
					this.round = this.round + 1;
					
				//PLAYER FASE 1
				}else if(this.fasea == 1) {
					//Descrip: Karten eta etsaien selekzioa aldatu.
					char c = Battle.lastCharRead;
					
					if(c == Battle.top) {
						//Etsai selekzioa gora (-1)
						this.changeEnemySelectionBy(-1);
					}else if(c == Battle.bot) {
						//Etsai selekzioa behera (+1)
						this.changeEnemySelectionBy(1);
					}else if(c == Battle.left) {
						//Karta selekzioa ezkerra (-1)
						this.changeCardSelectionBy(-1);
					}else if(c == Battle.right) {
						//Karta selekzioa eskuma (+1)
						this.changeCardSelectionBy(1);
						
					}else if(c == Battle.use) {
						//Karta erabili
						try {
							myDeck.useCardInPos(this.cardSelection);
							this.changeCardSelectionBy(0); //Update selection
						}catch (Exception e) {
							TextBox.getBox().addText(e.getMessage());
						}
					}else if(Character.isDigit(c)) {
						//Karta pozizio bat emanda hau aukeratu.
						int cPos = Character.getNumericValue(c);
						try {
							this.changeSelectionTo(cPos-1);
						}catch(Exception e) {
							TextBox.getBox().addText(e.getMessage());
						}
					}
					//Etsairik hil ahal den frogatu	
					this.enemyList.updateDeaths();
					//Etsaiaren selekzioa refereskatu
					this.changeEnemySelectionBy(0);
					
				//PLAYER FASE 2
				}else if(this.fasea == 2) {
					//Etsaiei efektuak aplikatu.
					this.enemyList.updateRoundAll();
				}
				/*else {
					this.fasea = 0;
					this.turnoPlayer = true;
				}*/
			}else {
				//ENEMY FASE 0
				if(this.fasea == 0) {
					//Etsaiak eraso egin
					this.enemyList.attackAll();
					
				//ENEMY FASE 1
				}else if(this.fasea == 1) {
					//Etsaiaren hurrengo erasoa prestatu eta jokalariari efektuak aplikatu
					this.enemyList.getAllNextAttack();
					
					myPlayer.updateRound();
				}
				
			}	
		}
	}
	
	public void updateFase(){
		/** 
		 * FUNTZIOA: Fasearen aldaketaren logika. Fasearen eta beste paremtro batzuen arabera(input-ak) fase aldaketa burutu ala fasea mantentzea
		 * 			erabakitzen duen metodoa.
		 * ERABILPENA: Fastik fasera pasatzeko
		*/
		this.proveStillFighting();
		
		if(this.fighting && !Mapa.getMyMapa().endGame() && Battle.currentBattle == this) { //TODO repasar !Mapa.getMyMapa().endGame() y cambiarlo a !Player.getMyPlayer().isDead()
			int prevFase = this.fasea;
			if(this.turnoPlayer) {
				if(this.fasea == 0) {
					//Fase P0 -> P1 segituan
					this.fasea = 1;
				}else if(this.fasea == 1){
					//Fase P1 -> P2 Battle.quit tekla sakatu bada ('q' normalean)
					if(Battle.lastCharRead == Battle.quit) {
						this.fasea = 2;
					}
					
				}else if(this.fasea == 2) {
					//Fase P2 -> E0 segituan
					this.fasea = 0;
					this.turnoPlayer = false;
				}else if(this.fasea == -1) {
					//Fase P-1 -> P0 segituan (hasieraketa)
					this.fasea = 0;
				}
			}else{
				if(this.fasea == 0) {
					//Fase E0 -> E1 segituan
					this.fasea = 1;

				}else if(this.fasea == 1){
					//Fase E1 -> P0 segituan
					this.fasea = 0;
					this.turnoPlayer = true;
				}
			}
			
			if(prevFase != this.fasea) {
				this.resetLastChar();
			}
		}else {
			this.endBattle();
		}
		
	}
	
	
	
	private void endBattle() {
		/** 
		 * FUNTZIOA: Bataia amaitzen du. Bataia atributu estatikotik kenduz, premioak emanez, Statea Map-era aldatuz eta deck-a erreseteatuz.
		 * ERABILPENA: Bataia amaitzeko
		*/
		if(this.finished) {
			this.getPrize();
			
			TextBox.getBox().addText("Bataia Amaitu da");
		}else {
			TextBox.getBoxDev().addText("Bataia jadanik amaituta zegoen eta berriro amaitu nahi izan da");
		}
		
		this.finished = true;
		
		Battle.currentBattle = null;
		GameState.getGameState().changeStateMap();

		Deck.getMyDeck().restart();
	}
	
	public Enemy getEnemy(){
		/** 
		 * FUNTZIOA: Aukeratuta dagoen etsaia bueltatzen du Listatik
		 * ERABILPENA: Aukeratua izan den etsaia zein den jakiteko
		 * InPut: --
		 * OutPut: Enemy (Aukeratuta dagoen etsaia)
		*/
		return this.enemyList.getEnemyInPos(this.enemySelection);
	}
	
	public int getEnemySelection(){
		/** 
		 * FUNTZIOA: Aukeratuta dagoen etsaiaren posizioa listan ematen du
		 * ERABILPENA: Aukeratua izan den etsaiaren zenbakia zein den jakiteko.
		 * InPut: --
		 * OutPut: int (Aukeratuta dagoen etsaiaren posizioa)
		*/
		return this.enemySelection;
	}
	
	private void proveStillFighting() {
		/** 
		 * FUNTZIOA: this.fighting atributua berritzen du. Etsairik bizirik baleko = true izango litzateke, bestela = false
		 * ERABILPENA: Aukeratua izan den etsaiaren zenbakia zein den jakiteko.
		 * InPut: --
		 * OutPut: --
		*/
		this.fighting = !this.enemyList.isEveryoneDead();
	}

	
	private void changeCardSelectionBy(int pAldaketa) {
		/** 
		 * FUNTZIOA: Karten selekzio zenbakia parametro bitartez zehaztutako zenbakiaren arabera aldatzen du. Aldaketa honen ondoren 
		 * 			selekzio zenbakia egokia izango ez balitz orduan bermoldatuko du egokia izateko.
		 * ERABILPENA: Karta ezberdinak aukeratu ahal izateko. Hauen artean desplazatzeko
		 * InPut: int pAldaketa
		 * OutPut: --
		*/
		this.cardSelection = this.selectionLogic(pAldaketa, this.cardSelection, Deck.getMyDeck().deckSize(2));
		
	}
	private void changeSelectionTo(int pPos) throws Exception {
		/** 
		 * FUNTZIOA: Karten selekzio zenbakia parametro bitartez zehaztutako zenbakira aldatzen du. Zenbaki hori karta poziziorik
		 * 			errepresentatuko ez balu orduan slbuespena jaurtiko du
		 * ERABILPENA: Karta ezberdinak zenbaki baten bitartez aukeratu ahal izateko
		 * InPut: int pPos
		 * OutPut: ---
		*/
		if(pPos < 0 || pPos >= Deck.getMyDeck().deckSize(2)) {
			throw new Exception("Ez dago kartarik pozizio horretan: " + (pPos+1));
		}else {
			this.cardSelection = pPos;
		}
	}
	
	private void changeEnemySelectionBy(int pAldaketa) {
		/** 
		 * FUNTZIOA: Etsaien selekzio zenbakia parametro bitartez zehaztutako zenbakiaren arabera aldatzen du. Aldaketa honen ondoren 
		 * 			selekzio zenbakia egokia izango ez balitz orduan bermoldatuko du egokia izateko.
		 * ERABILPENA: Etsai ezberdinak aukeratu ahal izateko. Hauen artean desplazatzeko
		 * InPut: int pAldaketa
		 * OutPut: --
		*/
		this.enemySelection = this.selectionLogic(pAldaketa, this.enemySelection, this.enemyList.size());
	}
	
	private int selectionLogic(int pAldaketa, int pOraingoa, int pMax) {
		/** 
		 * FUNTZIOA: Oinarrizko zenbaki bat, honek artu dezakeen balio maximoa eta aldakuntza zenbaki bat emanez oinarrizko zenbakia 
		 * 			aldakuntza zenbakiaren arabera aldatu egiten duen algoritmoa. Algoritmo honek aldaketa egin eta gero lortutako emaitza
		 * 			ez egokia izango balitz, orduan emaitza analizatuko du.
		 * 				� Analizatu beharreko zenbakia:
		 * 					- Negatiboa, balio hau balio maximoari gehitu
		 * 					- Maximoa baino handiagoa, balio hau % maximoa egin.
		 * ERABILPENA: Etsaiak eta kartak aukeratzeko algoritmo komuna
		 * InPut: int pAldaketa, int pOraingoa, int pMax
		 * OutPut: int (Prozezatutako balio berria)
		*/
		int ema = pOraingoa + pAldaketa;
		if(pMax != 0) {
			ema = ema % pMax;
			if(ema < 0) {
				ema = ema + pMax;
			
			}
		}
		return ema;
	}
	
	public int getRound() {
		/** 
		 * FUNTZIOA: Bataiaren ronda bueltatzen du
		 * ERABILPENA: Karta dinamikoetan erabilia
		 * InPut: --
		 * OutPut: int (this.round)
		*/
		return this.round;
	}
	
	public int getCardSelection() { // TODO ez da erabiltzen valorar eliminacion
		return this.cardSelection;
	}
	public void resetLastChar() {
		/** 
		 * FUNTZIOA: Irakurri den azkenengo karakterea garbitzeko erabilia
		 * ERABILPENA: Bataiaren fasearen aldaketaren logikan erabilia. Ez usteko fase aldaketak sahiesteko
		 * InPut: --
		 * OutPut: --
		*/
		Battle.lastCharRead = ' ';
	}
	
	public static void setLastCharRead(char pAzkenengoa) {
		/** 
		 * FUNTZIOA: Parametro bezala zehaztutako karakterea, sartutako azken karaktere bezala interpretetu eta lastCharRead-an gordetzen du
		 * ERABILPENA: Bataiaren eta faseen aldaketen logikan erabili
		 * InPut: char pAzkenengoa
		 * OutPut: --
		*/
		Battle.lastCharRead = pAzkenengoa;
	}
	
	private void getPrize() {
		/** 
		 * FUNTZIOA: Jokalariari dagokiozkion opariak eman
		 * ERABILPENA: Bataia amaitzerakoan dirua eta bestelakoak emateko
		 * InPut: --
		 * OutPut: --
		*/
		if(!Player.getMyPlayer().isDead()) {
			Player.getMyPlayer().takeMoney(this.prizeGold);
		}
	}
	
	private void setPrize( int pScalator) {
		/** 
		 * FUNTZIOA: Opariak ezarri eta finkatzeko algoritmoa
		 * ERABILPENA: Opariak finkatu
		 * InPut: --
		 * OutPut: --
		*/
		Random r = new Random();
		this.prizeGold = r.nextInt(6)+5;
	}
	
	public void printInfo() { //TODO Tal vez documentar mas este metodo
		/** 
		 * FUNTZIOA: Fasearen arabera gauza ezberdinak inprimatuko ditu
		 * ERABILPENA: Ikusiko dena pantailan
		 * InPut: --
		 * OutPut: --
		*/
		Deck myDeck = Deck.getMyDeck();
		Player myPlayer = Player.getMyPlayer();
		
		this.proveStillFighting();
		System.out.println("/Fight/");
		if(!this.fighting) {
			//Irabazi bada
			String symb = "\n" + 
					"  ______          _                   _    _ \n" + 
					" |___  /         (_)                 | |  | |\n" + 
					"    / / ___  _ __ _  ___  _ __   __ _| | _| |\n" + 
					"   / / / _ \\| '__| |/ _ \\| '_ \\ / _` | |/ / |\n" + 
					"  / /_| (_) | |  | | (_) | | | | (_| |   <|_|\n" + 
					" /_____\\___/|_|  |_|\\___/|_| |_|\\__,_|_|\\_(_)\n" + 
					"                                             \n" + 
					"                                             \n" + 
					"";
			
			
			int lenMaxTbl = "  ______          _                   _    _ \n".length();
			symb = Mapa.filler(symb, 0);
			
			System.out.println(symb);
			
			String print = "�BATAIA IRABAZI DUZU!" + "\n" + "------" +"--" + "\n";
			print = print + "Irabazitakoa: " + this.prizeGold + " urrezko txanpon";
			print = print + "\n" + "\n"+ "�����" + "\n" + "\n"+ "Edozein tekla sakatu Mapara Bueltatzeko" + "\n";
			print = Mapa.centerString(print, lenMaxTbl);
			System.out.println(print);
			
		}else
		if(this.turnoPlayer) {
			
			if(this.fasea == 0) {
				// Fase P0
				System.out.println("Jokalariaren txanda, 0 Fasea:");
				System.out.println("->> Kartak Lapurtu");
				String[] str = {myPlayer.getInfo(true, 20), this.enemyList.getAliveEnemiesAsString(-1)};
				System.out.println(Mapa.assembleAsHorizontalTable(str, -1, "|"));
				System.out.println("\n" + " � Kartak Eskuan:");
				System.out.print(myDeck.getCardTable(2, -1,5));
				System.out.println("\n"+ "\n" + " � Kartak Deck Nagusian:");
				System.out.print(myDeck.getCardTable(0, -1,5));
				System.out.println("\n" + " � Kartak Discard Pile-n:");
				System.out.print(myDeck.getCardTable(1, -1,5));
				
				System.out.println("\n" + "\n" + "Deck-aren Infoa:");
				
				
				
				
				System.out.println("Karta Eskuan: " + myDeck.deckSize(2) +"; Deck Naguzian: " + myDeck.deckSize(0) + "; Discard Deck-ean: " + myDeck.deckSize(1) +" \n Karta Guztira: " + myDeck.deckSize(3));
				
			}else if(this.fasea == 1){
				// Fase P1
				System.out.println(	"         />_________________________________\n"+
									"[########[]_________________________________>\n"+
									"         \\>\n");
				System.out.println("Jokalariaren txanda, 1 Fasea:");
				System.out.println("->> Kartak Aukeratu eta erabili");
				
				String[] str = {myPlayer.getInfo(true, 20), this.enemyList.getAliveEnemiesAsString(this.enemySelection)};
				System.out.println(Mapa.assembleAsHorizontalTable(str, 0, "|>"));
				
				System.out.println("\n" + " � Kartak Eskuan:");
				System.out.print(myDeck.getCardTable(2, this.cardSelection,5));
				
				System.out.println("\n" + "Deck-aren Infoa:");
				
				
				
				System.out.println("Karta Eskuan: " + myDeck.deckSize(2) +"; Deck Naguzian: " + myDeck.deckSize(0) + "; Discard Deck-ean: " + myDeck.deckSize(1) +" \n Karta Guztira: " + myDeck.deckSize(3));
				
			}else if(this.fasea == 2) {
				// Fase P2
				System.out.println("Jokalariaren txanda, 2 Fasea:");
				System.out.println("->> Etsaiean efektuak aplikatu");
				String[] str = {myPlayer.getInfo(true, 20), this.enemyList.getAliveEnemiesAsString(-1)};
				System.out.println(Mapa.assembleAsHorizontalTable(str, 0, "|>"));
				System.out.println("\n" + " � Kartak Eskuan:");
				System.out.print(myDeck.getCardTable(2, -1));
				
				System.out.println("\n" + "Deck-aren Infoa:");
				
				
				System.out.println("Karta Eskuan: " + myDeck.deckSize(2) +"; Deck Naguzian: " + myDeck.deckSize(0) + "; Discard Deck-ean: " + myDeck.deckSize(1) +" \n Karta Guztira: " + myDeck.deckSize(3));
				
			}else if(this.fasea == -1) {
				// Fase P-1
				String symb = "\r\n" + 
								"  	         ______ _       _     _   \n" + 
								" 	        |  ____(_)     | |   | |  \n" + 
								" 	        | |__   _  __ _| |__ | |_ \n" + 
								" 	        |  __| | |/ _` | '_ \\| __|\n" + 
								"         />_________________________________\n"+
								"[########[]_________________________________>\n"+
								"         \\>\n"+
								" 	        |_|    |_|\\__, |_| |_|\\__|\n" + 
								"		           / |          \n" + 
								"	                 |___/           \n" + 
								"";
				
				
				String[] str = this.enemyList.getEnemiesInfoAsString(false);
				String table = Mapa.assembleAsHorizontalTable(str, -1, "|");
				str = table.split("\n");
				int lenMaxTbl = str[1].length();
				symb = Mapa.filler(symb, 20);
				symb = Mapa.centerString(symb, lenMaxTbl);
				System.out.println(symb);
				String print = "";
				
				
				
				print = print + "BORROKAN SARTU ZARA!" + "\n";
				print = print + "---------" + "\n";
				print = print + "-----" + "\n";
				print = print + "Etsai hauekin borrokatuko zara:" + "\n";
				print = print + "-----" + "\n";
				print = Mapa.centerString(print, lenMaxTbl);
				
				System.out.println(print);
				
				System.out.println(table);
				
				
			}else {
				// Fase ??
				System.out.println("Fasea ez dago definiturik: " + this.fasea + "/ Tur.Player:" + this.turnoPlayer);
				TextBox.getBoxDev().addText("Fasea ez dago definiturik: " + this.fasea + " Tur.Player:" + this.turnoPlayer);
			}
			
		}else {
			if(this.fasea == 0) {
				// Fase E0
				System.out.println("Esaiaren txanda, 0 Fasea:");
				System.out.println("->> Jokakaria erasotu");
				String[] str = {myPlayer.getInfo(true, 20), this.enemyList.getAliveEnemiesAsString(-1)};
				System.out.println(Mapa.assembleAsHorizontalTable(str, 1, "<|"));
				System.out.println("\n" + " � Kartak Eskuan:");
				System.out.print(myDeck.getCardTable(2, -1));
				
				System.out.println("\n" + "Deck-aren Infoa:");
				
				
				System.out.println("Karta Eskuan: " + myDeck.deckSize(2) +"; Deck Naguzian: " + myDeck.deckSize(0) + "; Discard Deck-ean: " + myDeck.deckSize(1) +" \n Karta Guztira: " + myDeck.deckSize(3));
				
			}else if(this.fasea == 1){
				//Fase E1
				System.out.println("Esaiaren txanda, 1 Fasea:");
				System.out.println("->> Jokakariak jasotako efektuak aplikatu eta hurrengo erasoa prestatu");
				String[] str = {myPlayer.getInfo(true, 20), this.enemyList.getAliveEnemiesAsString(-1)};
				System.out.println(Mapa.assembleAsHorizontalTable(str, 1, "|"));
				System.out.println("\n" + " � Kartak Eskuan:");
				System.out.print(myDeck.getCardTable(2, -1));
				
				System.out.println("\n" + "Deck-aren Infoa:");
				
				
				System.out.println("Karta Eskuan: " + myDeck.deckSize(2) +"; Deck Naguzian: " + myDeck.deckSize(0) + "; Discard Deck-ean: " + myDeck.deckSize(1) +" \n Karta Guztira: " + myDeck.deckSize(3));
				
				
				
			}else {
				// Fase ??
				System.out.println("Fasea ez dago definiturik: " + this.fasea + " Tur.Player:" + this.turnoPlayer);
				TextBox.getBoxDev().addText("Fasea ez dago definiturik: " + this.fasea + " Tur.Player:" + this.turnoPlayer);
			}
		}
	}
	
	
}
