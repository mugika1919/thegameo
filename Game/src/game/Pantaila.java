package game;

import entity.Player;

public class Pantaila {
		
		private static Pantaila myPantaila;

		private int pantNumb = 0;
		private int selection = 0;
		private boolean inputSaihestu = false;
		private int waitMS = 0;
		
		private static char lastCharRead = ' ';
		
		private String gorde = "";
		private static String lastStringRead = " ";
		private boolean confirm = false;
		//Eraikitzaile
	    private Pantaila() {

	    }
	    //Geters
	    public static Pantaila getMyPantaila() {
	        if (Pantaila.myPantaila == null) {
	        	Pantaila.myPantaila = new Pantaila();
	        }
	        return Pantaila.myPantaila;
	    }
	    //seters
	    public static void setLastCharRead(char pChar) {
			Pantaila.lastCharRead = pChar;
		}
	    public static void setLastStringRead(String pStr) {
	    	Pantaila.lastStringRead  = pStr;
	    }
		//Metodoak
	    public String getPantaia() {
	    	/** 
			 * FUNTZIOA: Jokoaren hasieran agertuko den informazioa aukeratu eta inprimatuko du. pantNumb-aren arabera pantaila desberdinak agertuko, batzuk informazio bakarrik 
			 * 	erakutziko dute baine beste bazuetan aukerak egin beharko dira, adibidez jokalariaren klasea (bi daude) eta izena. 
			 * 	Jokoa amaitutakoan ere pantaila bat agertuko da.
			 * ERABILPENA: Jokoaren hasieran eta amaieran. 
			 * Input: --
			 * OutPut: Inprimatuko den informazioa string bezala.
			*/
	    	String ema = "";
	    	String piece = "";
	    	int len = 0;
	    	if(pantNumb == 0) {
	    		
	    		
	    		
	    		ema = ema + "\n" + 
	    				"  ______        ______       __            __       __     \n" + 
	    				" /      \\      /      \\     |  \\          |  \\     /  \\    \n" + 
	    				"|  $$$$$$\\    |  $$$$$$\\    | $$          | $$\\   /  $$    \n" + 
	    				"| $$   \\$$    | $$__| $$    | $$          | $$$\\ /  $$$    \n" + 
	    				"| $$          | $$    $$    | $$          | $$$$\\  $$$$    \n" + 
	    				"| $$   __     | $$$$$$$$    | $$          | $$\\$$ $$ $$    \n" + 
	    				"| $$__/  \\ __ | $$  | $$ __ | $$_____  __ | $$ \\$$$| $$ __ \n" + 
	    				" \\$$    $$|  \\| $$  | $$|  \\| $$     \\|  \\| $$  \\$ | $$|  \\\n" + 
	    				"  \\$$$$$$  \\$$ \\$$   \\$$ \\$$ \\$$$$$$$$ \\$$ \\$$      \\$$ \\$$\n" + 
	    				"";
	    		Mapa.filler(ema, 1);
	    		
	    		len = "                                                           \n".length();
	    		
	    		piece = "\r\n" + 
	    				"                             \r\n" + 
	    				" _____     _   _         _   \r\n" + 
	    				"|_   _|___| |_| |___ ___| |_ \r\n" + 
	    				"  | | | .'| | . | -_| .'| '_|\r\n" + 
	    				"  |_| |__,|_|___|___|__,|_,_|\r\n" + 
	    				"                             \r\n" + 
	    				"";
	    		
	    		
	    		ema = ema + "\n" + Mapa.centerString(piece, len);
	    		ema = ema + "\n" + "\n" + Mapa.centerString("Aurkezten du:", len) ;
	    		
	    	}else if(pantNumb == 1) {
	    		ema = ema + "                                                                                                 \r\n" + 
	    				"MM                                                                                            MM \r\n" + 
	    				"MM       db      `7MM\"\"\"Mq.  `7MM\"\"\"Mq.        db      `7MMF' `YMM'       db      `7MMF'      MM \r\n" + 
	    				"MM      ;MM:       MM   `MM.   MM   `MM.      ;MM:       MM   .M'        ;MM:       MM        MM \r\n" + 
	    				"MM     ,V^MM.      MM   ,M9    MM   ,M9      ,V^MM.      MM .d\"         ,V^MM.      MM        MM \r\n" + 
	    				"MM    ,M  `MM      MMmmdM9     MMmmdM9      ,M  `MM      MMMMM.        ,M  `MM      MM        MM \r\n" + 
	    				"MM    AbmmmqMA     MM  YM.     MM  YM.      AbmmmqMA     MM  VMA       AbmmmqMA     MM      , MM \r\n" + 
	    				"MM   A'     VML    MM   `Mb.   MM   `Mb.   A'     VML    MM   `MM.    A'     VML    MM     ,M MM \r\n" + 
	    				"MM .AMA.   .AMMA..JMML. .JMM..JMML. .JMM..AMA.   .AMMA..JMML.   MMb..AMA.   .AMMA..JMMmmmmMMM MM \r\n" + 
	    				"MM                                                                                            MM \r\n" + 
	    				"MM                                                                                            MM \n";
	    		//Georgia 11 http://patorjk.com/software/taag/#p=about&f=Georgia11&t=%7CARRAKAL%7C
	    		
	    		len = "MM                                                                                            MM".length();
	    		
	    		piece = " Edozein Tekla Sakatu Jokoa Hasteko "+" \n " + " \n  \n " + " \n " + "Kotrolak Hauek dira: " + "\n a s d q w e r \n \n"  + "MAPAN:" + "\n" + "a: Ezkerra; d: Eskuma" + "\n" + "w: Gora; " + "s: Behera";
	    		piece = piece + "\n"+ "-" + "\n" + "BORROKAN:" + "\n" + "a: Ezkerreko karta hautatu; " + "d: Eskumako karta hautatu" + "\n" + "w: Goiko Etsaia aukeratu; " + "s: Beheko Etsaia aukeratu" +
	    						"\n" + "e: Aukeratutako karta erabili aukeratutako etsaiean" + "\n" + "q: Txanda pasatu" ;
	    		piece = piece + "\n"+ "-" + "\n" + "DENDAN:" + "\n" + "a: Ezkerreko karta hautatu; " + "d: Eskumako karta hautatu" + "\n" + "r: Denda erreseatu; " + " e: Aukeratutako karta erosi" + "\n" + "q: Dendatik Atera" ;
	    		
	    		ema = ema + Mapa.centerString(piece, len);
	    		
	    	}else if(pantNumb == 2) {
	    		ema = ema + "KLASEAREN AUKERAKETA" + "\n \n";
	    		String[] motak = {"Warrior", "Rogue"};
	    		this.changeSelectionBy(0, motak.length);
	    		piece = Mapa.assembleAsHorizontalTable(motak, this.selection, "||");
	    		ema = ema + piece + "\n" + "DESKRIBAPENA" + "\n";
	    		if(this.selection == 0) {
	    			ema = ema + "Gerlaria pertsonai basatia da, oso indartsua ete erresistentea, \n zaurgarria jartzeko kapaza, baina pozoiekin ez dena oso ondo eramaten.";
	    			
	    			piece = "\r\n" + 
	    					"                   /^\\\r\n" + 
	    					"          _.-`:   /   \\   :'-._\n" + 
	    					"        ,`    :  |     |  :    '.\n" + 
	    					"      ,`       \\,|     |,/       '.\n" + 
	    					"     /           `-...-`           \\\n" + 
	    					"    :              .'.              :\n" + 
	    					"    |             . ' .             |\n" + 
	    					"    |             ' . '             |\n" + 
	    					"    :              '.'              :\n" + 
	    					"     \\           ,-\"\"\"-,           /\n" + 
	    					"      `.       /' |   |'\\       ,'\n" + 
	    					"        `._   ;   |   |  ;   _,'\n" + 
	    					"           `-.:   |   |  :,-'\n" + 
	    					"                  |   |\n" + 
	    					"                  |   |\n" + 
	    					"                  |   |\n" + 
	    					"                  |   |\n" + 
	    					"                  |   |  ";
	    			
	    			piece = Mapa.filler(piece, 1);
	    			ema = ema + "\n" + piece;
	    		}else if(this.selection == 1) {
	    			ema = ema + "Pikaroa azeri hutsa da, bere defentsa zahiestean datza, \n eta pozoia zein zaurgarria jartzeko aukerak ditu. Zailagoa da honekin jolastea.";
	    			
	    			piece = "       .---.\n" + 
	    					"       |---|\n" + 
	    					"       |---|\n" + 
	    					"       |---|\n" + 
	    					"   .---^ - ^---.\n" + 
	    					"   :___________:\n" + 
	    					"      |  |//|\n" + 
	    					"      |  |//|\n" + 
	    					"      |  |//|\n" + 
	    					"      |  |//|\n" + 
	    					"      |  |//|\n" + 
	    					"      |  |/*|\n" + 
	    					"      |* |**|\n" + 
	    					"      |*-'**|\n" + 
	    					"       \\***/\n" + 
	    					"        \\*/\n" + 
	    					"         V\n" + 
	    					"        '\n" + 
	    					"         ^'";
	    			
	    			piece = Mapa.filler(piece, 1);
	    			ema = ema + "\n" + piece + "\n";
	    			}
	    		if(this.confirm) {
	    			ema = ema + "ZIHUR ZAUDE \"" + motak[this.selection] + "\" aukeratu nahi duzula?";
	    		}
				ema = ema + "\n" + "e: sakatu konfirmatzeko" + "\n";

	    		ema = Mapa.centerString(ema, 1);
	    				
	    	}else if(this.pantNumb == 3) {    		
	    		if(!this.confirm) {
	    			ema = ema + "Eta, zein da zure izena ???" + "\n\n\n" + "Zure izena:";
	    		}else {
	    			ema = ema + this.gorde + " , horrela deitzen zara ???" + "\n \n" + "Bai(e) / Ez(q)" +"\n";
	    		}
	    	}else if(this.pantNumb == 4) {
				ema = ema + Player.getMyPlayer().getName() + " , ze izen polita duzun gero! " + "\n";
				ema = ema + "Eskerrik asko lana onartzeagatik, oraitxe \nbidaliko zaitugu Arrakalaren lehenego pisura. \nZorte on izan dezazun " + Player.getMyPlayer().getName() + "\n";
				
				ema = ema + "\n" + "              o\n" + 
						"                   O       /`-.__\n" + 
						"                          /  \\�'^|\n" + 
						"             o           T    l  *\n" + 
						"                        _|-..-|_\n" + 
						"                 O    (^ '----' `)\n" + 
						"                       `\\-....-/^\n" + 
						"             O       o  ) \"/ \" (\n" + 
						"                       _( (-)  )_\n" + 
						"                   O  /\\ )    (  /\\\n" + 
						"                     /  \\(    ) |  \\\n" + 
						"                 o  o    \\)  ( /    \\\n" + 
						"                   /     |(  )|      \\\n" + 
						"                  /    o \\ \\( /       \\\n" + 
						"            __.--'   O    \\_ /   .._   \\\n" + 
						"           //|)\\      ,   (_)   /(((\\^)'\\\n" + 
						"              |       | O         )  `  |\n" + 
						"              |      / o___      /      /\n" + 
						"             /  _.-''^^__O_^^''-._     /\n" + 
						"           .'  /  -''^^    ^^''-  \\--'^\n" + 
						"         .'   .`.  `'''----'''^  .`. \\\n" + 
						"       .'    /   `'--..____..--'^   \\ \\\n" + 
						"      /  _.-/                        \\ \\\n" + 
						"  .::'_/^   |                        |  `.\n" + 
						"         .-'|                        |    `-.\n" + 
						"   _.--'`   \\                        /       `-.\n" + 
						"  /          \\                      /           `-._\n" + 
						"  `'---..__   `.                  ._.._   __       \\\n" + 
						"           ``'''`.              .'     `'^  `''---'^\n" + 
						"                  `-..______..-'";
	    	
	    	}else if(this.pantNumb == 10) {
	    		//AMAIERA
	    		ema = "                                                      \r\n" + 
	    				"                                                      \r\n" + 
	    				"  .g8\"\"\"bgd       db      `7MMM.     ,MMF'`7MM\"\"\"YMM  \r\n" + 
	    				".dP'     `M      ;MM:       MMMb    dPMM    MM    `7  \r\n" + 
	    				"dM'       `     ,V^MM.      M YM   ,M MM    MM   d    \r\n" + 
	    				"MM             ,M  `MM      M  Mb  M' MM    MMmmMM    \r\n" + 
	    				"MM.    `7MMF'  AbmmmqMA     M  YM.P'  MM    MM   Y  , \r\n" + 
	    				"`Mb.     MM   A'     VML    M  `YM'   MM    MM     ,M \r\n" + 
	    				"  `\"bmmmdPY .AMA.   .AMMA..JML. `'  .JMML..JMMmmmmMMM \r\n" + 
	    				"                                                      \r\n" + 
	    				"                                                      \r\n" + 
	    				"                                                      \r\n" + 
	    				"                                                      \r\n" + 
	    				"  .g8\"\"8q.`7MMF'   `7MF'`7MM\"\"\"YMM  `7MM\"\"\"Mq.        \r\n" + 
	    				".dP'    `YM.`MA     ,V    MM    `7    MM   `MM.       \r\n" + 
	    				"dM'      `MM VM:   ,V     MM   d      MM   ,M9        \r\n" + 
	    				"MM        MM  MM.  M'     MMmmMM      MMmmdM9         \r\n" + 
	    				"MM.      ,MP  `MM A'      MM   Y  ,   MM  YM.         \r\n" + 
	    				"`Mb.    ,dP'   :MM;       MM     ,M   MM   `Mb.       \r\n" + 
	    				"  `\"bmmd\"'      VF      .JMMmmmmMMM .JMML. .JMM.      \r\n" + 
	    				"                                                      \r\n" + 
	    				"                                                      ";
	    	}
	    	
	    	
	    	return ema;
	    }
	    
	    
	    public void setPantNumb(int pInt) {
	    	/** 
			 * FUNTZIOA: Emandako pInt balioaren arabera autatutako pantaila aldatuko da.
			 * ERABILPENA: Pantailas aldatzeko.
			 * Input: Autatu nahi den pantaila.
			 * OutPut: --
			*/
	    	this.pantNumb = pInt;
	    }
	    public void update() {
	    	/** 
			 * FUNTZIOA: Pantaila bakoitzak logika bat edukiko du, batzuetan huggrengo pantailara bakarrik aldatu beharko da baina beste batzuetan aukera batzuk emango dira jokalariari.
			 * ERABILPENA: Loop bakoitzean.
			 * Input: --
			 * OutPut: --
			*/
	    	// 0. Pantaiaren Logika
	    	if(this.pantNumb == 0) {
	    		this.pantNumb = 1;
	    	}
	    	
	    	// 1. Pantaiaren Logika
	    	else if(this.pantNumb == 1) {
	    		this.pantNumb = 2;
	    
	    	}
	    	
	    	// 2. Pantaiaren Logika
	    	else if(this.pantNumb == 2) {
	    		if(lastCharRead == 'a') {
	    			this.selection --;
	    		}else if(lastCharRead == 'd') {
	    			this.selection ++;
	    		}else if(lastCharRead == 'e') {
	    			if(this.confirm) {
	    				this.pantNumb = 3;
	    				Player.getMyPlayer().setJob(this.selection);
	        			this.confirm = false;
	        			
	    			}else {
	    				this.confirm = true;
	    			}
	    		}
	    		if(lastCharRead != 'e') {
	    			this.confirm = false;
	    		}
	    	}
	    	
	    	// 3. Pantaiaren Logika
	    	else if(this.pantNumb == 3) {
	    		if(this.confirm) {
	    			if(Pantaila.lastStringRead.equals("e")) {
	        				this.pantNumb = 4;
	        				Player.getMyPlayer().changeName(this.gorde);
	            			this.confirm = false;
	            		
	        		}else if(Pantaila.lastStringRead.equals("q")){
	        			this.confirm = false;
	        		}
	    		}else {
	    			this.gorde = Pantaila.lastStringRead;
	    			this.confirm = true;
	    		}  	
	    	}
	    	
	    	// 4. Pantaiaren Logika
	    	else if(this.pantNumb == 4) {
	    		GameState.getGameState().changeStateMap();
	    		Shop.getShop().fillShop(); 
	    	}
	    	// ...
	    	// 10. Pantaiaren Logika
	    	else if(this.pantNumb == 10) {
	
	    	}
	    }
	    
	    //TODO pa que son los inputs?�?�
	    private void changeSelectionBy(int pInt, int pMax) {
	    	/** 
			 * FUNTZIOA: 
			 * ERABILPENA: Aukera bat egin behar denean.
			 * Input: --
			 * OutPut: --
			*/
			this.selection = this.selection + pInt;
			
			if(pMax != 0) {
				this.selection = this.selection % pMax;
				if(this.selection < 0) {
					this.selection = this.selection + pMax;
				
				}
			}
		}
	    
	    
	    
	    public boolean saltoEginInput() {
	    	/** 
			 * FUNTZIOA: Jokalariaren inputaren beharrik ez dagoenenan, ez zaio eskatuko eta jarraitu egingo du loop-ak.
			 * ERABILPENA: Inputa saihestu nahi bada jakiteko.
			 * Input: --
			 * OutPut: Saihestu  behar bada true, bestela false.
			*/
	    	return this.inputSaihestu;
	    }
	    //TODO quitamos o no?
	    public int getMillisToWait() {
	    	return this.waitMS;
	    }
	    
}
