package game;

import java.util.ArrayList;
import java.util.Iterator;

public class TextBox {
	
	/* // TODO valorar eliminacion si no vamos a meter colores.
		private String black="\033[30m"; 
		 
		private String green="\033[32m"; 
		private String yellow="\033[33m"; 
		private String blue="\033[34m"; 
		private String purple="\033[35m"; 
		private String cyan="\033[36m"; 
		private String white="\033[37m";
	*/
	private String red="\033[31m";
	private String reset="\u001B[0m";
	
	
	
	private  static TextBox myTextBox;
	private  static TextBox myTextBoxDev;
	private ArrayList<String> list;
	private int limit;
	private int repeated = 1;
	private boolean activated = false;
	private String description;
	private boolean newMsg = false;
	
	
	
	//Eraikitzaileak
	private TextBox(String pDesc) {
		this(pDesc, 5);
		
	}
	private TextBox(String pDesc, int pLimit) {
		this.list = new ArrayList<String>();
		this.limit = pLimit;
		this.description = pDesc;
	}
	
	//geters
	static public TextBox getBox() {
		/** 
		 * FUNTZIOA: box TextBox objetua jokalariari jokoaren egoera eta gertaeraz jakinarazkteko erabilia izango da
		 * ERABILPENA: Jokoaren gertaerak gordetzeko erabiltzen da. 
		 * Input: -- 
		 * OutPut: TextBox
		*/
		if (myTextBox == null) {
			myTextBox = new TextBox("Jokoaren Info");
			myTextBox.activated = true;
		}
		return myTextBox;
	}
	static public TextBox getBoxDev() {
		/** 
		 * FUNTZIOA: boxDev TextBox objetua jokalariari jakinarazi ez zaizkion errore eta warning mezuak gordetzeko eta pantaiaratzeko erabilia
		 * ERABILPENA: Jokoaren gertaera ezOhikoak gordetzeko erabiltzen da. Bug-ak kentzeko erabilia ere.
		 * Input: -- 
		 * OutPut: TextBox
		*/
		if (myTextBoxDev == null) {
			myTextBoxDev = new TextBox("DEV", 10);
		}
		return myTextBoxDev;
	}
	
	public Iterator<String> getIterator() {
		
		return this.list.iterator();
	}
	
	//Metodoak
	public void addText(String pText) {
		/** 
		 * FUNTZIOA: String bat textBoxean sartuko da. Sartutako mezua haskenegoa bezalakoa bada 2x bat agertuko , ahu da, mezu hori bi 
		 * 	aldiz sartu da. Mezua berriro sartzen bada 3x agertuko da mezua baino lehen. 3x hori geitzean mezuen arteko konparaketak 
		 * 	3x horrekin egin behar dira. Horretarako 3x (4x,5x,6x,...) mezu berrian jarriko da konparazioa ondo egiteko.
		 * 	textBox-a limitan badago mezu zaharrena kenduko da.
		 * ERABILPENA: Jokalariaren mugimenduak eta jokoaren gertaerak irakurtzeko erabiltzen da. 
		 * Input: String moduan sartu behar den infomazioa. 
		 * OutPut: --
		*/
		
		if(this.list.size() != 0) {
			if(this.list.get(0).equals("x" + this.repeated + " " + pText) || this.list.get(0).equals(pText)) {
				this.repeated = this.repeated + 1;
				this.list.remove(0);
				this.list.add(0, "x" + this.repeated + " " + pText);
			}else {
				this.repeated = 1;
				this.list.add(0, pText);
			}
		}else {
			this.repeated = 1;
			this.list.add(0, pText);
		}
		if(this.list.size() > this.limit){
			
			this.list.remove(this.limit);
			
		}	
		this.newMsg = true;
	}
	
		
	public void printText() {
		/** 
		 * FUNTZIOA: textBox-ean dauden mezu guztiak idatziko dira pantailan. while loop-ean lista guztitik iteratuko da idazteko.
		 * ERABILPENA: loop guztietan idatziko da.
		 * Input: -- 
		 * OutPut: --
		*/
		if(this.activated) {
			String lag = "";
			if(this.newMsg) {
				lag = " | "+ this.red +"�NEW MSG!" + this.reset;
				this.newMsg = false;
			}
				
			
			System.out.println("---------------------------------------------");
			
			System.out.println("TextBox: "+ this.description + lag);
			System.out.println("---------------------------------------------");
			Iterator<String> itr = this.getIterator();
			String current;
			while (itr.hasNext()) {
				current = (String)itr.next();
				System.out.println("-"+current);
			}
		}
		
		/*
		Iterato<String> itr = this.getIterator();
		String current;
		while (itr.hasNext()) {
			current = (String) itr.next();
			System.out.println("-"+current);
		}
		*/
	}
	
	public void activate(boolean pActiv) {
		/** 
		 * FUNTZIOA: pActive-aren arabera activated atributua aldatuko da.
		 * ERABILPENA: textBox-a aktibatu edo desaktibatu nahi bada.
		 * Input: true edo false den parametroa.
		 * OutPut: --
		*/
		this.activated = pActiv;
	}
	public boolean isActivated() {
		/** 
		 * FUNTZIOA: Activated atributua true edo false bada bueltatukodu.
		 * ERABILPENA: Activated true edo false bada jakiteko.
		 * Input: -- 
		 * OutPut: Aktibatuta badago true, bestela false.
		*/
		return this.activated;
	}
	
}
