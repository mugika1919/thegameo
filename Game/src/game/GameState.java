package game;

public class GameState {

	private String State;
	private static GameState nireGameState;
	
	
	//Eraikitzailea
	private GameState() {
		
		//this.ChangeStateMap();
		this.changeStatePantaila();		
	}
	
	//Geters
	public static GameState getGameState() {
		
		if (nireGameState == null)
			nireGameState = new GameState();
		
		return nireGameState;
		
	}
	
	public String getCurrentState() {
		
		return this.State;
		
	}
	
	//Seters
	public void changeStateShop(){
		
		this.State = "Shop";
		TextBox.getBox().addText("Denda batean sartu zara");
		
	}
	
	public void changeStateMap(){

		this.State = "Map";
		TextBox.getBox().addText("Mapa-ra joan zara");
		
	}
	
	public void changeStateFight(){
		
		this.State = "Fight";
		TextBox.getBox().addText("Kontuz! Borrokan sartu zara");
		
	}
	
	public void changeStateEvent(){
		
		this.State = "Event";
		TextBox.getBox().addText("Adi! Ebentu bat");
		
	}
	
	public void changeStatePantaila(){
		
		this.State = "Pantaila";
		
	}
}
