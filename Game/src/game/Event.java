package game;

import java.util.Random;

import cards.Card;
import cards.CardCollection;
import cards.Deck;
import entity.Player;

public class Event {
    private static Event myEvent;

    //eraikitzailea
    private Event() {

    }
    
    //Geters
    public static Event getMyEvent() {
        if (Event.myEvent==null)
            Event.myEvent = new Event();
        return Event.myEvent;
    }



    public static void runEvent() {
    	/** 
		 * FUNTZIOA: Maparen solairuaren arabera, ebentu desberdinak agertuko dira. Ebentu bakoitzak efektu desberdinak izango ditu
		 * 	jokalarian. Ebentua random klasearekin erabakiko da.
		 * ERABILPENA: Ebentu bat agertu behar denean hauetako bat autatuko da.
		 * Input: --
		 * OutPut: --
		*/
        Random r = new Random(System.currentTimeMillis());
        int a = r.nextInt(6)+1;

        
        if (Mapa.getMyMapa().getSolairua()==1) {
        	
        	if (a==1) {
        		System.out.println("Hey! Badirudi gela honetan arriskurik ez dagoela, are gehiago, aurrean kutxa bat ikusten da");
                System.out.println("Kontxo, hau da nere sortea, hemen urrea dago! ");
                System.out.println("+20 dirua");
                Player.getMyPlayer().takeMoney(20);
        	}
        	if (a==2) {
        		System.out.println("Sagarrondo bat aurkitu dut! Hainbat sagar jan izan ahal ditut joan baino lehen");
        		System.out.println("+10 max hp");
        		Player.getMyPlayer().increaseMaxHp(10);
        	}
        	if (a==3) {
        		System.out.println("Hutsik dago gela hau.");
        		System.out.println("Gutxienez ez dut borrokatu beharrik.");
        	}
        	if (a==4) {
        		System.out.println("Txanpon batzuk daude lurrean");
                System.out.println("Ez dira asko, baina ez ditut hor utziko ");
                System.out.println("+8 dirua");
                Player.getMyPlayer().takeMoney(8);
        		
        	}
        	if (a==5) {
        		System.out.println("Aiba! Zizarria dago hor aurrean");
                System.out.println("Hmm... jada hilda dago, ze ondo. ");
        	}
        	if (a==6) {
        		System.out.println("Aiba! Armikaitzak daude hor aurrean");
                System.out.println("Hmm... jada hilda daude, hau da hau nire sortea ");
        	}
        }
        
        if (Mapa.getMyMapa().getSolairua()==2) {
        	
        	if (a==1) {
        		System.out.println("Hey! Badirudi gela honetan arriskurik ez dagoela, are gehiago, aurrean kutxa bat ikusten da");
                System.out.println("Kontxo, hau da nere sortea, hemen urrea dago! ");
                System.out.println("+10 dirua");
                Player.getMyPlayer().takeMoney(10);
        	}
        	if (a==2) {
        		System.out.print("Ah! Hauxe da gela polita, bai horixe, agian lokuluxkatxo bat hartzeko denbora daukat...");
                Player.getMyPlayer().heal(25);
        	}
        	if (a==3) {
        		System.out.println("Hutsik dago gela hau.");
        		System.out.println("Gutxienez ez dut borrokatu beharrik.");
        	}
        	if (a==4) {
        		System.out.print("Ur beroko bainu handi bat dago hemen, a ze gustura nagoen...");
                Player.getMyPlayer().heal(50);
        	}
        	if (a==5) {
        		System.out.println("Hm? Zer da hori? Aiba, gorpua da, baina ez dirudi arriskurik dagoenik.");
                System.out.println("Ezpata bat dago arroka batean sartuta, eta hainbat txanpon daude hortik botata.");
                System.out.println("+10 dirua eta ezpata zahar bat");
                Card Karta1 = CardCollection.getCardCollection().searchCardById(-1).cloneCard();
                Deck.getMyDeck().addCard(Karta1);
                Player.getMyPlayer().takeMoney(10);
                
                System.out.print("        _\n" + 
                		"       (_)\n" + 
                		"       |=|\n" + 
                		"       |=|\n" + 
                		"   /|__|_|__|\\\n" + 
                		"  (    ( )    )\n" + 
                		"   \\|\\/\\\"/\\/|/\n" + 
                		"     |  Y  |\n" + 
                		"     |  |  |\n" + 
                		"     |  |  |\n" + 
                		"    _|  |  |\n" + 
                		" __/ |  |  |\\\n" + 
                		"/  \\|  |  |  \\\n" + 
                		"   __|  |  |   |\n" + 
                		"/\\/ |  |  |   |\\\n" + 
                		" <  +\\ |  |\\ />  \\\n" + 
                		"  >  + \\  | LJ    |\n" + 
                		"       + \\|+  \\  < \\\n" + 
                		"  (O)      +    |    )\n" + 
                		"   |             \\  /\\ \n" + 
                		" ( | )   (o)      \\/  )\n" + 
                		"_\\\\|//__( | )______)_/ \n" + 
                		"        \\\\|//");
        	}
        	if (a==6) {
        		System.out.println("Gela handia, gauza pila leju guztietatik, harriak, saguzarrak, sugeak...");
                System.out.println("Erabilgarria den gauza bakarra dago, ezkutu bat, nahiko polita gainera.");
                Card Karta2 = CardCollection.getCardCollection().searchCardById(-2).cloneCard();
                Deck.getMyDeck().addCard(Karta2);
                System.out.print("|`-._/\\_.-`|\n" + 
                		"  |    ||    |\n" + 
                		"  |___o()o___|\n" + 
                		"  |__((<>))__|\n" + 
                		"  \\   o\\/o   /\n" + 
                		"   \\   ||   /\n" + 
                		"    \\  ||  /\n" + 
                		"      '.||.'\n" + 
                		"       ``");
        	}
        }
        
        if (Mapa.getMyMapa().getSolairua()==3) {
        	
        	if (a==1) {
        		System.out.println("Hey! Badirudi gela honetan arriskurik ez dagoela, are gehiago, aurrean kutxa bat ikusten da");
                System.out.println("Kontxo, hau da nere sortea, hemen urrea dago! ");
                System.out.println("+20 dirua");
                Player.getMyPlayer().takeMoney(20);
        	}
        	if (a==2) {
        		System.out.print("Ah! Hauxe da gela polita, bai horixe, agian lokuluxkatxo bat hartzeko denbora daukat...");
                Player.getMyPlayer().heal(25);
        	}
        	if (a==3) {
        		System.out.println("Hutsik dago gela hau.");
        		System.out.println("Gutxienez ez dut borrokatu beharrik.");
        	}
        	if (a==4) {
        		System.out.print("Ur beroko bainu handi bat dago hemen, a ze gustura nagoen...");
                Player.getMyPlayer().heal(50);
        	}
        	if (a==5) {
        		System.out.println("Hutsik dago gela hau.");
        		System.out.println("Gutxienez ez dut borrokatu beharrik.");
        	}
        	if (a==6) {
        		
        		System.out.println("Laku bat dago hemen! Aiba la... Esku bat uretik atera eta ezpata bat eman dit");
        		System.out.println("Oso oso arma polita da, urrez dekoratuta dago eta aura insartsua dauka");
        		System.out.println("+Excalibur");
        		Card Karta3 = CardCollection.getCardCollection().searchCardById(-101).cloneCard();
                Deck.getMyDeck().addCard(Karta3);
        		System.out.print("::::::::::::::::::::::::::::::::::::::::::::::::::::::\n" + 
        				"::::::::::::::::::::::::::::::::::::::::::::::::::::::\n" + 
        				"::::::::::::::::::::::::::::::::::::::::::::::::::::::\n" + 
        				"::::::::::::::::::::::::::/\\::::::::::::::::::::::::::\n" + 
        				"::::::::::::::::::::::::::||::::::::::::::::::::::::::\n" + 
        				"::::::::::::::::::::::::::||::::::::::::::::::::::::::\n" + 
        				"::::::::::::::::::::::::::||::::::::::::::::::::::::::\n" + 
        				"::::::::::::::::::::::::::||::::::::::::::::::::::::::\n" + 
        				"::::::::::::::::::::::::::||::::::::::::::::::::::::::\n" + 
        				"::::::::::::::::::::::::::||::::::::::::::::::::::::::\n" + 
        				"::::::::::::::::::::::::::||::::::::::::::::::::::::::\n" + 
        				"~^~_-~^^-^~^_~^_^-~^~^-~^ || ~^~_-~^_^-^~^_~^_^-~^~^-~\n" + 
        				"~^-_~^-~^_~^-~^_~^-_ _^,##[]##,~_ _~^-_~^-~^_~^-~^_~^-\n" + 
        				"~^-~^~-_~^__.===~'`__.. ~{ \\ _-..__`'~===.__~^-~^~-_~^\n" + 
        				"~_~^_.=~~'   ~_.==~-.-~ .=||=. ~=.-~==._~^-^'~~=._~_~^\n" + 
        				"~-:`-~^-~^_~^:-~^~-_~-._`-==-'_.=~-_~^-_:~^-~^-_~`;-~\n" + 
        				" ~-'._~^-~^-_^~=._~-~_~-'~~'~`_^-~_^_.=~-~^-_~^-_.'^-\n" + 
        				"_~^-~^~=._~^-~^_-^~~==..,~_^_,..==~~-_~^-~^-_.=~_~^-~^\n" + 
        				"_-~^-~^_~^`~==.__-~^_~^-_~^-_~^-_~^-~__.==~`_-~^-~^_~^\n" + 
        				"-~_~^~-~^-~^~_~^~`~~~==,,....,,==~~~`-~_~^~-~^-~^~_~^~\n" + 
        				" ~jgs^-~^-_~^~^_-^~^-~^~-_~^-~^-~^_~^~-~^~-~^-~^-~^-~^\n" + 
        				" ~^~^-~^-~^_~^~-^~_~^-^~^~^-~^-~^~^~-^~-~^-~^~~-^~-^~^");
        		
        	}
        }
        
        if (Mapa.getMyMapa().getSolairua()==4) {
        	
        	if (a==1) {
        		System.out.println("Hey! Badirudi gela honetan arriskurik ez dagoela, are gehiago, aurrean kutxa bat ikusten da");
                System.out.println("Kontxo, hau da nere sortea, hemen urrea dago! ");
                System.out.println("+30 dirua");
                Player.getMyPlayer().takeMoney(30);
        	}
        	if (a==2) {
        		System.out.println("Hutsik dago gela hau.");
        		System.out.println("Gutxienez ez dut borrokatu beharrik.");
        	}
        	if (a==3) {
        		System.out.print("Ah! Hauxe da gela polita, bai horixe, agian lokuluxkatxo bat hartzeko denbora daukat...");
                Player.getMyPlayer().heal(40);
        	}
        	if (a==4) {
        		System.out.println("Aiba la letxe! Urrez betetako kriston kutxa da hori!");
                System.out.println("+300 dirua ");
                System.out.println("          |                   |                  |                     |\n" + 
                		" _________|________________.=\"\"_;=.______________|_____________________|_______\n" + 
                		"|                   |  ,-\"_,=\"\"     `\"=.|                  |\n" + 
                		"|___________________|__\"=._o`\"-._        `\"=.______________|___________________\n" + 
                		"          |                `\"=._o`\"=._      _`\"=._                     |\n" + 
                		" _________|_____________________:=._o \"=._.\"_.-=\"'\"=.__________________|_______\n" + 
                		"|                   |    __.--\" , ; `\"=._o.\" ,-\"\"\"-._ \".   |\n" + 
                		"|___________________|_._\"  ,. .` ` `` ,  `\"-._\"-._   \". '__|___________________\n" + 
                		"          |           |o`\"=._` , \"` `; .\". ,  \"-._\"-._; ;              |\n" + 
                		" _________|___________| ;`-.o`\"=._; .\" ` '`.\"\\` . \"-._ /_______________|_______\n" + 
                		"|                   | |o;    `\"-.o`\"=._``  '` \" ,__.--o;   |\n" + 
                		"|___________________|_| ;     (#) `-.o `\"=.`_.--\"_o.-; ;___|___________________\n" + 
                		"____/______/______/___|o;._    \"      `\".o|o_.--\"    ;o;____/______/______/____\n" + 
                		"/______/______/______/_\"=._o--._        ; | ;        ; ;/______/______/______/_\n" + 
                		"____/______/______/______/__\"=._o--._   ;o|o;     _._;o;____/______/______/____\n" + 
                		"/______/______/______/______/____\"=._o._; | ;_.--\"o.--\"_/______/______/______/_\n" + 
                		"____/______/______/______/______/_____\"=.o|o_.--\"\"___/______/______/______/____\n" + 
                		"/______/______/______/______/______/______/______/______/______/______/");
                System.out.println("Hmm... Itxaron momentu bat, hau ez da urrea, txokolatezko txanponak dira!");
                System.out.println("-300 dirua-->+15 max hp ");
                Player.getMyPlayer().increaseMaxHp(15);
        	}
        	if (a==5) {
        		System.out.println("Hutsik dago gela hau.");
        		System.out.println("Gutxienez ez dut borrokatu beharrik.");
        	}
        	if (a==6) {
        		System.out.println("Hala ze ezkutu polita, oso gogorra dirudi ere!");
        		System.out.println("+Perseoren ezkutua");
        		Card Karta4 = CardCollection.getCardCollection().searchCardById(-102).cloneCard();
                Deck.getMyDeck().addCard(Karta4);
        		System.out.print(" _________________________ \n" + 
        				"|<><><>      |  |     <><><>|\n" + 
        				"|<>          |  |         <>|\n" + 
        				"|            |  |           |\n" + 
        				"|  (______ <\\-/> ______)   |\n" + 
        				"| //_.-=-.\\| \" |/.-=-._\\ | \n" + 
        				"|   /_    \\(o_o)/    _\\   |\n" + 
        				"|  /_  /\\/ ^ \\/\\  _\\    |\n" + 
        				"|     \\/ | / \\ | \\/      |\n" + 
        				"|_______ /(((( )))\\ _______|\n" + 
        				"|     __\\ \\___// //__     |\n" + 
        				"|---  (((---'    '---))) ---|\n" + 
        				"|            |  |           |\n" + 
        				"|            |  |           |\n" + 
        				"::           |  |          ::     \n" + 
        				" \\<>        |  |       <>//      \n" + 
        				"  \\<>       |  |      <>//       \n" + 
        				"   \\<>      |  |     <>//       \n" + 
        				"    `\\<>    |  |   <>//'         \n" + 
        				"      `\\<>  |  |  <>//'         \n" + 
        				"        `\\<>|  |<>//'         \n" + 
        				"          `-.  .-`           \n" + 
        				"            '--'");
        	}
        }
        
        if (Mapa.getMyMapa().getSolairua()==5) {
        	
        	if (a==1) {
        		System.out.println("Hey! Badirudi gela honetan arriskurik ez dagoela, are gehiago, aurrean kutxa bat ikusten da");
                System.out.println("Kontxo, hau da nere sortea, hemen urrea dago! ");
                System.out.println("+40 dirua");
                Player.getMyPlayer().takeMoney(40);
        	}
        	if (a==2) {
        		System.out.println("Hutsik dago gela hau.");
        		System.out.println("Gutxienez ez dut borrokatu beharrik.");
        	}
        	if (a==3) {
        		System.out.print("Ur beroko bainu handi bat dago hemen, a ze gustura nagoen...");
                Player.getMyPlayer().heal(50);
        	}
        	if (a==4) {
        		System.out.println("Txanponak daude zorutik botata");
                System.out.println("+12 dirua");
                Player.getMyPlayer().takeMoney(12);
        	}
        	if (a==5) {
        		System.out.println("Liburu zahar bat dago hemen: Trukea");
                System.out.println("Magia bat da antza denez, odol magia alegia");
                Card Karta5 = CardCollection.getCardCollection().searchCardById(-103).cloneCard();
                Deck.getMyDeck().addCard(Karta5);
        	}
        	if (a==6) {
        		System.out.println("Zerbaitekin tropiezo egin duzu, kasko moduko bat da, baina hasieran ez duzu ikusi.");
                System.out.println("Hadesen Kaskoa aurkitu dut");
                Card Karta6 = CardCollection.getCardCollection().searchCardById(-104).cloneCard();
                Deck.getMyDeck().addCard(Karta6);
                System.out.print("___________________\n" + 
                		"      /.-------+-+-------.\\\n" + 
                		"     //        :|:     :::\\\\\n" + 
                		"    //         :|:       ::\\\\\n" + 
                		"   //          :|:       :::\\\\\n" + 
                		"  //           :|:       ::::\\\\\n" + 
                		" //            :|:         :::\\\\\n" + 
                		"((             :|:          :::))\n" + 
                		"||     _______/:|:\\_______   ::||\n" + 
                		"||    /.-----.\\:|:/.-----.\\ :.:||\n" + 
                		"||   ((       ))|((       ))  :||\n" + 
                		"||    \\\\_    //:|:\\\\    _//   :||\n" + 
                		"||:     \\\\  (( :|: ))  //     :||\n" + 
                		"||::     \\\\  \\\\:|://  //     ::||\n" + 
                		"||::      \\\\  \\\\_//  //     :::||\n" + 
                		"||::       \\\\  `-'  //     ::::||\n" + 
                		"||::        ))     ((  <<()>>::||\n" + 
                		"||::       //       \\\\   ::::::||\n" + 
                		"((::      //         \\\\ :::::::))\n" + 
                		" \\\\______//           \\\\______//\n" + 
                		"  `------'             `------'");
        	}
        }
        
        if (Mapa.getMyMapa().getSolairua()==6) {
        	
        	if (a==1) {
        		System.out.println("Hey! Badirudi gela honetan arriskurik ez dagoela, are gehiago, aurrean kutxa bat ikusten da");
                System.out.println("Kontxo, hau da nere sortea, hemen urrea dago! ");
                System.out.println("+40 dirua");
                Player.getMyPlayer().takeMoney(40);
        	}
        	if (a==2) {
        		System.out.println("Hutsik dago gela hau.");
        		System.out.println("Gutxienez ez dut borrokatu beharrik.");
        	}
        	if (a==3) {
        		System.out.print("Ur beroko bainu handi bat dago hemen, a ze gustura nagoen...");
                Player.getMyPlayer().heal(50);
        	}
        	if (a==4) {
        		System.out.println("Liburu zahar bat dago hemen: Trukea");
                System.out.println("Magia bat da antza denez, odol magia alegia");
                Card Karta7 = CardCollection.getCardCollection().searchCardById(-103).cloneCard();
                Deck.getMyDeck().addCard(Karta7);
        	}
        	if (a==5 || a==6) {
        		System.out.println("Mailu bat dago lurrean, altxatxerakoan pisutsua iruditu zait, baina orain nahiko eramangarria da");
                System.out.println("Mjolnir bat aurkitu dut");
                Card Karta8 = CardCollection.getCardCollection().searchCardById(-105).cloneCard();
                Deck.getMyDeck().addCard(Karta8);
                System.out.print(" _______      _______ \n" + 
                		"||      \\_()_/      ||\n" + 
                		"||      |    |      ||\n" + 
                		"||      |    |      ||\n" + 
                		"||      |____|      ||\n" + 
                		"||______/ || \\______||\n" + 
                		"          ||  \n" + 
                		"          ||\n" + 
                		"          ||\n" + 
                		"          XX\n" + 
                		"          XX\n" + 
                		"          XX\n" + 
                		"          XX\n" + 
                		"          OO");
        	}
        }
        
        if (Mapa.getMyMapa().getSolairua()==7) {
        	
        	if (a==1||a==2||a==3) {
        		System.out.println("Urre pila dago hemen botata, a ze arraroa");
                System.out.println("+50 dirua");
                Player.getMyPlayer().takeMoney(50);
        	}
        	if (a==4||a==5||a==6) {
        		System.out.println("Oso kalmatua dago guztia");
        		System.out.println("Kalmatuegia agian...");
        	}
        }
    }

}