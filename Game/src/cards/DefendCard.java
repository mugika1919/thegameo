package cards;

import entity.Entity;

public class DefendCard extends Card{ //TODO Hemen behar bada health egin daiteke
	
	public DefendCard(String pName, int pId,int pPower, int pCost, int pPrice) {
		super(pName, pId, pPower, pCost, pPrice);
	}

	public void useCard(Entity pOwner, Entity pTarget){
		/** 
		 * FUNTZIOA: Defentsa karta erabiliko du pOwner-ean, jadakin karta erabilita ez badago eta pOwner-ak energia nahikoa badu. Honek pOwner*aren defentsa probisionala handituko du.
		 * ERABILPENA: Karta erabiliko da etsaia zein jokalaria defendatseko. 
		 * Input: Entity motatako 2 objetu: pOwner eta pTarget
		 * OutPut: --
		*/
		if(pOwner != null) {
			if(!this.isUsed() && pOwner.enoughEnergy(this.getEnergyCost())) {
				pOwner.spendEnergy(this.getEnergyCost());
				pOwner.increaseDefense(this.getPower());
				this.use();
					
			}
		}
		
	}
	
	public Card cloneCard() {
		/** 
		 * FUNTZIOA: DefendCard klaseko karta kopiatzen du, atributuen balio berdinak dituen beste DefendCard bat bueltzatuz.
		 * ERABILPENA: Karta bakarra diseinatu ondoren honen kopia independenteak egin ahal izateko erabilia. 
		 * Input: --
		 * OutPut: Card (DefendCard) // AttackCard motatako objetua Card eran bueltatu.
		*/
		Card pCard = new DefendCard(this.getName(), this.getId(), this.getPower(), this.getEnergyCost(),this.getPrice());
		pCard.setDescription(this.getDesc()); 
		return pCard;
	}
	
	@Override
	public String getRealTarget(Entity pUser, Entity pTarget) {
		/** 
		 * FUNTZIOA: DefendCard-aren target erreala bueltatuko du, hau da, pOwner-en objetuaren izena.
		 * ERABILPENA: TextBox-ean karta nori zuzendua izan den jakiteko. 
		 * Input: Entity motatako 2 objetu: pOwner eta pTarget
		 * OutPut: String // entitatearen izena 
		*/
		String ema = "Ez dago";
		if(pTarget != null) {
			ema = pUser.getInstanceSring();;
		}
		return ema; //TODO
	}
	
}
