package cards;
import java.util.Random;

import entity.Entity;
import entity.Player;
import game.Battle;
import game.TextBox;

//EMA KLASEA
public class Deck {
	
	private CardList[] fullDeck; 	//{primaryDeck(0), discardDeck(1), currentHand(2)}
	
	private static Deck myDeck; 
	
	private Deck() {
		
		this.fullDeck = new CardList[3];
		for(int i = 0; i < this.fullDeck.length; i++) {
			this.fullDeck[i] = new CardList();
		}

		for(int i = 1; i <= 5; i++){
			this.addCard(1101); //Ataque 
			this.addCard(1201);	//Defensa
		}
		
	}
	
	public static Deck getMyDeck() {
		if(Deck.myDeck == null)
			Deck.myDeck = new Deck();
		return Deck.myDeck;
	}
	
	
	private boolean validDeckNumber(int pDeckNumber) {
		/** 
		 * FUNTZIOA: Parametro bitartez sartutako deck zenbakia egokia den ala ez adierazi
		 * ERABILPENA: hurrengo metodoetan deck zenbakia eskatu ondoren zenbaki ona dela zihurtatzeko
		 * Input: pDeckNumber: int
		 * OutPut: boolean
		*/
		
		return 0 <= pDeckNumber && pDeckNumber < this.fullDeck.length;
	}
	
	public int deckSize(int pDeck){ // POSIBLE ERROR AQUI!! CAMBIADO NUMEROS
		/** 
		 * FUNTZIOA: Deck osoaren edo zati baten tamaina eman sartutako int parametroaren arabera:
		 * 				� 0 : primaryDeck
		 * 				� 1 : discardDeck
		 * 				� 2 : currentHand
		 * 				� Beste edozein zenbaki : Aurreko 3 deck zatien karten batura.
		 * ERABILPENA: Deck bakoitzean edo deck osoan zenbat karta dauden jakiteko
		 * Input: pDeckNumber: int
		 * OutPut: int
		*/
		
		int ema = 0;
		if(this.validDeckNumber(pDeck)) {
			ema = this.fullDeck[pDeck].size();
		}else {
			for(int i = 0; i < this.fullDeck.length; i++) {
				ema = ema + this.fullDeck[i].size();
			}	
		}
		return ema;
	}
	
	public Card searchCardById(int pId) { //TODO ez da inon erabiltzen Valorar eliminacion
		/** 
		 * FUNTZIOA: Id bat emanda fullDeck-ean ide hori duen lehenengo karta bilatzen du eta bueltatzen du.
		 * ERABILPENA: --
		 * Input: pId: int
		 * OutPut: Card
		*/
		Card ema = null;
		int kont = 0;
		int length = this.fullDeck.length;
		while(kont < length && ema == null) {
			ema = this.fullDeck[kont].searchCardById(pId);
			kont = kont + 1;
		}
		
		return ema;
  	}
	
	public Card searchCardWithSameId(Card pCard) {//TODO ez da erabiltzen valorar eliminacion
		/** 
		 * FUNTZIOA: Karta bat emanda fullDeck-ean emandako kartearen id-a duen lehenengo karta bilatzen du eta bueltatzen du.
		 * ERABILPENA: --
		 * Input: Card pCard
		 * OutPut: Card
		*/
		Card ema = null;
		int kont = 0;
		int length = this.fullDeck.length;
		while(kont < length && ema == null) {
			ema = this.fullDeck[kont].cardWhithSameId(pCard);
			kont = kont + 1;
		}
		
		return ema;
	}
	
	public boolean hasCard(Card pCard) {//TODO ez da erabiltzen valorar eliminacion
		/** 
		 * FUNTZIOA: Karta bat emanda fullDeck-ean karta hori badagoen esaten du (objetu bera .equals() erabili).
		 * ERABILPENA: --
		 * Input: Card pCard
		 * OutPut: boolean
		*/
		boolean ema = false;
		int kont = 0;
		int length = this.fullDeck.length;
		while(kont < length && !ema) {
			ema = this.fullDeck[kont].hasCard(pCard);
			kont = kont + 1;
		}
		
		return ema;

	}
	public boolean hasCardWithId(int pId) {//TODO ez da erabiltzen valorar eliminacion
		/** 
		 * FUNTZIOA: Id bat emanda fullDeck-ean karta hori badagoen esaten du.
		 * ERABILPENA: --
		 * Input: pId: int
		 * OutPut: boolean
		*/
		return null != this.searchCardById(pId);
	}
	
	public void addCard(int pId) { //TODO se usa una vez y se usa localmente: Valorar private o eliminacion
		/** 
		 * FUNTZIOA: Id bat emanda CardCollection listatik id hori duen karta klonatu eta primaryDeck-ean gehitzen du.
		 * ERABILPENA: Kartak deck-ean id-aren bitartez zuzenenan gehitzeko
		 * Input: pId: int
		 * OutPut: --
		*/
		Card card = CardCollection.getCardCollection().searchCardById(pId);
		if(card != null) {
			card = card.cloneCard();
			this.fullDeck[0].addCard(card);
		}
	}
	
	public void addCard(Card pCard) {
		/** 
		 * FUNTZIOA: Karta bat emanda hau primaryDeck-ean gehitzen du. Ez du honen kopiarik egiten eta deste 2 deck-etan karta baldin badago ez da gehituko
		 * ERABILPENA: Kartak deck-ean zuzenenan gehitzeko
		 * Input: pCard: Card 
		 * OutPut: --
		*/
		if(pCard != null) {
			if(!this.fullDeck[1].hasCard(pCard) && !this.fullDeck[2].hasCard(pCard)) {
				this.fullDeck[0].addCard(pCard);
			}
		}
	}
	
	public void addCards(CardList cl) { //TODO Ez da erabiltzen valorar eliminacion
		/** 
		 * FUNTZIOA: Karten lista bat emanda lista honetan dauden karta guztien kopia primaryDeck-ean gehitzen ditu.
		 * ERABILPENA: Karten Lista bat primaryDeck-ean zuzenenan gehitzeko
		 * Input: CardList cl 
		 * OutPut: --
		*/
		this.addCards(cl, 0);
	
	}
	
	public void addCards(CardList cl, int pDeck) {//TODO Ez da erabiltzen valorar eliminacion
		/** 
		 * FUNTZIOA: Karten lista bat emanda lista honetan dauden karta guztien kopia pDeck zenbakiaz zehaztutako azpi-deckean gehitzen ditu. Zehaztutako
		 * 			deck zenbakia txarto egongo balitz primaryDeck-ean gehituko ditu.
		 * ERABILPENA: Karten Lista bat deckaren zati batean zuzenenan gehitzeko
		 * Input: CardList cl, int pDeck
		 * OutPut: --
		*/
		//Evitar duplicados
		int deckNum = 0;
		int length = this.fullDeck.length;
		CardList clDup = cl.duplicateCardlist();
		
		if(0 <= pDeck && pDeck < length) {
			deckNum = pDeck;
		}
		
		clDup.moveAllCardsToTarget(this.fullDeck[deckNum]);
	}
	
	public void useCardInPos(int pPosInHand) throws Exception{ //TODO Este metodo esta bien pero es un poco caotico. Podemos hacer que el use card trowee excepciones cuando no pueda usar las cartas y quitar el atributo used entonces
		/** 
		 * FUNTZIOA: Zenbaki bat emanda eta hau currentHand listaren pozizio bezala interpretatuz, pozizio honetan dagoen karta erabiliko du. 
		 * 			Karta erabiltzeko bataia bat habian egon beharko da.
		 * 			Bataian etsairik ez balego edo karta erabili ostean benetan ez bada erabili orduan salbuespen bat botako du. Salbuespen hori
		 * 			karta ez erabltzearen errakizuna mezua izango du barne.
		 * ERABILPENA: Karten Lista bat deckaren zati batean zuzenenan gehitzeko
		 * Input: CardList cl, int pDeck
		 * OutPut: --
		*/
		Card car = this.fullDeck[2].getCardInPos(pPosInHand);
		
		Entity target, user;
		
		if(car != null) {
			if(Battle.getCurrentBatle() != null) {
				user = Player.getMyPlayer();
				target = Battle.getCurrentBatle().getEnemy();
				if(target != null) {
					car.useCard(user, target);
					if(car.isUsed()) {
						this.fullDeck[2].removeCard(car);
						this.fullDeck[1].addCard(car);
						TextBox.getBox().addText(car.getName() + " karta erabili da "+ car.getRealTarget(user, target) + "n");
						
					}else {
						throw new Exception("Ez du energiarik karta hori erabiltzeko");
					}
				}else {
					throw new Exception("Ez duzu etsairik hautatu");
				}
			}
			
		}else {
			throw new Exception("Ez dago kartarik posizio horretan" + pPosInHand);
		}
	}
	
	public void getRandomCardsToHand(int pKartaKop) {
		/** 
		 * FUNTZIOA: primaryDeck-etik currentHand-era pKartaKop karta kopuru auzaz mugituko ditu. primaryDeck-ean karta nahikorik egongo
		 * 			ez balitz discardDeck-etik kartak primaryDeck-era pasatu eta mugitzeko falta diren kartak mugituko ditu primaryDeck-etik currentHand-era.
		 * 			Hau egin ondoren mugitzeko karta gehiago egongo balitz TextBoxDev-ean errore mezua pataiaratu eta TextBox ean gertatutakoaren berri emango du
		 * 			era pausoago batean. 
		 * ERABILPENA: Kartak lapurtzeko
		 * Input: int pKartaKop 
		 * OutPut: --
		*/
		int kont = 	pKartaKop;
		int maxTry = 0;
		boolean exit = false;
		Random r = new Random(); //To be more random  Random r = new Random(System.currentTimeMillis());
		
		while(kont > 0 && maxTry <= 3 && !exit) {
			
			if(this.fullDeck[0].size() == 0) {
				this.discardToPrimary();
			}
			if(this.fullDeck[0].size() != 0) {
				
				int randInt = r.nextInt(this.fullDeck[0].size()); 
				
				Card card = this.fullDeck[0].getCardInPos(randInt);
				try {
					this.fullDeck[0].removeCard(card);
					this.fullDeck[2].addCard(card);
					TextBox.getBoxDev().addText(" CARDNUM" + kont + this.fullDeck[2].size());
					kont = kont - 1;
					
				}catch(Exception e) {
					TextBox.getBoxDev().addText(e.getMessage());
					maxTry = maxTry + 1;
				}
				
			}else {
				exit = true;
			}
		}
		if(exit) {
			TextBox.getBoxDev().addText("ERROREA: Ezin izan da karta gehiago lapurtu, discardDeck-ean kartarik gelditzen ez delako. Faltan: " + (pKartaKop - kont));
			
			TextBox.getBox().addText("Lapurtzeko falta izan diren kartak: " + (pKartaKop - kont));
		}
	}
	/* //TODO no se usa valorar eliminacion
	public CardList getCardsOnHand() {
		
		return this.fullDeck[2];
	}
	*/
	
	public void discardCurrentHand() {
		/** 
		 * FUNTZIOA: currentHand-ean dauden kartak discardPile-ra mugitzen ditu
		 * ERABILPENA: Kartak deskartatzeko/baztertzeko
		 * Input: --
		 * OutPut: --
		*/
		this.fullDeck[2].moveAllCardsToTarget(this.fullDeck[1]);
	}
	
	public void restart() {
		/** 
		 * FUNTZIOA: karta guztiak primaryDeck-era mugitzen ditu, hau da currentHand eta discardDeck-ean dauden kartak
		 * ERABILPENA: deck-a reiniziatzeko
		 * Input: -- 
		 * OutPut: --
		*/
		this.discardToPrimary();
		this.fullDeck[2].moveAllCardsToTarget(this.fullDeck[0]);
	}
	
	private void discardToPrimary() {
		/** 
		 * FUNTZIOA: discardDeck-eko kartak banan banan primaryDeck-era mugitzen ditu hauek banan banan reiniziatuz
		 * ERABILPENA: kartak berriro zikloan jartzeko
		 * Input: --
		 * OutPut: --
		*/
		if(this.fullDeck[1].size() != 0) {
			this.fullDeck[1].resetCards();
			
			this.fullDeck[1].moveAllCardsToTarget(this.fullDeck[0]);
		}
		
	}
	
	public void removeCard(Card pCard) { //TODO ez da erabiltzen: valorar eliminacion
		/** 
		 * FUNTZIOA: karta bat emanda karta hau deck-etik borratzen du. 
		 * ERABILPENA: 
		 * Input: -- 
		 * OutPut: --
		*/
		
		int kont = 0;
		boolean atera = false;
		
		if(Battle.getCurrentBatle() == null) {
			do {
				try {
					this.fullDeck[kont].removeCard(pCard);
					atera = true;
					
				}catch(Exception e) {
					kont = kont + 1;
				}
			}while(!atera && kont < this.fullDeck.length);
		}
		if(atera) {
			TextBox.getBoxDev().addText("Ezin izan da karta kendu");
		}
		
	}
	
	public void removeCardFirstById(int pId) { //TODO Ez da erabiltzen valorar eliminacion
		/** 
		 * FUNTZIOA: id bat emanda id hori duen lehenengo karta deck-etik borratzen du. 
		 * ERABILPENA: 
		 * Input: int pId
		 * OutPut: --
		*/
		int count = this.fullDeck.length - 1;
		boolean ondo = false;
		
		do {	
			try {
				this.fullDeck[count].removeFirstCardById(pId);
				ondo = true;
			}catch (Exception e){
				count = count - 1;
			}
			
		}while(count >= 0 && !ondo);
		
		if(!ondo) {
			System.out.println("Kendu nahi den karta ez dago listen barnean ID: " + pId);
		}
		
	}
	/*//TODO no se usa: valorar eliminacion
	public String getHandTable() {
		return this.getCardTable(2,-1);
	}
	*/
	
	
	public String getCardTable(int pDeck, int pSelection) {
		/** 
		 * FUNTZIOA: deck zenbakia eta aukeraketa zenbaki bat emanda deck zati horretan dauden karten tabla horizontala bueltatzen du.
		 * 			aukeraketa zenbakia negatiboa bada taula noramala bueltatuko du, baina zenbaki positibo bat edo zero bada, deck zati horren
		 * 			horren barnean pSelection pozizioan dagoen karta erresaltatuko du. pDeck parametroa zenbaki ez erabilgarria izango balu currentHand
		 * 			deck zatia hartuko du balio lehenetsi bezala.
		 * ERABILPENA: Kartak bistaratzeko pantaian
		 * Input: int pDeck, int pSelection 
		 * OutPut: String (Karta taula horizontala)
		*/

		String[] strList;
		int deck = 2;
		if(this.validDeckNumber(pDeck)) {
			deck = pDeck;
		}
		strList = this.fullDeck[deck].getCardsInfoAsString();

		
		return this.getCardTableMaster(strList, pSelection);
		
		
	}
	
	public String getCardTable(int pDeck, int pSelection, int pMaxShown) {
		/** 
		 * FUNTZIOA: deck zenbakia, aukeraketa zenbaki bat emanda eta lerro berean bistaratuko diren karta maximo bat emanda lista horretan
		 * 			dauden karten taula bueltatuko du. Kasu honetan gehienezko karta kopuru zehatz bat horizontalki jarriko ditu eta maximo
		 * 			hori baino karta gehiago egongo balira orduan lerro berri batean faltatzen direnak jarriko ditu.
		 * ERABILPENA: Kartak bistaratzeko pantaian
		 * Input: int pDeck, int pSelection, int pMaxShown 
		 * OutPut: String (Karta taula horizontal zatitua)
		*/
		String[] strList;
		String ema = "";
		
		int deck = 2;
		if(this.validDeckNumber(pDeck)) {
			deck = pDeck;
		}
		strList = this.fullDeck[deck].getCardsInfoAsString();

		
		int botLimit = 0;
		int topLimit = botLimit + pMaxShown-1;
		
		String[] strListZatia;
		boolean atera = false;
		while(!atera) {
			if(topLimit >= strList.length){
				topLimit = strList.length-1;
				atera = true;
			}
			int lag = 0;
			strListZatia = new String[topLimit - botLimit + 1];
			while(lag <= topLimit - botLimit) {
				strListZatia[lag] = strList[botLimit+lag];
				lag = lag + 1;
			}
			
			int selectionLag = pSelection;
			if(selectionLag >= 0) {
				if(selectionLag>=botLimit && selectionLag<=topLimit) {
					selectionLag = selectionLag - botLimit;
				}else {
					selectionLag = strList.length;
				}
			}
			
			
			ema = ema + getCardTableMaster(strListZatia, selectionLag) + "\n";
			botLimit = topLimit + 1;
			topLimit = topLimit + pMaxShown;
			if(botLimit >= strList.length) {
				atera = true;
			}
			
		}
		return ema;
		
	}
	
	
	private String getCardTableMaster(String[] strList, int pSelection) {
		/** 
		 * FUNTZIOA: Algoritmo honek karten informazioen array bat eta aukeraketa zenbaki bat emanez tabla horizontala sortuko du.
		 * 			Aukeraketa zenbakia negatiboa bada taula normala bueltatuko du, baina zenbaki positibo bat edo zero bada, array
		 * 			horren barnean pSelection pozizioan dagoen karta erresaltatuko du.
		 * Input: int pDeck, int pSelection, int pMaxShown 
		 * OutPut: String (Karta taula horizontal zatitua)
		*/
		boolean selectionOn = false;
		if(pSelection >= 0) {
			selectionOn = true;
		}
		
		
		String ema = "";
		if(strList.length != 0) {
			
			int kont = 0;
			int maxBlocks = 0;
			int tableLenTot = 0;
			
			String[][] strKartakGuzt = new String[strList.length][];
			
			//Kartak banandu
			while(kont < strList.length) {
				
				strKartakGuzt[kont] = strList[kont].split("\n");
				if(maxBlocks < strKartakGuzt[kont].length) {
					maxBlocks = strKartakGuzt[kont].length;
				}
				
				kont = kont + 1;
			}
			
			
			
			
			
			//Karten Informazioa lotu
			String mark = "|";
			kont = 0;
			int lerroa = 0;
			
			while(lerroa < maxBlocks) {
				kont = 0;
				while(kont < strKartakGuzt.length) {
					
					if(selectionOn) {
						if(pSelection == kont) {
							mark = "|";
						}else {
							mark = " ";
						}
					}
					
					if(lerroa < strKartakGuzt[kont].length ) {
						ema = ema + mark + strKartakGuzt[kont][lerroa] +  mark;
					}else {
						String full = new String(new char[strKartakGuzt[kont][0].length()]).replace("\0", " ");
						ema = ema +  mark + full +  mark;
					}
					
					if(lerroa == 0) {
						tableLenTot = ema.length();
					}
					
					kont = kont + 1;
					
				}
				ema = ema + "\n";
				lerroa = lerroa + 1;
			}
			
			String full = new String(new char[tableLenTot]).replace("\0", "-");
			ema = full + "\n" + ema + full;
		}else {
			ema = "Ez dago kartarik";
		}
		
		return ema;
		
	}
	
}
