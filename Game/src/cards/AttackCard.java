package cards;

import entity.Entity;

public class AttackCard extends Card{ //TODO repasar por que se pueden eliminar 2 atributos si hay ganas
	
	//Atributuak
	private int status[]  = {0, 0}; //{veneno 1, vulnerable 2} <- Power
	private int statusN = 0;
	private int statPower = 0;
	
	//Eraikitzaileak
	public AttackCard(String pName, int pId,int pPower, int pEnergyCost, int pPrice, int pStatus, int pStatPower) {
		super(pName, pId, pPower, pEnergyCost, pPrice);
		this.statusN = pStatus;
		this.statPower = pStatPower;
		this.translateSpecial();
		
	}
	
	public void useCard(Entity pOwner, Entity pTarget) {
		/** 
		 * FUNTZIOA: Eraso Karta erabiliko du pTarget-ean. Hau da, pTarget-ari min egingo dio eta karta honek efektuak jartzeko gai balitz, orduan pTarget-arik efektuak gehituko lizkioke.
		 * 			bakarrik karta erabiliko da pOwner-ak Enegia nahikoa badu eta karta urretik erabilita ez badago nozki. (Kartaren used atributua = false bada)
		 * ERABILPENA: Karta erabiliko da etsaia zein jokalaria ahultzeko. 
		 * Input: Entity motatako 2 objetu: pOwner eta pTarget (Ez nuluak)
		 * OutPut: -- 
		*/
		if(pOwner != null && pTarget != null) {
			if(!this.isUsed() && pOwner.enoughEnergy(this.getEnergyCost())) {
				pOwner.spendEnergy(this.getEnergyCost());
				pTarget.getHurt(this.getPower());
				this.useSpecial(pTarget);
				this.use();
			}
		}
		
	}
	
	public Card cloneCard() {
		/** 
		 * FUNTZIOA: AttackCard klaseko karta kopiatzen du, atributuen balio berdinak dituen beste AttackCard.
		 * ERABILPENA: Karta bakarra diseinatu ondoren honen kopia independenteak egin ahal izateko erabilia. 
		 * Input: --
		 * OutPut: Card (AttackCard) // AttackCard motatako objetua Card eran bueltatu.
		*/
		Card pCard = new AttackCard(this.getName(), this.getId(), this.getPower(), this.getEnergyCost(), this.getPrice(), this.statusN, this.statPower);
		pCard.setDescription(this.getDesc());
		return pCard;
	}
	
	@Override
	public String getRealTarget(Entity pUser, Entity pTarget) {
		/** 
		 * FUNTZIOA: AttackCard-aren target erreala bueltatuko du, hau da, pTarget-en objetuaren izena. pTargeta nulua izango balitz rduan "Ez dago" bueltatuko luke
		 * ERABILPENA: TextBox-ean karta nori zuzendua izan den jakiteko. 
		 * Input: Entity motatako 2 objetu: pOwner eta pTarget
		 * OutPut: String // entitatearen izena 
		*/
		String ema = "Ez dago";
		if(pTarget != null) {
			ema = pTarget.getInstanceSring();
		}
		return ema; //TODO
	}
	
	//Lana banatzeko metodo pribatuak
	private void useSpecial(Entity pTarget) {
		/** 
		 * FUNTZIOA: AttackCard objetua efektuak jartzeko ahalmena balu, efektuak pTarget-ari aplikatuko lizkioke.
		 * ERABILPENA: Efektuak entitateetan jartzeko 
		 * Input: Entity motatako objetu bakarra: pTarget
		 * OutPut: --
		*/
		if(this.status[0] != 0) {
			pTarget.setVeneno(this.status[0]);;
		}
		if(this.status[1] != 0) {
			pTarget.setVulnerable(this.status[1]);;
		}
	}
	private void translateSpecial() {	//TODO AHORA EXISTE EL MODO 3: veneno + vulnerable
		/** 
		 * FUNTZIOA: AttackCard objetua sotzerakoan efektuak era sistematiko batean egokitu.
		 * 			3 era:
		 * 				� this.estatusN = 1 -> status[0] = this.power
		 * 				� this.estatusN = 2 -> status[1] = this.power
		 * 				� this.estatusN = 3 -> aurreko biak
		 *				� Bestela -> ezer
						
		 * ERABILPENA: Efektuak status[] array-ean sartzeko 
		 * Input: --
		 * OutPut: --
		*/
		
		if(this.statusN > 0 && this.statusN <= 3) {
			if(this.statusN == 1 || this.statusN == 2) {
				this.status[this.statusN - 1] = this.statPower;
			}else {
				this.status[0] = this.statPower;
				this.status[1] = this.statPower;
			}
			
		}
	}
	

}
