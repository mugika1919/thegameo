package cards;

import entity.Entity;
import entity.Player;
import game.Battle;
import game.TextBox;

public class DynamicCard extends Card{
	
	private int type;	
	private boolean noEnergyCost = false;
	
	public DynamicCard(String pName, int pId, int pPower, int pCost, int pPrice, int pType) {
		super(pName, pId, pPower, pCost, pPrice);
		this.type = pType; 
		this.energiarikGastatukoBahalDu();
	}
	
	private void energiarikGastatukoBahalDu() {
		/** 
		 * FUNTZIOA: Karta type-aren arabera energia ala bizitza gastatuko duen ala ez zehaztu. 
		 * ERABILPENA: Bizitza ala energia erabili ahal izateko kartak erabiltzeko
		 * Input: --
		 * OutPut: --
		*/
		int t = this.type;
		if(t == -10) {
			this.noEnergyCost = true;
		}
	}
	
	public void useCard(Entity pOwner, Entity pTarget) {
		/** 
		 * FUNTZIOA: Dynamic karta erabiliko du, jadakin karta erabilita ez badago eta pOwner-ak energia nahikoa badu. Honek karta honen type atributuaren arabera eragin ezberdina izango du.
		 * ERABILPENA: Karta erabiliko da etsaia zein jokalaria defendatseko edo erasotzeko. 
		 * Input: Entity motatako 2 objetu: pOwner eta pTarget ez null
		 * OutPut: --
		*/
		if(pOwner != null && pTarget != null) {
			if(!this.isUsed() && (pOwner.enoughEnergy(this.getEnergyCost()) || this.noEnergyCost)) {
				if(!this.noEnergyCost) {
					pOwner.spendEnergy(this.getEnergyCost());
				}
				
				if ((this.type==2 || this.type==-1)) {
					//Type 2 || -1: Min egin Target -> Deck-aren tamainaren arabera eta bataiaren rondaren arabera(Bataiarik badago)
					if(Battle.getCurrentBatle() != null) {
						pTarget.getHurt(Deck.getMyDeck().deckSize(10) + Battle.getCurrentBatle().getRound());
					}else {
						pTarget.getHurt((Deck.getMyDeck().deckSize(10)));
					}
					
				}else
				if (this.type == 3) {
					//Type 3: Min egin Target -> Jokalariaren bizitza maximoaren %20
					pTarget.getHurt((int)(Player.getMyPlayer().getMaxHp()*0.2));
				}else 
				if (this.type == 4) {
					//Type 3: Min egin Target -> Jokalariaren diruaren arabera
					pTarget.getHurt(Player.getMyPlayer().getMoney());
				}else 
				if (this.type == 6) {
					//Type 6: Min egin Target -> Owner-ari kendutako bizitzaren arabera
					pTarget.getHurt((pOwner.getMaxHp()-pOwner.getHp())/10);
				}else  
				if (this.type == -2) {
					//Type -2: Min egin Target -> Player-aren diruaren arabera /2
					pTarget.getHurt(Player.getMyPlayer().getMoney()/2);
				}else 
				if (this.type == -3) {
					//Type -3: Defentsa handitu ower-en -> Falta zaizun bizitza defentsa moduan eman
					pOwner.increaseDefense(pOwner.getMaxHp()-pOwner.getHp());
				}else 
				if (this.type == -4) {
					//Type -4: Min egin Target -> Falta zaizun bizitza
					pTarget.getHurt((pOwner.getMaxHp()-pOwner.getHp())*2);
				}else 
				if (this.type == -10) {
					//Type -10: Min egin Target eta Owner -> Energia erabili ordez bizitza erabili. Kontuz bizitza nahikorik ez baduzu hil egingo zara unean bertan.
					pOwner.getHurt(this.getEnergyCost());
					pTarget.getHurt(this.getPower());
				}
			
				this.use();
				TextBox.getBoxDev().addText(this + ": "+this.getName() + "-DYN K Erabili da arrakastatsuki / type:" + this.type); //TODO NO SE SI es NECESARIO
				
			}else {
				TextBox.getBoxDev().addText(this + ": "+this.getName() + "-DYN K Ez da erabili / type:" + this.type); 	//TODO NO SE SI es NECESARIO
				
			}
		}
		
		
		
	}
	
	public Card cloneCard() {
		/** 
		 * FUNTZIOA: DynamicCard klaseko karta kopiatzen du, atributuen balio berdinak dituen beste DynamicCard bat bueltzatuz.
		 * ERABILPENA: Karta bakarra diseinatu ondoren honen kopia independenteak egin ahal izateko erabilia. 
		 * Input: --
		 * OutPut: Card (DynamicCard) // AttackCard motatako objetua Card eran bueltatu.
		*/
		Card pCard = new DynamicCard(this.getName(), this.getId(), this.getPower(), this.getEnergyCost(), this.getPrice(), this.type);
		pCard.setDescription(this.getDesc());
		return pCard;
	}
	
	@Override
	public String getRealTarget(Entity pUser, Entity pTarget) {
		/** 
		 * FUNTZIOA: DynamicCard-aren target erreala bueltatuko du. Kasu hoenetan target-a type atributuaren arabera target ezberdina izango du. Target errealak honako hauek
		 * 			izan ahal dira: pTarget, pOwner eta biak batera
		 * ERABILPENA: TextBox-ean karta nori zuzendua izan den jakiteko. 
		 * Input: Entity motatako 2 objetu: pOwner eta pTarget
		 * OutPut: String // entitatearen izena 
		*/
		
		String realTarg = "Ez Dago"; //Ez dago espezifikatua
		int t = this.type;
		if( t==2 || t==3 || t==4 || t==6 || t==-1 || t==-2 || t==-4) {
			//Target erreala: pTarget da
			//Type: 2, 3, 4, 6,-1,-2, -4;
			realTarg = pTarget.getInstanceSring();
		}else 
		if(t==-3) {
			//Target erreala: pUser da
			//Type: -3;
			realTarg = pUser.getInstanceSring();
		}else 
		if(t==-10) {
			//Target erreala: pUser eta pTarget da
			//Type: -10;
			realTarg = pUser.getInstanceSring() + " eta " + pTarget.getInstanceSring();
		}
		
		return realTarg; //TODO
	}
	
	
	
}