package cards;
import java.util.Random;

import entity.Player;
import game.TextBox;

//EMA KLASEA
public class CardCollection {
	private CardList lista;
	private int[] cardCount = new int[3];
	
	//Atributu estatikoa: Singleton Patroia
	private static CardCollection myCardColection;
	
	
	//Eraikitzaile Pribatua: Singleton Patroia
	private CardCollection() {
		this.lista = new CardList();
		this.createAllCards();
	}
	
	//Getter estatikoa: Singleton Patroia
	public static CardCollection getCardCollection() {
		if(CardCollection.myCardColection == null)
			CardCollection.myCardColection = new CardCollection();
		return CardCollection.myCardColection;
	}
	
	public int cardNumber() { //TODO este metodo no se llama desde ningun lado: valorar eliminacion
		/** 
		 * FUNTZIOA: Listan dauden karta kopurua ematen du
		 * ERABILPENA: karta kopurua jakiteko
		 * Input: --
		 * OutPut: --
		*/
		return this.lista.size();
	}
	
	public Card searchCardById(int pId) {
		/** 
		 * FUNTZIOA: Sartutako id-a duen karta listan bilatu eta bueltatzen du. Id hori duen kartarik ez balego null bueltatuko luke
		 * ERABILPENA: Karta amak editatu ahal izateko
		 * Input: pId: int
		 * OutPut: Card
		*/
		return this.lista.searchCardById(pId);
  	}
	
	public void addCardToCollection(Card pCard) { //TODO metodo hau lokalki bakarrik deitzen da. Valorar ponerlo en private
		/** 
		 * FUNTZIOA: Parametro bezala sartutako karta listan gehitzen du, gainera karta horren id-a analizatuko du eta honen arabera cardCount array aldagaiari dagokion 
		 * 			pozioari +1 gehituko du. Karta hori null bada ala listan id berdina duen kartarik balego TextBoxDev-era warning mezua gehitu eta ez du karta listara gehituko.
		 * 			Karten id-aren analisia:
		 * 				+ Id-aren millako pozizioan dagoen zenbakia karta horren jabea nor den zehazteuko du:
		 * 					� 0 : Enemy
		 * 					� 1 : Warrior
		 * 					� 2 : Rogue
		 * ERABILPENA: Karta amak editatu ahal izateko
		 * Input: pCard: Card 
		 * OutPut: --
		*/
		try {
			if(pCard != null) {
				if(this.lista.cardWhithSameId(pCard) == null) {
					//Id-aren analisia egin
					if ((pCard.getId() % 100000)/10000 == 0) {
						
						int mota = (pCard.getId() % 10000)/1000;
						this.cardCount[mota]++;
					}
					
					//Karta Gehitu
					this.lista.addCard(pCard);
					
				}else {
					throw new Exception("Ezin izan da karta gehitu id berdina duen besta karta bat dagoelako " + pCard.getId());
				}
			}else {
				throw new Exception("Ezin izan da null karta gehitu");
			}
		}catch(Exception e) {
			TextBox.getBoxDev().addText("CARDCOLLECTION: " + e.getMessage());
		}
	}
	
	public Card getRandomCard() {
		/** 
		 * FUNTZIOA: Jokalariaren rolaren arabera auzaz karta bat bueltatzen du. Ezin izan badu karta random-ik lortu null bueltatuko du.
		 * ERABILPENA: Shop eta ebentuetan auzako kartak lortzeko
		 * Input: --
		 * OutPut: Card
		*/
		Card myCard = null;
		Random r = new Random();
		int id = 0;
		int saiakerak = 100;
		while(myCard == null && saiakerak > 0)
		{
			id = 0;
			//Player-aren roll-aren arabera
			if (Player.getMyPlayer().getJob() == 0) {
				id = r.nextInt(this.cardCount[1]-1) + 1;
				//Warrior karta
				id += 1000;
			}
			else {
				id = r.nextInt(this.cardCount[2]-1) + 1;
				//Rogue karta
				id += 2000;				
			}
			
			//Hautatu Type
			if (r.nextBoolean()) {
				id += 100;
			}
			else {
				id += 200;
			}
			
			//Hautatu upgrade
			if (r.nextBoolean()) {
				id += 10000;
			}
			
			myCard = this.searchCardById(id);
			saiakerak = saiakerak - 1;
		}
		
		return myCard;
	}
	
	public void reset() {
		CardCollection.myCardColection = null;
	}

	private void createAllCards() {
		/** 
		 * FUNTZIOA: Behean diseinatutako karta guztiak sortzen ditu eta hauek listara gehitzen ditu.
		 * ERABILPENA: Kartak diseinatuko dira hasieratik. Jokoak kartarik nahi badu lista honi eskatuko dizkio.
		 * Input: --
		 * OutPut: --
		*/
		
		//WARRIOR
		
			//ATTACK
			Card wcard101u = new AttackCard("Kolpe+", 11101, 9, 1, 1, 0, 0);
			wcard101u.setDescription("Egin 9 puntuko mina");
			this.addCardToCollection(wcard101u);
			Card wcard101 = new AttackCard("Kolpe", 1101, 6, 1, 1, 0, 0);
			wcard101.setDescription("Egin 6 puntuko mina");
			this.addCardToCollection(wcard101);
			
			Card wcard102u = new AttackCard("Ostiko+", 11102, 11, 1, 2, 0, 0);
			wcard102u.setDescription("Egin 11 puntuko mina");
			this.addCardToCollection(wcard102u);
			Card wcard102 = new AttackCard("Ostiko", 1102, 7, 1, 1, 0, 0);
			wcard102.setDescription("Egin 7 puntuko mina");
			this.addCardToCollection(wcard102);
			
			Card wcard103u = new AttackCard("Zaplasteko+", 11103, 13, 1, 3, 0, 0);
			wcard103u.setDescription("Egin 13 puntuko mina");
			this.addCardToCollection(wcard103u);
			Card wcard103 = new AttackCard("Zaplasteko", 1103, 8, 1, 2, 0, 0);
			wcard103.setDescription("Egin 8 puntuko mina");
			this.addCardToCollection(wcard103);
			
			Card wcard104u = new AttackCard("Ukabilkada+", 11104, 4, 0, 3, 2, 2);//VULNERABLE
			wcard104u.setDescription("Egin 4 puntuko mina eta Zaurgarri 2 aplikatu");
			this.addCardToCollection(wcard104u);
			Card wcard104 = new AttackCard("Ukabilkada", 1104, 2, 0, 2, 2, 1);
			wcard104.setDescription("Egin 2 puntuko mina eta Zaurgarri 1 aplikatu");
			this.addCardToCollection(wcard104);
			
			Card wcard105u = new AttackCard("Plaustada+", 11105, 18, 2, 3, 2, 3);//VULNERABLE
			wcard105u.setDescription("Egin 18 puntuko mina eta Zaurgarri 3 aplikatu");
			this.addCardToCollection(wcard105u);
			Card wcard105 = new AttackCard("Plaustada", 1105, 11, 2, 1, 2, 2);
			wcard105.setDescription("Egin 11 puntuko mina eta Zaurgarri 2 aplikatu");
			this.addCardToCollection(wcard105);
			
			//DEFEND
			Card wcard201u = new DefendCard("Blokeatu+", 11201, 8, 1, 1);
			wcard201u.setDescription("Lortu 8 defentsa");
			this.addCardToCollection(wcard201u);
			Card wcard201 = new DefendCard("Blokeatu", 1201, 5, 1, 1);
			wcard201.setDescription("Lortu 5 defentsa");
			this.addCardToCollection(wcard201);
			
			Card wcard202u = new DefendCard("Gainburutu+", 11202, 12, 1, 3);
			wcard202u.setDescription("Lortu 11 defentsa");
			this.addCardToCollection(wcard202u);
			Card wcard202 = new DefendCard("Gainburutu", 1202, 8, 1, 2);
			wcard202.setDescription("Lortu 7 defentsa");
			this.addCardToCollection(wcard202);
			
			Card wcard203u = new DefendCard("Tirabira+", 11203, 12, 1, 3);
			wcard203u.setDescription("Lortu 12 defentsa");
			this.addCardToCollection(wcard203u);
			Card wcard203 = new DefendCard("Tirabira", 1203, 8, 1, 2);
			wcard203.setDescription("Lortu 8 defentsa");
			this.addCardToCollection(wcard203);
			
			Card wcard204u = new DefendCard("Babestu+", 11204, 18, 2, 3);
			wcard204u.setDescription("Lortu 18 defentsa");
			this.addCardToCollection(wcard204u);
			Card wcard204 = new DefendCard("Babestu", 1204, 12, 2, 2);
			wcard204.setDescription("Lortu 12 defentsa");
			this.addCardToCollection(wcard204);
			
			Card wcard205u = new DefendCard("Gogortu+", 11205, 40, 3, 6);
			wcard205u.setDescription("Lortu 40 defentsa");
			this.addCardToCollection(wcard205u);
			Card wcard205 = new DefendCard("Gogortu", 1205, 12, 3, 4);
			wcard205.setDescription("Lortu 25 defentsa");
			this.addCardToCollection(wcard205);		
			
		//ROGUE
			
			//ATTACK
			Card rcard101u = new AttackCard("Kolpe+", 12101, 9, 1, 1, 0, 0);
			rcard101u.setDescription("Egin 9 puntuko mina");
			this.addCardToCollection(rcard101u);
			Card rcard101 = new AttackCard("Kolpe", 2101, 6, 1, 1, 0, 0);
			rcard101.setDescription("Egin 6 puntuko mina");
			this.addCardToCollection(rcard101);
			
			Card rcard102u = new AttackCard("Sastakai+", 12102, 8, 0, 2, 0, 0);
			rcard102u.setDescription("Egin 8 puntuko mina");
			this.addCardToCollection(rcard102u);
			Card rcard102 = new AttackCard("Sastakai", 2102, 5, 0, 1, 0, 0);
			rcard102.setDescription("Egin 5 puntuko mina");
			this.addCardToCollection(rcard102);
			
			Card rcard103u = new AttackCard("Zartailu+", 12103, 12, 1, 3, 0, 0);
			rcard103u.setDescription("Egin 12 puntuko mina");
			this.addCardToCollection(rcard103u);
			Card rcard103 = new AttackCard("Zartailu", 2103, 8, 1, 2, 0, 0);
			rcard103.setDescription("Egin 8 puntuko mina");
			this.addCardToCollection(rcard103);
			
			Card rcard104u = new AttackCard("Hanka ziztada+", 12104, 4, 1, 2, 2, 2);
			rcard104u.setDescription("Egin 4 puntuko mina eta Zaurgarri 2 aplikatu");
			this.addCardToCollection(rcard104u);
			Card rcard104 = new AttackCard("Hanka ziztada", 2104, 1, 1, 1, 2, 2);
			rcard104.setDescription("Egin 1 puntuko mina eta Zaurgarri 2 aplikatu");
			this.addCardToCollection(rcard104);
			
			Card rcard105u = new AttackCard("Pozoitu+", 12105, 0, 1, 3, 1, 7);
			rcard105u.setDescription("Pozoia 7 aplikatu");
			this.addCardToCollection(rcard105u);
			Card rcard105 = new AttackCard("Pozoitu", 2105, 0, 1, 2, 1, 5);
			rcard105.setDescription("Pozoia 5 aplikatu");
			this.addCardToCollection(rcard105);
			
			Card rcard106u = new AttackCard("Daga pozoitua+", 12106, 7, 1, 4, 1, 4);
			rcard106u.setDescription("Egin 7 puntuko mina eta Pozoia 4 aplikatu");
			this.addCardToCollection(rcard106u);
			Card rcard106 = new AttackCard("Daga pozoitua", 2106, 4, 1, 3, 1, 2);
			rcard106.setDescription("Egin 4 puntuko mina eta Pozoia 2 aplikatu");
			this.addCardToCollection(rcard106);
			
			//DEFEND
			Card rcard201u = new DefendCard("Blokeatu+", 12201, 8, 1, 1);
			rcard201u.setDescription("Lortu 8 defentsa");
			this.addCardToCollection(rcard201u);
			Card rcard201 = new DefendCard("Blokeatu", 2201, 5, 1, 1);
			rcard201.setDescription("Lortu 5 defentsa");
			this.addCardToCollection(rcard201);
			
			Card rcard202u = new DefendCard("Sahiestu+", 12202, 12, 1, 3);
			rcard202u.setDescription("Lortu 12 defentsa");
			this.addCardToCollection(rcard202u);
			Card rcard202 = new DefendCard("Saiestu", 2202, 8, 1, 2);
			rcard202.setDescription("Lortu 8 defentsa");
			this.addCardToCollection(rcard202);
			
			Card rcard203u = new DefendCard("Gainezkatu+", 12203, 18, 2, 3);
			rcard203u.setDescription("Lortu 18 defentsa");
			this.addCardToCollection(rcard203u);
			Card rcard203 = new DefendCard("Gainezkatu", 2203, 12, 2, 2);
			rcard203.setDescription("Lortu 12 defentsa");
			this.addCardToCollection(rcard203);
			
			Card rcard204u = new DefendCard("Denbora moteldu+", 12204, 30, 2, 6);
			rcard204u.setDescription("Lortu 30 defentsa");
			this.addCardToCollection(rcard204u);
			Card rcard204= new DefendCard("Denbora moteldu", 2204, 30, 3, 4);
			rcard204.setDescription("Lortu 30 defentsa");
			this.addCardToCollection(rcard204);
		
		//ENEMIES
			
	        //ATTACK
	        	//Normalak
	        Card ecard101 = new AttackCard("Atzaparkada I", 101, 4, 1, 1, 0, 0);
	        ecard101.setDescription("Kolpeatu 4");
	        this.addCardToCollection(ecard101);

	        Card ecard102 = new AttackCard("Atzaparkada II", 102, 7, 1, 1, 0, 0);
	        ecard102.setDescription("Kolpeatu 7");
	        this.addCardToCollection(ecard102);

	        Card ecard103 = new AttackCard("Atzaparkada III", 103, 10, 1, 1, 0, 0);
	        ecard103.setDescription("Kolpeatu 10");
	        this.addCardToCollection(ecard103);

	        	//Vulnerabledunak
	        Card ecard201 = new AttackCard("Hozkada I", 201, 3, 1, 1, 2, 2);
	        ecard201.setDescription("Kolpeatu 3 eta zaurigarri 2");
	        this.addCardToCollection(ecard201);

	        Card ecard202 = new AttackCard("Hozkada II", 202, 5, 1, 1, 2, 3);
	        ecard202.setDescription("Kolpeatu 5 eta zaurigarri 3");
	        this.addCardToCollection(ecard202);

	        Card ecard203 = new AttackCard("Hozkada III", 203, 8, 1, 1, 2, 3);
	        ecard203.setDescription("Kolpeatu 8 eta zaurigarri 3");
	        this.addCardToCollection(ecard203);
	        
	        	//Boss-ak
	        Card bcard1 = new AttackCard("Kriston kolpea", 911, 30, 1, 1, 0, 0);
	        bcard1.setDescription("Kolpeatu 30");
	        this.addCardToCollection(bcard1);
	        
	        Card bcard2 = new DefendCard("Nagia", 912, 0, 1, 1);
	        bcard2.setDescription("Ezer ez");
	        this.addCardToCollection(bcard2);
	        
	        Card bcard3 = new DynamicCard("Lizuna", 921, 0, 1, 1, 2);
	        bcard3.setDescription("Karta kopuruaren arabera kolpeatu");
	        this.addCardToCollection(bcard3);
	        
	        Card bcard4 = new DynamicCard("Gula", 931, 0, 1, 1, 3);
	        bcard4.setDescription("Bizitza maximoaren %20 egin");
	        this.addCardToCollection(bcard4);
	        
	        Card bcard5 = new DynamicCard("Zakua apurtu", 941, 0, 1, 1, 4);
	        bcard5.setDescription("Kolpeatu diruaren arabera");
	        this.addCardToCollection(bcard5);
	        
	        Card bcard6 = new DynamicCard("Haserrea",961, 0, 1, 1, 6);
	        bcard6.setDescription("Kolpeatu rondaren arabera");
	        this.addCardToCollection(bcard6);

	        

	        //DEFEND
	        	//Normalak
	        Card ecard301 = new DefendCard("Kiribildu I", 301, 4, 1, 1);
	        ecard301.setDescription("Blokeatu 4");
	        this.addCardToCollection(ecard301);

	        Card ecard302 = new DefendCard("Kiribildu II", 302, 6, 1, 1);
	        ecard302.setDescription("Blokeatu 6");
	        this.addCardToCollection(ecard302);

	        Card ecard303 = new DefendCard("Kiribildu III", 303, 8, 1, 1);
	        ecard303.setDescription("Blokeatu 8");
	        this.addCardToCollection(ecard303);
	        
	        Card ecard304 = new DefendCard("Defendatu I", 304, 12, 1, 1);
	        ecard304.setDescription("Blokeatu 12");
	        this.addCardToCollection(ecard304);
	        
	        Card ecard305 = new DefendCard("Defendatu II", 305, 18, 1, 1);
	        ecard305.setDescription("Blokeatu 18");
	        this.addCardToCollection(ecard305);
	    //EVENTS
	        Card eventCard1 = new AttackCard("Ezpata ahaztua (R)", -1, 14, 1, 1, 0, 0);
            eventCard1.setDescription("Egin 14 puntuko mina");
            this.addCardToCollection(eventCard1);
            
            Card eventCard2 = new DefendCard("Ezkutu gogorra (R)", -2, 16, 1, 1);
            eventCard2.setDescription("Lortu 16 defentsa");
            this.addCardToCollection(eventCard2);
            
            
            
            Card legenCard1 = new AttackCard("Excalibur (L)", -101, 20, 1, 1, 0, 0);
            legenCard1.setDescription("Ziztada insartsua dauka: egin 20 puntuko mina");
            this.addCardToCollection(legenCard1);
            
            Card legenCard2 = new DefendCard("Perseoren ezkutua (L)", -102, 25, 1, 1);
            legenCard2.setDescription("Brontzezko ezkutua: lortu 25 defentsa ");
            this.addCardToCollection(legenCard2);
            
            Card legenCard3 = new DynamicCard("Trukea (L)", -103, 0, 0, 1, -10);
            legenCard3.setDescription("5 min hartu eta 25 kolpeatu");
            this.addCardToCollection(legenCard3);
            
            Card legenCard4 = new DefendCard("Hadesen Kaskoa (L)", -104, 99, 2, 1);
            legenCard4.setDescription("Ikusezin bihurtu: lortu 99 defentsa ");
            this.addCardToCollection(legenCard4);
            
            Card legenCard5 = new DefendCard("Perseoren ezkutua (L)", -105, 50, 2, 1);
            legenCard5.setDescription("Etsaia obliteratu: egin 50 puntuko min ");
            this.addCardToCollection(legenCard5);
            
    	//BOSS REWARDS
            Card rewardCard1 = new DefendCard("Nagia", -10, 20, 0, 1);
            rewardCard1.setDescription("Lortu 10 defentsa");
            this.addCardToCollection(rewardCard1);
            
            Card rewardCard2 = new DynamicCard("Lizunkeria", -20, 0, 1, 1, -1);
            rewardCard2.setDescription("Egin deckaren araberako mina");
            this.addCardToCollection(rewardCard2);
            
            Card rewardCard3 = new AttackCard("Gula", -30, 25, 0, 1, 0, 0);
            rewardCard3.setDescription("Egin 10 puntuko mina");
            this.addCardToCollection(rewardCard3);
            
            Card rewardCard4 = new DynamicCard("Zekenkeria", -40,0, 1, 1, -2);
            rewardCard4.setDescription("Egin diruaren araberako mina");
            this.addCardToCollection(rewardCard4);
            
            Card rewardCard5 = new DynamicCard("Bekaizkeria", -50, 0, 1, 1, -3);
            rewardCard5.setDescription("Lortu falta zaizun bizitza defentsa moduan");
            this.addCardToCollection(rewardCard5);
            
            Card rewardCard6 = new DynamicCard("Haserrea", -60, 0, 2, 1, -4);
            rewardCard6.setDescription("Egin falta zaizun bizitzaren bikoitza puntuko min");
            this.addCardToCollection(rewardCard6);
            
	}
	
}
