package cards;

import entity.Entity;
import game.Mapa;

public abstract class Card {
	//Atributuak	
	private String name;
	private String description = "Ez du deskripziorik";
	
	private int id;
	private int energyCost;
	private int power;
	private int price;
	private boolean used = false;
	
	private int probability = 10;

	//Eraikitzailea
	public Card(String pName, int pId, int pPower, int pEnergyCost, int pPrice) {
		this.name = pName;
		this.id = pId;
		this.power = pPower;
		this.energyCost = pEnergyCost;
		this.price = pPrice;
		
	}
	
	//Getter
	
	public String getName() {
		return this.name;
	}
	public String getDesc() {
		return this.description;
	}
	public int getPrice() {
		return this.price;
	}
	
	public int getId() {
		return this.id; 
	}
	protected int getEnergyCost() {
		return this.energyCost;
	}
	protected int getPower(){
		return this.power;
	}
	
	public int getProbability() {
		return this.probability;
	}
	public Card getUpgrade() {
		int sum = 10000;
		if(this.id < 0) {
			sum = sum*(-1);
		}
		return CardCollection.getCardCollection().searchCardById(this.id + sum);
	}
	public boolean isUsed() {
		return this.used;
	}
	
	//Setter
	public void setDescription(String pDesc){
		this.description=pDesc;
	}
	public void setProbability(int pProbab) {
		if(pProbab >= 0) {
			this.probability = pProbab;
		}
	}
	protected void use() {
		this.used = true;
	}
	
	//Metodo konprobatzaile/konparatzaile
	public boolean hasThisId(int pid){
		return this.id == pid;
	}
	public boolean hasSameId(Card pCard){
		return pCard.hasThisId(this.id);
	}
	
	public void reset() {
		/** 
		 * FUNTZIOA: Use atributua erreseteatu: used = false;
		 * ERABILPENA: Kartak berrerabili ahal izateko eta kopiak egitea eta borratzea aurrezteko.
		 * Input: --
		 * OutPut: --
		*/
		this.used = false;
	}
	
	/*//TODO valorar eliminacion
	protected void setLastUser(Entity pUser) {
		this.lastEntities[0] = pUser;
	}
	protected void setLastTarget(Entity pTarget) {
		this.lastEntities[1] = pTarget;
	}
	*/
	
	public abstract Card cloneCard();
	
	public abstract void useCard(Entity pOwner, Entity pTarget);
	
	// public abstract String getLastTarget(); //TODO LAST TARGET - valorar eliminacion
	
	public abstract String getRealTarget(Entity pUser, Entity pTarget);
	
	public String getCardAsString() {
		/** 
		 * FUNTZIOA: Metodo honek karta objetuaren Izena, Energia-kostua eta Deskripzioa bueltatuko du prozezatutako String ordenatu eta laburtu batean.
		 * ERABILPENA: Joklariak Deck-ean dauden kartak eta hauen info-a zeintzuk den jakiteko + Dendan kartaren-infoa bistaratzeko. Karta-String hau inprimatuko da beste Karta-String batzuekin taula batean.
		 * Input: --
		 * OutPut: Kartaren deskripzio naguzia blokean + deskripzioa moldatuko du behar diren saltoak eginez testuan(\n) -> String
		*/
		
		String card;
		
		card = this.name + "  Cost: " + this.energyCost + "\n"; 

		
		card = card + Mapa.reduceString(this.description, card.length()-1, true, true);
		
		return card;
	}
	
	public String getCardInfoAsSimpleString() {
		/** 
		 * FUNTZIOA: Metodo honek karta objetuaren Izena, Deskripzioa eta honen Probabilitatea bueltatuko du String batean.
		 * ERABILPENA: Etsaien aurkezpenak egiteko beharrezkoa den karta informazioa
		 * Input: --
		 * OutPut: Kartaren deskripzio txikia -> String
		*/
		
		String str = "";
		str = str + this.name +": "+ this.description ;
		if(this.probability != 0) {
			str = str + "; Probab: " +this.probability;
		}
		return str;
	}
	
	//TODO valorar eliminacion metodo inutil por que no se usa pero estoy orgulloso del algoritmo a si que me da pena borrarlo :)
	/*
	public static int[] getWordPosOnString(String pStr, int pPos){
		int[] ema = new int[2]; // 0 aurre/ 1 amaiera
		ema[0]= pPos;
		ema[1]= pPos;
		boolean stop1 = false;
		boolean stop2 = false;
		
		
			while(!stop1 || !stop2) {
				if(!stop1) {
					if(pStr.length() - ema[1] -1 <= 0) {
						stop1 = true;
					}
					else if(pStr.charAt(ema[1]) == ' ') {
						stop1 = true;
					}else {
						ema[1]= ema[1] + 1;
					}
				}
				if(!stop2) {
					if(ema[0] <= 0) {
						stop2 = true;
					}
					else if(pStr.charAt(ema[0]) == ' ') {
						ema[0]= ema[0] + 1;
						stop2 = true;
					}else if(!stop2){
						ema[0]= ema[0] + 1;
					}
				}
			}
		
		
		
		return ema;
	}
	*/
}
