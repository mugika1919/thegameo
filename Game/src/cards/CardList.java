package cards;
import java.util.ArrayList;
import java.util.Iterator;

import game.TextBox;

public class CardList {
	
	private ArrayList<Card> lista;
	
	public CardList() {
		this.lista= new ArrayList<Card>();
	}
	
	private Iterator<Card> getIterator(){
		return this.lista.iterator();
	}
	public int size() {
		return this.lista.size();
	}
	
	
	public Card searchCardById(int pId) {
		/** 
		 * FUNTZIOA: pId int bat emanda listan id hori duen karta bueltatuko du. Kartarik topatuko ez balu null bueltatuko du.
		 * ERABILPENA: Kartarik bahaldagoen jakiteko edo id hori duen karta lortzeko.
		 * Input: int pId
		 * OutPut: Card
		*/
		Iterator<Card> itr = this.getIterator();
		boolean found = false;
		Card myCard = null;
		while(itr.hasNext() && !found) {
			myCard = itr.next();
			if(myCard.hasThisId(pId)) {
				found = true;
			}
		}
		if(!found) {
			myCard = null;
		}
		return myCard;
  	}
	
	public boolean hasCard(Card pCard) {
		/** 
		 * FUNTZIOA: Objetu BERA listan badagoen esan (Punteroak konparatzen ditu)
		 * ERABILPENA: Karta bera lista berean ez dagoela zihurtatzeko
		 * Input: Card pCard
		 * OutPut: --
		*/
		return this.lista.contains(pCard);
	}
	
	public int getTotalProbability() {
		/** 
		 * FUNTZIOA: listan dauden karta guztien probalilitatearen gehiketa lortu.
		 * ERABILPENA: listaren probabilitate totala jakiteko
		 * Input: --
		 * OutPut: int
		*/
		Iterator<Card> itr = this.getIterator();
		int kont = 0;
		while(itr.hasNext()) {
			kont = kont + itr.next().getProbability();
		}

		return kont;
	}
	
	public Card getCardByProbability(int pNumber) { //Zenbaki hau normalean 0-99 (100 zenbaki)
		/** 
		 * FUNTZIOA: pNumber int bat emanda listan dauden karten probabilitateen gehiketen arabera zenbaki honekin bat egiten duen karta lortu.
		 * ERABILPENA: Auzazko karta bat lortzeko.
		 * Input: int pNumber
		 * OutPut: Card
		*/
		int total = this.getTotalProbability();
		Card card = null;
		
		if(total != 0) {
			Iterator<Card> itr = this.getIterator();
			
			int edited = pNumber % total; 	//	Honekin pNumber-a < probTotala dela zihurtatu
											// 	pNumber < 0 bada orduan lehenengo karta bidali
			
			boolean stop = false;
			int akumul = -1;
			

			while(!stop && itr.hasNext()) {
				card = itr.next();
				akumul = akumul + card.getProbability();
				if(edited <= akumul) {
					stop = true;
				}
			}
			if(!stop) {
				card = null;
			}
			
		}
		
		return card;
	}
	
	public Card cardWhithSameId(Card pCard) {
		/** 
		 * FUNTZIOA: Karta bat emanda id berdina duen karta bueltatuko du. Id berdina duen kartarik ez balego null bueltatuko du
		 * ERABILPENA: id berdina duen karta CardCollection-en sartzea saihestu
		 * Input: Card pCard
		 * OutPut: Card
		*/
		Iterator<Card> itr = this.getIterator();
		boolean found = false;
		Card myCard = null;
		while (itr.hasNext() && !found) {
			myCard = itr.next();
			if(myCard.hasSameId(pCard))
				found=true;
		}
		if(!found)
			myCard=null;
		return myCard;
	}
	
	public void addCard(Card pCard) { //Trows KartaListanDago
		/** 
		 * FUNTZIOA: Karta bat emanda karta hau listan sartuko du baldin eta karta bera hori listan ez baitago. Karta holi listan egongo balitz text box dev-ean mezua gehituko du.
		 * ERABILPENA: Sartzen diren karta objetu guztiak ezberdinak (karta1.equals(karta2) = false beti)
		 * Input: Card pCard
		 * OutPut: void
		*/
		if(!this.lista.contains(pCard)) {
			this.lista.add(pCard);
		}else {
			TextBox.getBoxDev().addText("Karta hau jadanik deck-ean zegoen: " + pCard.getName() + "/" + pCard.getId());//Karta hori gehitu nahi bada kopia bat egin eta gero bertan sartu.
		}
	}
	
	public Card getCardInPos(int pPos) { //trows OutOfBoundsExeption
		/** 
		 * FUNTZIOA: Pozizio bat emanda listan pozizio horretan dagoen karta bueltatuko du. Pozizioa txarto egongo balitz edo pozizio horretan kartarik egongo ez balitz null bueltatukoo luke
		 * ERABILPENA: Pozizioa emada karta bat lortzeko. Hau kartak aukeratzeko eta kartak auzazki hautatzeko erabiltzen da.
		 * Input: int pPos (Pozizio bat listan)
		 * OutPut: Card (Listan pozizio horretan dagoen karta)
		*/
		Card ema = null;
		try {
			ema = this.lista.get(pPos);
		}catch (IndexOutOfBoundsException e) {
			ema = null;
			System.out.println("Pozizio Honetan ez dago kartarik");
		}
		return ema;
	}
	
	public void removeCard(Card pCard) throws Exception{
		/** 
		 * FUNTZIOA: Karta bat emanda karta hori listatik kendu
		 * ERABILPENA: Kartak listatik kentzeko, eta beste batzuetara mugitzeko erabilia.
		 * Input: Card pCard
		 * OutPut: --
		*/
		if(pCard != null) {
			if(this.lista.contains(pCard)) {
				this.lista.remove(pCard);
			}else {
				throw new Exception("CardList: Kendu nahi den karta ez dago listaren barnean ID: " + pCard.getId());
				//System.out.println("Kendu nahi den karta ez dago listaren barnean");
			}
		}else {
			TextBox.getBoxDev().addText("CardList: Ezin da karta nulua kendu");
		}
		
	}
	
	public void removeFirstCardById(int pId) throws Exception{ //TODO este metodo no se usa por que no hay upgrades
		/** 
		 * FUNTZIOA: ID bat emanda, id hori duen lehenengo karta listatik kenduko du.
		 * ERABILPENA: Upgrade egiteko erabiliko zen. Id hori zuen karta sinple bat hartu haren upgradea listan sartu eta id hori zuen lehenengo karta sinplea listatik kendu.
		 * Input: int pId
		 * OutPut: --
		*/
		Card ema = this.searchCardById(pId);
		if(ema != null) {
			this.lista.remove(ema);
		}else {
			throw new Exception("Ezin izan da kendu, Ez dago id hori duen kartarik Id: " + pId);
			//System.out.println("Kendu nahi den karta ez dago listaren barnean ID: " + pId);
		}
	}
	
	public void moveAllCardsToTarget(CardList pCardList) { //TODO MAYBE EXCEPTION que pasa si el parametro es = null da error y peta (Arreglado con un if)
		/** 
		 * FUNTZIOA: Kartak deitu den objetuaren listatik parametroaren bitartez zehaztutako listara mugitzen ditu. Hau da, lista batetik karta hartu beste listan gehitu eta hasierako 
		 * 			listatik kentzen du, hau hasierako karta guztiekin egingo da.
		 * ERABILPENA: Kartak deck-en artean mugitzeko erabilia
		 * Input: CardList pCardList
		 * OutPut: --
		*/
		if(pCardList != null) {
			Iterator<Card> itr = this.getIterator();
			
			while(itr.hasNext()) {
				pCardList.addCard(itr.next());
			}
		}
		this.removeAllCards(); //TODO this.lista.clear(); deitu daiteke eta metodo pribatua removeAllCards() kendu
		
	}
	private void removeAllCards() { //TODO private izatera pasatu. Lokalki bakarrik erabilia (behin moveAllCardsToTaget) //TODO behar bada metodo osoa kendu eta moveAllCardsToTaget metodoan this.lista.clear(); jarri metodoari deitu beharrean
		/** 
		 * FUNTZIOA: listatik karta objetu guztiak borratzen ditu lista hutsa lortuz
		 * ERABILPENA: Kartak mugitzerako momentuan kartak lista batetik kentzeko
		 * Input: --
		 * OutPut: --
		*/
		//Karta guztiak ezabatzen ditu.
		this.lista.clear();
	}
	/*
	public void removeAllCardsToTarget(CardList pCardList) {//TODO no se usa :(
		
		Iterator<Card> itr = this.getIterator();
		
		while(itr.hasNext()) {
			
			try{
				pCardList.removeCard(itr.next());
			}catch(Exception e) {
				TextBox.getBoxDev().addText("RemoveAllCardsToTarget: Ezin izan da kendu karta ez baitago");
			}
			
		}
		
	}
	*/
	/*
	public CardList ebakidura(CardList pCd) { //TODO no se usa :(
		CardList ema = new CardList();
		Iterator<Card> itr = this.getIterator();
		while(itr.hasNext()) {
			Card car = itr.next();
			if(pCd.hasCard(car)) {
				ema.addCard(car);
			}
		}
		return ema;	
	}
	*/
	public void resetCards() {
		/** 
		 * FUNTZIOA: Karta guztiak erreseteatzen ditu baina ez ditu listatik kentzen
		 * ERABILPENA: Listan dauden karta guztiak erreseteatzeko erabilia
		 * Input: --
		 * OutPut: --
		*/
		Iterator<Card> itr = this.getIterator();
		while(itr.hasNext()) {
			itr.next().reset();
		}
	}
	
	public CardList duplicateCardlist() {
		/** 
		 * FUNTZIOA: lista kopiatu egiten du, hau da, ama listako karta guztien kopia bat egin ondoren hauek lista berri batean sartu eta azkenengo lista hau bueltatzen du.
		 * ERABILPENA: Listak kopiatzeko kartak duplikatuz
		 * Input: --
		 * OutPut: --
		*/
		CardList ema = new CardList();
		Iterator<Card> itr = this.getIterator();
		while(itr.hasNext()) {
			Card car = itr.next();
			car = car.cloneCard();
			ema.addCard(car);
		}
		return ema;
	}
	/*
	public void printDeck() { //TODO Ez da erabiltzen, kartak zuzenean inprimatzen dituen metodorik ez da egongo eamenduamgure plant
		Iterator<Card> itr = this.getIterator();
		int n=0;
		Card k=null;
		while(itr.hasNext()) {
			k=itr.next();
			n++;
			System.out.print(n);
			System.out.print(". ");
			k.printCard();
		}
	}
	*/
	public String[] getCardsInfoAsString(){
		/** 
		 * FUNTZIOA: Listaren karta guztien informazioa array batean gorde eta hau bueltatzen du.
		 * ERABILPENA: Listan dauden karten infoa jasotzeko eta gero inprimatu ahal izateko
		 * Input: --
		 * OutPut: Array[]: Strings / length: karta kop
		*/
		
		return this.getCardsInfoAsString(false);
	}
	
	public String[] getCardsInfoAsSinpleString(){
		/** 
		 * FUNTZIOA: Listaren karta guztien informazio sinplea array batean gorde eta hau bueltatzen du.
		 * ERABILPENA: Listan dauden karten info sinplea jasotzeko eta gero inprimatu ahal izateko
		 * Input: --
		 * OutPut: Array[]: Strings / length: karta kop
		*/
		
		return this.getCardsInfoAsString(true);
	}
	
	private String[] getCardsInfoAsString(boolean pSinple) {
		/** 
		 * FUNTZIOA: Parametro boolear baten arabera(pSinple) listaren karta guztien informazio sinplea edo informazio osoa array batean gorde eta hau bueltatzen du.
		 * 			pSinple = true bada: Informazio Sinplea // pSinple = false bada: Informazio osoa
		 * ERABILPENA: Listan dauden karten info sinplea edo informazio osoa jasotzeko eta gero inprimatu ahal izateko
		 * Input: pSinple: boolean 
		 * OutPut: Array[]: Strings / length: karta kop
		*/
		String[] strList = new String[this.lista.size()];
		Iterator<Card> itr = this.getIterator();
		int kont = 0;
		while(itr.hasNext()) {
			if(!pSinple) {
				strList[kont] = itr.next().getCardAsString();
			}else {
				strList[kont] = itr.next().getCardInfoAsSimpleString();
			}
			kont = kont + 1;
		}
		return strList;
	}
}
